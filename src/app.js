import dispatcher from "./modules/dispatcher.js";
import domReady from "./modules/domReady.js";
import "./modules/utils/math.utils.js";

import "./config/ease.config.js";
import "./config/time.config.js";

import preloaderSimple from "./modules/page-load/preloader-simple.view.js";
import "./modules/events/keyboard.view.js";
import resizeHelper from "./modules/resize/resize-helper.view.js";
import "./modules/accessibility/outliner.view.js";

import scrollLockerCss from "./modules/scroll/scroll-locker-css.view.js";

import "./modules/popup/popup.component.js";
import "./modules/popup/popup-toggle.component.js";

domReady(function () {
  dispatcher.dispatch({
    type: "dom:ready",
  });

  preloaderSimple.init();
  resizeHelper.init();
  scrollLockerCss.init();
});

export default {};
