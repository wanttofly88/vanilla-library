let pathElement = document.getElementsByName("content-path")[0];

if (!pathElement) {
	console.warn("meta name=\"content-path\" is missing");
}

let data = {
	content: pathElement ? pathElement.content : "",
};

if (data.content.slice(-1) !== "/") {
	data.content += "/";
}

if (!window._vars) {
    window._vars = {}
}

window._vars.path = data;

export default data;