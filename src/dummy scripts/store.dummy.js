import dispatcher from "../dispatcher.js";
import EventEmitter from "../utils/EventEmitter.js";

class Store {
	constructor() {
		this.eventEmitter = new EventEmitter();
		this.eventEmitter.subscribe = this.eventEmitter.subscribe.bind(
			this.eventEmitter
		);
		this.eventEmitter.unsubscribe = this.eventEmitter.unsubscribe.bind(
			this.eventEmitter
		);

		this.handleEvent = this.handleEvent.bind(this);
		this.getData = this.getData.bind(this);
	}

	init() {
		dispatcher.subscribe(this.handleEvent);
	}

	handleEvent() {}

	getData() {
		return {};
	}
}

let store = new Store();
store.init();

export default {
	subscribe: store.eventEmitter.subscribe,
	unsubscribe: store.eventEmitter.unsubscribe,
	getData: store.getData,
};