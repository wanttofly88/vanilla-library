import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";

const decorators = [];

const defaultOptions = {
    __namespace: "",
};

class ElementClass extends HTMLElement {
    constructor(self) {
        self = super(self);
        self.init.call(self);
    }

    init() {}

    connectedCallback() {
        this._options = datasetToOptions(this.dataset, defaultOptions);

        // code goes here
        decorators.forEach((d) => d.attach(this));
    }

    disconnectedCallback() {
        // code goes here
        decorators.forEach((d) => d.detach(this));
    }
}

customElements.define("component-name", ElementClass);

export default ElementClass;
