import { datasetToOptions } from "../utils/component.utils.js";
import sliderStore from "./slider.store.js";

const defaultOptions = {
    __namespace: "",
    id: "",
    indexClass: "index",
    totalClass: "total"
};

class ElementClass extends HTMLElement {
    constructor(self) {
        self = super(self);
        self.init.call(self);
    }

    init() {
        this.handleSlider = this.handleSlider.bind(this);
    }

    connectedCallback() {
        this._options = datasetToOptions(this.dataset, defaultOptions);

        this._elementIndex = this.getElementsByClassName(this._options.indexClass)[0];
        this._elementTotal = this.getElementsByClassName(this._options.totalClass)[0];

        this.handleSlider();
        sliderStore.subscribe(this.handleSlider);
    }

    disconnectedCallback() {
        sliderStore.unsubscribe(this.handleSlider);
    }

    handleSlider() {
        let sliderData = sliderStore.getData()[this._options.id];
        if (!sliderData) {
            return;
        }

        this._elementIndex.innerHTML = sliderData.index + 1;
        this._elementTotal.innerHTML = sliderData.total;
    }
}

customElements.define("slider-counter", ElementClass);

export default ElementClass;
