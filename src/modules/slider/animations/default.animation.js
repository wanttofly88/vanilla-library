import dispatcher from "../../dispatcher.js";
import { attach, detach } from "../../utils/decorator.utils.js";
import { transition, simpleTween } from "../../utils/animation.utils.js";

const name = "animation";

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleDispatcher = this.handleDispatcher.bind(this);
    }

    init() {
        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    animate(options) {
        let prevSlide = options.prevSlide;
        let nextSlide = options.nextSlide;
        let durationMultiplyer = options.durationMultiplyer;
        let direction = options.direction;
        let width = this._parent.clientWidth;
        let control = options.userData.control;

        let directionSign = direction === "next" ? 1 : -1;

        if (prevSlide) {
            setTimeout(() => {
                transition(
                    prevSlide,
                    (this._options.speed) * durationMultiplyer,
                    {
                        transform: `translateX(${-directionSign * width / 2}px)`,
                    },
                    {
                        ease: "ease",
                    }
                );
            }, 20 * durationMultiplyer);
        }

        if (control !== "drag") {
            transition(
                nextSlide,
                0,
                {
                    transform: `translateX(${directionSign * width}px)`,
                },
                {
                    ease: "ease",
                }
            );
        }

        if (nextSlide) {
            setTimeout(() => {
                transition(
                    nextSlide,
                    this._options.speed * durationMultiplyer,
                    {
                        transform: "translateX(0px)",
                    },
                    {
                        ease: "ease",
                    }
                );
            }, 20 * durationMultiplyer);
        }
    }

    handleDispatcher(e) {
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
};
