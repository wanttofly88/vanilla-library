import { attach, detach } from "../../utils/decorator.utils.js";
import { transition } from '../../utils/animation.utils.js';

const name = "animation";

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);
	}

	animate(options) {
		let prevSlide = options.prevSlide;
		let nextSlide = options.nextSlide;
		let durationMultiplyer = options.durationMultiplyer;
		let direction = options.direction;
		let width = this._parent.clientWidth;

		let ease;

		if (window._vars && window._vars.ease) {
			ease = window._vars.ease.css;
		} else {
			ease = {
				ease: "ease",
				easeOut: "ease-out",
				easeIn: "ease-in",
			}
		}

		// if (!nextSlide.classList.contains("moving")) {
			if (direction === "next") {
				transition(nextSlide, 0, {
					opacity: 0,
				});
			}
			if (direction === "prev") {
				transition(nextSlide, 0, {
					opacity: 0,
				});
			}
		// }

		setTimeout(() => {
			if (direction === "next") {
				transition(
					prevSlide,
					(this._options.duration / 1.3) * durationMultiplyer,
					{
						opacity: 0,
					},
					{
						ease: ease.easeIn
					}
				);
			}

			if (direction === "prev") {
				transition(
					prevSlide,
					(this._options.duration / 1.3) * durationMultiplyer,
					{
						opacity: 0,
					},
					{
						ease: ease.easeIn
					}
				);
			}

			transition(
				nextSlide,
				(this._options.duration) * durationMultiplyer,
				{
					opacity: 1,
				},
				{
					ease: ease.easeOut,
					delay: this._options.duration / 2
				}
			);
		}, 20 * durationMultiplyer);
	}

	init() {}

	destroy() {}
}

export default {
	attach: (parent, options) => {
		return attach(
			Decorator,
			parent,
			Object.assign({}, defaultOptions, options)
		);
	},
	detach: (parent, options) => {
		return detach(parent, Object.assign({}, defaultOptions, options));
	},
};