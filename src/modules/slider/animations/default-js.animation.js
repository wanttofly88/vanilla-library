import dispatcher from "../../dispatcher.js";
import { attach, detach } from "../../utils/decorator.utils.js";
import { transition, simpleTween } from "../../utils/animation.utils.js";

const name = "animation";

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleDispatcher = this.handleDispatcher.bind(this);
    }

    init() {
        this._parent._slides.forEach((slide) => {
            slide.element._position.push({
                name: "slider",
                value: { left: 0, top: 0 },
            });
        });

        this._parent._usesJsAnimation = true;

        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    animate(options) {
        let prevSlide = options.prevSlide;
        let nextSlide = options.nextSlide;
        let durationMultiplyer = options.durationMultiplyer;
        let direction = options.direction;
        let width = this._parent.clientWidth;

        let directionSign = direction === "next" ? 1 : -1;

        if (prevSlide) {
            if (prevSlide._sliderTween) {
                prevSlide._sliderTween.kill();
            }

            prevSlide.style.willChange = "transform";

            setTimeout(() => {
                prevSlide.style.willChange = "";
            }, this._options.speed * 1000 * durationMultiplyer);

            let prevPos = prevSlide._position;
            prevSlide._sliderTween = simpleTween(
                {
                    from: prevPos.get({ name: "slider" }).left,
                    to: -directionSign * width,
                    duration: this._options.speed * durationMultiplyer,
                    ease: "ease",
                },
                (tick) => {
                    if (!prevPos) return;
                    prevPos.set({
                        name: "slider",
                        value: { left: tick.value },
                    });
                }
            );
        }

        if (nextSlide) {
            if (nextSlide._sliderTween) {
                nextSlide._sliderTween.kill();
            }

            nextSlide.style.willChange = "transform";

            setTimeout(() => {
                nextSlide.style.willChange = "";
            }, this._options.speed * 1000 * durationMultiplyer);

            let nextPos = nextSlide._position;

            nextSlide._sliderTween = simpleTween(
                {
                    from: nextPos.get({ name: "slider" }).left,
                    to: 0,
                    duration: this._options.speed * durationMultiplyer,
                    ease: "ease",
                },
                (tick) => {
                    if (!nextPos) return;
                    nextPos.set({
                        name: "slider",
                        value: { left: tick.value },
                    });
                },
                () => {
                    nextSlide._animating = false;
                }
            );
        }
    }

    handleDispatcher(e) {
        if (
            e.type === "slider:prepare-next-slide" &&
            e.id === this._parent._options.id
        ) {
            let width = this._parent.clientWidth;
            let slide = this._parent._slides[e.index];

            if (!slide) return;
            slide = slide.element;

            slide._position.set({
                name: "slider",
                value: { left: -width * e.direction, top: 0 },
            });
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
};