import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import sliderControlDecorator from "./decorators/slider-control.decorator.js";

const decorators = [sliderControlDecorator];

const defaultOptions = {
	__namespace: "",
};

class ElementClass extends HTMLButtonElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		// code goes here
		decorators.forEach((d) => d.attach(this, this._options));
	}

	disconnectedCallback() {
		// code goes here
		decorators.forEach((d) => d.detach(this, this._options));
	}
}

customElements.define("slider-control", ElementClass, {
	extends: "button"
});

export default ElementClass;