import { attach, detach, update } from "../../utils/decorator.utils.js";
import { datasetToOptions } from "../../utils/component.utils.js";
import sliderStore from "../slider.store.js";
import dispatcher from "../../dispatcher.js";

const name = "slider-control"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
    id: "",
    to: ""
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.handleClick = this.handleClick.bind(this);
        this.handleSliderStore = this.handleSliderStore.bind(this);
    }

    init() {
        this._options = datasetToOptions(this._parent.dataset, this._options);

        let validValues = ["next", "prev"];

        if (validValues.indexOf(this._options.to) === -1) {
            this._options.to = parseInt(this._options.to);
            this._mode = "goto";
        } else {
            this._mode = "arrow";
        }

        this.handleSliderStore();
        sliderStore.subscribe(this.handleSliderStore);

        this._parent.addEventListener("click", this.handleClick);
    }

    destroy() {
        sliderStore.unsubscribe(this.handleSliderStore);

        this._parent.removeEventListener("click", this.handleClick);
    }

    handleClick() {
        if (this._mode === "arrow") {
            dispatcher.dispatch({
                type: "slider:" + this._options.to,
                id: this._options.id,
                userData: {
                    control: "button",
                    direction: this._options.to,
                },
            });
        } else if (this._mode === "goto") {
            dispatcher.dispatch({
                type: "slider:to",
                index: this._options.to,
                id: this._options.id,
                userData: {
                    control: "button",
                },
            });
        }
    }

    handleSliderStore() {
        var sliderData = sliderStore.getData()[this._options.id];
        if (!sliderData) return;

        if (this._mode === "arrow" && !sliderData.continuous) {
            if (sliderData.index === 0 && this._options.to === "prev") {
                this._parent.classList.add("disabled");
                this._parent.setAttribute("aria-disabled", true);
                this._parent.dispatchEvent(new Event('change'));
            } else if (sliderData.index === sliderData.total - 1 && this._options.to === "next") {
                this._parent.classList.add("disabled");
                this._parent.setAttribute("aria-disabled", true);
                this._parent.dispatchEvent(new Event('change'));
            } else {
                this._parent.classList.remove("disabled");
                this._parent.removeAttribute("aria-disabled");
                this._parent.dispatchEvent(new Event('change'));
            }
        } else if (this._mode === "goto") {
            if (sliderData.index === this._options.to) {
                this._parent.classList.add("disabled");
                this._parent.classList.add("active");
                this._parent.setAttribute("aria-current", true);
                this._parent.dispatchEvent(new Event('change'));
            } else {
                this._parent.classList.remove("disabled");
                this._parent.classList.remove("active");
                this._parent.removeAttribute("aria-current");
                this._parent.dispatchEvent(new Event('change'));
            }
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};