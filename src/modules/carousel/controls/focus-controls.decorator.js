import dispatcher from '../../dispatcher.js';
import {attach, detach} from '../../utils/decorator.utils.js';

const name = 'focus-controls';

const defaultOptions = {
    name: name
};

class Decorator {
    constructor(parent, options) {
        this._name = name;
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this._enabled = false;
        this._subscribedFocus = false;

        this.enable = this.enable.bind(this);
        this.disable = this.disable.bind(this);
        this._handleKeyboard = this._handleKeyboard.bind(this);
        this._handleFocus = this._handleFocus.bind(this);
        this._handleBlur = this._handleBlur.bind(this);
    }

    _handleKeyboard(e) {
        if (e.type === 'keyboard:left') {
            dispatcher.dispatch({
                type: 'carousel:prev',
                id: this._parent._id,
                by: '1',
            });
        }
        if (e.type === 'keyboard:right') {
            dispatcher.dispatch({
                type: 'carousel:next',
                id: this._parent._id,
                by: '1',
            });
        }
        if (e.type === 'keyboard:tab' || e.type === 'keyboard:shift-tab') {
        }
    }

    _handleFocus() {
        this._subscribedFocus = true;
        dispatcher.subscribe('keyboard', this._handleKeyboard);
    }

    _handleBlur() {
        this._subscribedFocus = false;
        dispatcher.unsubscribe('keyboard', this._handleKeyboard);
    }

    enable() {
        if (this._enabled) return;
        this._enabled = true;

        this._parent.setAttribute('tabindex', 0);
        // Класс нужен для блокировки-включения фокуса внешними факторами. Например, попапом.
        this._parent.classList.add('focusable');
        dispatcher.dispatch({
            type: 'focusable:add',
            element: this._parent,
        });

        this._parent.addEventListener('focus', this._handleFocus);
        this._parent.addEventListener('blur', this._handleBlur);
    }

    disable() {
        if (!this._enabled) return;
        this._enabled = false;

        this._parent.removeAttribute('tabindex');
        this._parent.classList.remove('focusable');
        dispatcher.dispatch({
            type: 'focusable:remove',
            element: this._parent,
        });

        this._parent.removeEventListener('focus', this._handleFocus);
        this._parent.removeEventListener('blur', this._handleBlur);
        if (this._subscribedFocus) {
            dispatcher.unsubscribe('keyboard', this._handleKeyboard);
        }
    }

    init() {}

    destroy() {
        if (this._enabled) {
            this._parent.removeAttribute('tabindex');
            this._parent.classList.remove('focusable');
            dispatcher.dispatch({
                type: 'focusable:remove',
                element: this._parent,
            });

            this._parent.removeEventListener('focus', this._handleFocus);
            this._parent.removeEventListener('blur', this._handleBlur);
            if (this._subscribedFocus) {
                dispatcher.unsubscribe('keyboard', this._handleKeyboard);
            }
        }
    }
}

export default {
    attach: (parent, options) => {
        attach(Decorator, parent, Object.assign({}, defaultOptions, options));
    },
    detach: (parent, options) => {
        detach(parent, Object.assign({}, defaultOptions, options));
    },
};