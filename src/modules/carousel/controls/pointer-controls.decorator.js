import dispatcher from '../../dispatcher.js';
import { attach, detach } from '../../utils/decorator.utils.js';

const name = 'pointer-controls';

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._name = name;
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this._enabled = false;

		this.enable = this.enable.bind(this);
		this.disable = this.disable.bind(this);
		this._handlePointerStart = this._handlePointerStart.bind(this);
		this._handlePointerMove = this._handlePointerMove.bind(this);
		this._handlePointerEnd = this._handlePointerEnd.bind(this);
	}

	_handlePointerStart(e) {
		let x, y;

		this._parent.eventEmitter.dispatch({
			type: "pointer-start"
		});

		this._startEvent = e;
		this._dragging = false;

		if (e.touches) {
			if (e.touches.length !== 1) {
				return false;
			}
			x = e.touches[0].clientX;
			y = e.touches[0].clientY;

			this._touch = true;
		} else {
			x = e.clientX;
			y = e.clientY;

			this._touch = false;
		}

		this._dragging = true;

		this._parent._dragging = true;
		this._parent.style.cursor = 'grabbing';
		this._parent._pointerDelta = 0;
		this._parent.classList.add('grabbing');

		this._vertical = undefined;

		this._pointerStartPosition = {
			x: x,
			y: y,
		};
		this._parent._pointerX = x;
		this._lastPointer = x;
	}

	_handlePointerMove(e) {
		let x, y;
		let delta;
		let d;

		// if (!this._lastPointer) return;

		// this._parent._dragging = true;

		if (!this._dragging) {
			return false;
		}

		if (e.touches) {
			if (e.touches.length !== 1) {
				return false;
			}

			x = e.touches[0].clientX;
			y = e.touches[0].clientY;

			this._touch = true;
		} else {
			x = e.clientX;
			y = e.clientY;

			this._touch = false;
		}

		d = {
			x: this._pointerStartPosition.x - x,
			y: this._pointerStartPosition.y - y,
		};

		if (this._vertical === undefined && d.x !== 0 && d.y !== 0) {
			this._vertical = Math.abs(d.x) < Math.abs(d.y);
		}

		if (this._vertical) {
			this._parent.style.touchAction = 'pan-y';
			this._delta = 0;
			this._parent._pointerDelta = 0;

			return false;
		} else {
			if (this._touch) {
				e.preventDefault();
				e.stopPropagation();
			}

			this._parent.style.touchAction = 'none';
		}

		this._delta = this._lastPointer - x;
		this._lastPointer = x;

		this._parent._pointerX = x;

		return false;
	}

	_handlePointerEnd() {
		this._parent.eventEmitter.dispatch({
			type: "pointer-end"
		});

		this._parent._dragging = false;
		this._parent.style.touchAction = 'pan-y';
		this._parent.style.cursor = 'grab';
		this._parent.classList.remove('grabbing');

		this._parent._inertion = 0;
		this._lastPointer = null;
		this._firstPointer = null;
		this._dragging = false;

		// this._parent.removeEventListener('touchmove', this._handlePointerMove);
		// this._parent.removeEventListener('touchend', this._handlePointerEnd);
		// this._parent.removeEventListener('mousemove', this._handlePointerMove);
		// this._parent.removeEventListener('mouseup', this._handlePointerEnd);
		// this._parent.removeEventListener('mouseleave', this._handlePointerEnd);
	}

	enable() {
		if (this._enabled) return;
		this._enabled = true;

		this._parent.style.cursor = 'grab';
		this._parent.style.MozUserSelect = this._parent.style.userSelect =
			'none';
		this._parent._container.style.MozUserSelect = this._parent._container.style.userSelect =
			'none';
		this._parent.style.userSelect = 'none';
		this._parent.style.webkitTouchCallout = 'none';
		this._parent.style.touchAction = 'pan-y';

		this._parent.addEventListener('touchstart', this._handlePointerStart, { passive: false });
		this._parent.addEventListener('mousedown', this._handlePointerStart, { passive: false });
		this._parent.addEventListener('touchmove', this._handlePointerMove, {
			passive: false,
		});
		this._parent.addEventListener('touchend', this._handlePointerEnd, {
			passive: false,
		});
		this._parent.addEventListener('mousemove', this._handlePointerMove, { passive: false });
		this._parent.addEventListener('mouseup', this._handlePointerEnd, { passive: false });
		this._parent.addEventListener('mouseleave', this._handlePointerEnd, { passive: false });
	}

	disable() {
		if (!this._enabled) return;
		this._enabled = false;

		this._parent.style.cursor = '';
		this._parent.style.MozUserSelect = this._parent.style.userSelect =
			'';
		this._parent._container.style.MozUserSelect = this._parent._container.style.userSelect =
			'';
		this._parent.style.userSelect = '';
		this._parent.style.webkitTouchCallout = '';
		this._parent.style.touchAction = '';

		this._parent.removeEventListener(
			'touchstart',
			this._handlePointerStart
		);
		this._parent.removeEventListener('mousedown', this._handlePointerStart);
		this._parent.removeEventListener('touchmove', this._handlePointerMove);
		this._parent.removeEventListener('touchend', this._handlePointerEnd);
		this._parent.removeEventListener('mousemove', this._handlePointerMove);
		this._parent.removeEventListener('mouseup', this._handlePointerEnd);
		this._parent.removeEventListener('mouseleave', this._handlePointerEnd);
	}

	init() {}

	destroy() {
		if (this._enabled) {
			this._parent.removeEventListener(
				'touchstart',
				this._handlePointerStart
			);
			this._parent.removeEventListener(
				'mousedown',
				this._handlePointerStart
			);
			this._parent.removeEventListener(
				'touchmove',
				this._handlePointerMove
			);
			this._parent.removeEventListener(
				'touchend',
				this._handlePointerEnd
			);
			this._parent.removeEventListener(
				'mousemove',
				this._handlePointerMove
			);
			this._parent.removeEventListener('mouseup', this._handlePointerEnd);
			this._parent.removeEventListener(
				'mouseleave',
				this._handlePointerEnd
			);
		}
	}
}

export default {
	attach: (parent, options) => {
		attach(Decorator, parent, Object.assign({}, defaultOptions, options));
	},
	detach: (parent, options) => {
		detach(parent, Object.assign({}, defaultOptions, options));
	},
};