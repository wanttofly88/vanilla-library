import dispatcher from '../../dispatcher.js';
import {attach, detach} from '../../utils/decorator.utils.js';

const name = 'tab-controls';

const defaultOptions = {
	name: name
};

class Decorator {
	constructor(parent, options) {
		this._name = name;
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this._enabled = false;

		this._handleKeyboard = this._handleKeyboard.bind(this);
		this.enable = this.enable.bind(this);
		this.disable = this.disable.bind(this);
	}

	_handleKeyboard(e) {
		let activeElement;
		let parent = this._parent;
		let pointerPosition = this._parent._pointerPosition;

		if (e.type === 'keyboard:tab' || e.type === 'keyboard:shift-tab') {
			setTimeout(function () {
				activeElement = document.activeElement;

				if (!parent.contains(activeElement)) {
					return;
				}

				parent._elements.forEach(function (el, index) {
					if (el === activeElement || el.contains(activeElement)) {
						// add tab handler here
					}
				});
			}, 0);
		}
	}

	enable() {
		if (this._enabled) return;
		this._enabled = true;

		dispatcher.subscribe('keyboard', this._handleKeyboard);
	}

	disable() {
		if (!this._enabled) return;
		this._enabled = false;

		dispatcher.unsubscribe('keyboard', this._handleKeyboard);
	}

	init() {}

	destroy() {
		if (this._enabled) {
			dispatcher.unsubscribe('keyboard', this._handleKeyboard);
		}
	}
}

export default {
    attach: (parent, options) => {
        attach(Decorator, parent, Object.assign({}, defaultOptions, options));
    },
    detach: (parent, options) => {
        detach(parent, Object.assign({}, defaultOptions, options));
    },
};