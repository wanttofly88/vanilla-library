import dispatcher from '../dispatcher.js';
import EventEmitter from '../utils/EventEmitter.js';

let eventEmitter = new EventEmitter();
var items = {};

let _handleEvent = function (e) {
	if (e.type === 'carousel:add') {
		if (items[e.id]) return;
		items[e.id] = {
			id: e.id,
			interactive: false,
		};

		eventEmitter.dispatch();
	}

	if (e.type === 'carousel:interactive-enable') {
		if (!items[e.id] || items[e.id].interactive) return;

		items[e.id].interactive = true;

		eventEmitter.dispatch();
	}

	if (e.type === 'carousel:interactive-disable') {
		if (!items[e.id] || !items[e.id].interactive) return;
		items[e.id].interactive = false;

		eventEmitter.dispatch();
	}

	if (e.type === 'carousel:remove') {
		if (!items[e.id]) return;
		delete items[e.id];
	}
};

let getData = function () {
	return items;
};

let _init = function () {
	dispatcher.subscribe(_handleEvent);
};

_init();

export default {
	subscribe: eventEmitter.subscribe.bind(eventEmitter),
	unsubscribe: eventEmitter.unsubscribe.bind(eventEmitter),
	getData: getData,
};
