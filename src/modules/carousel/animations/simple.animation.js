import { simpleTween } from "../../utils/animation.utils.js";
import { attach, detach } from "../../utils/decorator.utils.js";
import createLoop from "../../utils/loop.utils/createLoop.js";
import { simpleEase } from "../../utils/ease.utils.js";

let name = "simple-animation";

const defaultOptions = {
    name: name,
    easeing: 14,
    edgeEasing: 5,
    interpolation: simpleEase
};

// Функция коррекции скорости, чтобы карусель остановилась ровно на каком-то элементе, выровненном по левому краю
function correctionFunction(
    { speed, position, sizes, previousCorrection, mode },
    options
) {
    let prognosedIteration = 0;

    // Прогнозирование примерного конечного положения остановки. Для вычисления коррекции.
    let prognosedSpeed = speed;
    let prognosedEndPosition = position + prognosedSpeed;

    let segmentWidth = sizes.itemWidth + sizes.gapWidth;

    if (mode === "simple") {
        if (
            position > 0 ||
            position < -(sizes.totalWidth - sizes.containerWidth)
        ) {
            // Не корректировать, если карусель вышла за пределы
            return 0;
        }
    }

    while (Math.abs(prognosedSpeed) >= 0.02 || prognosedIteration < 100) {
        prognosedIteration++;
        prognosedSpeed = options.interpolation(
            prognosedSpeed,
            0,
            options.easeing
        );
        prognosedEndPosition -= prognosedSpeed;
    }

    let deviation = prognosedEndPosition % segmentWidth;

    if (deviation > segmentWidth * 0.5) {
        deviation = -segmentWidth + deviation;
    } else if (deviation < -segmentWidth * 0.5) {
        deviation = segmentWidth + deviation;
    }

    if (Math.abs(deviation) < 0.1) deviation = 0;

    return options.interpolation(
        previousCorrection,
        deviation,
        options.easeing * 2
    );
}

// Функция возвращения, если карусель вышла за границы.
function edgeFuntion({ position, sizes }, options) {
    let newPosition;

    if (position > 0) {
        newPosition = options.interpolation(position, 0, options.edgeEasing);
    } else if (position < -(sizes.totalWidth - sizes.containerWidth)) {
        newPosition = options.interpolation(
            position,
            -(sizes.totalWidth - sizes.containerWidth),
            options.edgeEasing
        );
    } else {
        newPosition = position;
    }

    return newPosition;
}

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this._speed = 0;
        this._delta = 0;
        this._roughPosition = 0;
        this._previousRoughPosition = this._roughPosition;
        this._previousPosition = null;
        this._inertionCorrection = 0;

        this.loop = this.loop.bind(this);
    }

    moveOne(direction) {
        let carouselPointerPosition = this._parent._positionValueArray.get({
            name: "mainPosition",
        });

        let sizes = this._parent._sizes;

        let segmentWidth = sizes.itemWidth + sizes.gapWidth;
        let self = this;

        this._isOverrideAnimation = true;
		
		simpleTween({
			from: 0,
			to: segmentWidth * direction,
			ease: "ease"
		}, (tick) => {
			this._roughPosition += tick.delta;
			this._parent._positionValueArray.set({
				name: "mainPosition",
				value: this._roughPosition,
			});
			this._parent.updatePosition();
		}, () => {
			this._isOverrideAnimation = false;
		});
        // не написан. сдвиг на 1 экран, для клавиатуры или диспатчера
    }

    moveBy() {
        
    }

    moveTo(index) {
        // не написан. переход к конкретному элементу
    }

    loop() {
		// цикл для инерции драга

        // данные доступные из родителя
        let carouselPointerPosition = this._parent._positionValueArray.get({
            name: "mainPosition",
        });
        let isDragActive = this._parent._dragging;
        let pointerPositionX = this._parent._pointerX;
        let sizes = this._parent._sizes;
        //

        let newPosition;
        // если запущена другая анимация, ничего не делать в цикле
        if (!this._isOverrideAnimation) {
            if (isDragActive) {
                if (this._previousPosition === null) {
                    this._previousPosition = pointerPositionX;
                }
                this._speed = this._previousPosition - pointerPositionX;
                this._previousPosition = pointerPositionX;
            } else {
                this._previousPosition = null;
                this._speed = this._options.interpolation(
                    this._speed,
                    0,
                    this._options.easeing
                );
            }

            if (this._parent._options.correction && !isDragActive) {
                // Дополнительная функция коррекции. Чтобы после скролла элемент остановился на левой грани сегмента
                this._inertionCorrection = correctionFunction(
                    {
                        speed: this._speed,
                        position: carouselPointerPosition,
                        sizes: sizes,
                        previousCorrection: this._inertionCorrection,
                        mode: this._parent._options.mode,
                    },
                    this._options
                );
            } else {
                this._inertionCorrection = 0;
            }

            this._roughPosition =
                this._roughPosition -
                this._speed -
                this._inertionCorrection / 20;

            if (!isDragActive && this._parent._options.mode === "simple") {
                this._roughPosition = edgeFuntion(
                    {
                        position: this._roughPosition,
                        sizes: sizes,
                    },
                    this._options
                );
            }

            newPosition = this._roughPosition;

            if (Math.abs(newPosition - carouselPointerPosition) >= 0.1) {
                this._parent._positionValueArray.set({
                    name: "mainPosition",
                    value: newPosition,
                });
                this._parent.updatePosition();
            }
        }

        this._parent._delta = this._roughPosition - this._previousRoughPosition;
        this._previousRoughPosition = this._roughPosition;
    }

    init() {
        this._parent._positionValueArray.push({
            name: "mainPosition",
            value: 0,
		});

		this._raf = createLoop(this.loop);
		setTimeout(() => {
			this._raf.start();
		}, 0);
    }

    destroy() {
		this._raf.kill();
    }
}

export default {
    attach: (parent, options) => {
        attach(Decorator, parent, Object.assign({}, defaultOptions, options));
    },
    detach: (parent, options) => {
        detach(parent, Object.assign({}, defaultOptions, options));
    },
};
