import { attach, detach, update } from "../../utils/decorator.utils.js";
import { createLoop } from "../../utils/loop.utils.js";
import { PowerEase } from "../../utils/ease.utils.js";
import { ValueArray } from "../../utils/array.utils.js";
import { simpleTween } from "../../utils/animation.utils.js";

const name = "card-animation"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.loop = this.loop.bind(this);
        this.handleEvent = this.handleEvent.bind(this);
        this.updatePosition = this.updatePosition.bind(this);

        this._ease = new PowerEase({
            ease: 10,
            power: 1
        });

        this._delta = 0;

        this._rotationArray = new ValueArray();
        this._rotationArray.push({
            name: "move",
            value: {
                x: 0,
                y: 0,
                z: 0
            }
        });

        this._scaleArray = new ValueArray();
        this._scaleArray.push({
            name: "pointer",
            value: 1
        });
    }

    init() {
        this._parent.eventEmitter.subscribe(this.handleEvent);

        this._raf = createLoop(this.loop);
        this._raf.start();
    }

    destroy() {
        this._raf.kill();
    }

    loop() {
        let delta = Math.min(20, Math.max(-20, this._parent._delta));
        this._delta = this._ease.ease(delta);
        if (Math.abs(this._delta) < 0.01) {
            if (this._delta === 0) return;
            this._delta = 0;
        }

        this._rotationArray.set({
            name: "move",
            value: {
                x: 0,
                y: this._delta,
                z: 0
            }
        });

        this.updatePosition();
    }

    updatePosition() {
        let elements = this._parent.getElementsByClassName('item');
        let rotation = this._rotationArray.sum();
        let scale = this._scaleArray.multiply();

        Array.prototype.forEach.call(elements, (element) => {
            element.style.transform = `rotateY(${rotation.y}deg) scale(${scale})`;
        });
    }

    handleEvent(e) {
        if (e.type === "pointer-start") {
            if (this._tween) this._tween.kill();

            this._tween = simpleTween({
                from: this._scaleArray.get({ name: "pointer" }),
                to: 0.95,
                duration: 0.3,
                ease: "ease"
            }, (tick) => {
                if (tick.delta === 0) return;

                this._scaleArray.set({
                    name: "pointer",
                    value: tick.value
                });

                this.updatePosition();
            });
        }

        if (e.type === "pointer-end") {
            if (this._tween) this._tween.kill();

            this._tween = simpleTween({
                from: this._scaleArray.get({ name: "pointer" }),
                to: 1,
                duration: 0.6,
                ease: "ease"
            }, (tick) => {
                if (tick.delta === 0) return;

                this._scaleArray.set({
                    name: "pointer",
                    value: tick.value
                });

                this.updatePosition();
            });
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, Object.assign({}, defaultOptions, options));
    },
};
