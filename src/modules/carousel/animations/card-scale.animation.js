import { attach, detach, update } from "../../utils/decorator.utils.js";
import { createLoop } from "../../utils/loop.utils.js";
import { PowerEase } from "../../utils/ease.utils.js";
import { ValueArray } from "../../utils/array.utils.js";
import { simpleTween } from "../../utils/animation.utils.js";

const name = "card-animation"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.handleEvent = this.handleEvent.bind(this);
        this.updatePosition = this.updatePosition.bind(this);

        this._scaleArray = new ValueArray();
        this._scaleArray.push({
            name: "pointer",
            value: 1
        });
    }

    init() {
        this._parent.eventEmitter.subscribe(this.handleEvent);
    }

    destroy() {
        this._parent.eventEmitter.unsubscribe(this.handleEvent);
    }

    updatePosition() {
        let elements = this._parent.getElementsByClassName('item');
        let scale = this._scaleArray.multiply();

        Array.prototype.forEach.call(elements, (element) => {
            element.style.transform = `scale(${scale})`;
        });
    }

    handleEvent(e) {
        if (e.type === "pointer-start") {
            if (this._tween) this._tween.kill();

            this._tween = simpleTween({
                from: this._scaleArray.get({ name: "pointer" }),
                to: 0.95,
                duration: 0.3,
                ease: "ease"
            }, (tick) => {
                if (tick.delta === 0) return;

                this._scaleArray.set({
                    name: "pointer",
                    value: tick.value
                });

                this.updatePosition();
            });
        }

        if (e.type === "pointer-end") {
            if (this._tween) this._tween.kill();

            this._tween = simpleTween({
                from: this._scaleArray.get({ name: "pointer" }),
                to: 1,
                duration: 0.6,
                ease: "ease"
            }, (tick) => {
                if (tick.delta === 0) return;

                this._scaleArray.set({
                    name: "pointer",
                    value: tick.value
                });

                this.updatePosition();
            });
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, Object.assign({}, defaultOptions, options));
    },
};
