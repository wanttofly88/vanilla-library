import dispatcher from "../dispatcher.js";
import carouselStore from "./carousel.store.js";
import resizeStore from "../resize/resize.store.js";
import { ValueArray } from "../utils/array.utils.js";
import { FunctionArray } from "../utils/array.utils.js";
import { datasetToOptions } from "../utils/component.utils.js";
import EventEmitter from "../utils/EventEmitter.js";

import focusControlsDecorator from "./controls/focus-controls.decorator.js";
import tabControlsDecorator from "./controls/tab-controls.decorator.js";
import pointerControlsDecorator from "./controls/pointer-controls.decorator.js";

import continuousResizeDecorator from "./resize/continuous-resize.decorator.js";
import simpleResizeDecorator from "./resize/simple-resize.decorator.js";

import simpleAnimation from "./animations/simple.animation.js";
import cardRotationAnimation from "./animations/card-rotation.animation.js";
import cardScaleAnimation from "./animations/card-scale.animation.js";

const defaultOptions = {
	resize: "none", // auto || none - авторесайзит элементы, например если нужно 3 в строку
	mode: "simple", // continuous || simple - режим скролла (continuous - бесконечная карусель, simple - ограниченная по краям)
	animation: "default", // название анимации
	correction: false, // автоматический доскролл карусели к левой границе
	align: "left", // left || center выравнивание активного элемнта,
	interactive: "true", // 'true' || 'auto' || 'false' включено ли перетягивание элементов и управдение с клавииатуры. 'auto' будет выключать при необходимости, например если в карусели недостаточно элементов
};

const decorators = [
	focusControlsDecorator, // позволяет ставить фокус на карусель и управлять стрелочками
	tabControlsDecorator, // скроллит карусель к нужному элементу, когда человек ставит фокус на дочерний интерактивный элемент (кнопки на слайдах)
	pointerControlsDecorator, // тач и драг
];

const animations = {
	"default": [simpleAnimation, cardScaleAnimation],
	"rotation": [simpleAnimation, cardRotationAnimation],
}

const resizeDecorators = {
	simple: simpleResizeDecorator, // модуль ресайза для бесконечной карусели
	continuous: continuousResizeDecorator, // дефолтный модуль ресайза для карусели с границами
};

// для задания кастомного смещения сделать
// this._positionValueArray.push({name: 'имя_ключа', value: значение});
// например если нужен отступ от края

// <carousel-slider
//	data-id="partners-carousel-1"	- id такой же как у контролов
//	class="..."
//	data-gap="30"	- расстояние между элементами
//	data-maxWidth=315	- максимальная ширина элемента (приоритет над следующим аттрибутом)
//	data-perWrapper="320:1;640:2;1200:3;1600:4">	- сколько элементов на каком разрешении будет находиться в слайдере
//		<div class="carousel-container">
//			<div class="item">...</div>
//			...
//			<div class="item">...</div>
//		</div>
// </carousel-slider>

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._options = datasetToOptions(this.dataset, defaultOptions);
		this._positionValueArray = new ValueArray();
		this._enableFlagsArray = new FunctionArray();
		this._gsapAnimations = [];
		this._pointerDelta = 0;
		this._pointerDirection = 0;
		this._interactive = false;
		this._delta = 0;

		// заполняются в ресайз декораторе
		this._sizes = {
			containerWidth: null,
			itemWidth: null,
			gapWidth: null,
			totalWidth: null,
		};

		this.eventEmitter = new EventEmitter();

		this.handleResize = this.handleResize.bind(this);
		this.updatePosition = this.updatePosition.bind(this);
		this.handleDispatcher = this.handleDispatcher.bind(this);
		this.disable = this.disable.bind(this);
		this.enable = this.enable.bind(this);
		this.checkInteractiveFlags = this.checkInteractiveFlags.bind(this);
	}

	connectedCallback() {
		this._id = this.getAttribute("data-id");

		this._container = this.getElementsByClassName("carousel-container")[0];
		this.classList.add("active");

		this._elements = this.getElementsByClassName("item");

		this._total = this._elements.length;
		this._elements = Array.prototype.slice.call(this._elements);

		this._elements.forEach((el, index) => {
			el.setAttribute("data-index", index);
			el.classList.add("index-" + index);
		});

		this.setAttribute("draggable", false);

		resizeDecorators[this._options.mode].attach(this, this._options);

		decorators.forEach((d) => {
			d.attach(this);
		});

		animations[this._options.animation].forEach((a) => {
			a.attach(this);
		});

		dispatcher.dispatch({
			type: "carousel:add",
			id: this._id,
		});

		this.handleResize();
		resizeStore.subscribe(this.handleResize);
		dispatcher.subscribe(this.handleDispatcher);

		this._previousPosition = this._positionValueArray.sum();

		this._enableFlagsArray.push(() => {
			return (
				this._options.interactive === "true" ||
				this._options.interactive === "auto"
			);
		});

		this.checkInteractiveFlags();
	}

	disconnectedCallback() {
		resizeStore.unsubscribe(this.handleResize);
		dispatcher.unsubscribe(this.handleDispatcher);

		decorators.forEach((d) => {
			d.detach(this);
		});

		resizeDecorators[this._options.mode].detach(this);

		this._gsapAnimations.forEach((a) => a.kill());

		dispatcher.dispatch({
			type: "carousel:remove",
			id: this._id,
		});
	}

	checkInteractiveFlags() {
		let flagsResult = this._enableFlagsArray.boolReduce();
		if (flagsResult && !this._interactive) {
			this.enable();
		} else if (!flagsResult && this._interactive) {
			this.disable();
		}
	}

	enable() {
		if (this._interactive) return;
		this._interactive = true;

		if (this._decorators) {
			Object.values(this._decorators).map((d) =>
				d.enable ? d.enable() : null
			);
		}

		dispatcher.dispatch({
			type: "carousel:interactive-enable",
			id: this._id,
		});
	}

	disable() {
		if (!this._interactive) return;
		this._interactive = false;

		if (this._decorators) {
			Object.values(this._decorators).map((d) =>
				d.disable ? d.disable() : null
			);
		}

		dispatcher.dispatch({
			type: "carousel:interactive-disable",
			id: this._id,
		});
	}

	updatePosition() {
		let pos = this._positionValueArray.sum();
		let originalWidth = this._sizes.totalWidth;

		// зацикливаем в случае бесконечной карусели
		if (this._options.mode === "continuous") {
			while (pos < -originalWidth) {
				pos += originalWidth;
			}
			while (pos > 0) {
				pos -= originalWidth;
			}
		}

		this._container.style.transform = "translate3d(" + pos + "px, 0, 0)";
	}

	handleResize() {
		let styles = getComputedStyle(this._elements[0]);
		this._gap = parseInt(styles.marginRight);

		if (this._decorators[this._options.mode + "-resize"]) {
			this._decorators[this._options.mode + "-resize"].resize();
		}
	}

	handleDispatcher(e) {
		if (e.type === "carousel:next" && e.id === this._id) {
			this._decorators[this._options.animation + "-animation"].moveBy({
				direction: 1,
				by: e.by,
			});
		}
		if (e.type === "carousel:prev" && e.id === this._id) {
			this._decorators[this._options.animation + "-animation"].moveBy({
				direction: -1,
				by: e.by,
			});
		}
		if (e.type === "carousel:to" && e.id === this._id) {
			this._decorators[this._options.animation + "-animation"].moveTo(
				e.to
			);
		}
	}
}

customElements.define("carousel-component", ElementClass);

export default ElementClass;