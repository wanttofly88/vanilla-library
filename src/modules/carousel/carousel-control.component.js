import dispatcher from '../dispatcher.js';
import carouselStore from './carousel.store.js';
import {datasetToOptions} from '../utils/component.utils.js';

const defaultOptions = {
    __namespace: '',
    id: 'null', // id карусели 
    to: 'null',  // 'next' || 'prev' || <number> - куда скроллить. Вперед - назад или к определенному номеру. null - ничего не делать.
    by: '100%', // '1' || '10%' - скролить по элементам или взависимости от ширины контейнера. Если в строке пристутствует процент, то будет в процентах, иначе - количество элементов.
    enabled: 'auto' // 'false' || 'true' || 'auto' - всегда включены, выключены или в зависимости от интерактивности карусели
}

let _handleCarouselStore = function() {
    let carouselData = carouselStore.getData()[this._options.id];
    if (!carouselData) return;

    let carouselInteractive = carouselData.interactive;
    
    if (carouselInteractive &&
        !this._enabled) {
        
        this._enabled = true;
        this.removeAttribute('disabled');
        this.addEventListener('click', this.handleCarouselClick);
    } else if (!carouselInteractive &&
        this._enabled) {
        
        this._enabled = false;
        this.setAttribute('disabled', '');
        this.removeEventListener('click', this.handleCarouselClick);
    }
}

let _handleCarouselClick = function() {
    if (this._options.to === 'next') {
        dispatcher.dispatch({
            type: 'carousel:next',
            id: this._options.id,
            by: this._options.by
        });
    } else if (this._options.to === 'prev') {
        dispatcher.dispatch({
            type: 'carousel:prev',
            id: this._options.id,
            by: this._options.by
        });
    } else if (!isNaN(parseFloat(this._options.to))) {
        dispatcher.dispatch({
            type: 'carousel:to',
            id: this._options.id,
            to: parseFloat(this._options.to)
        });
    }
}

class ElementClass extends HTMLButtonElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
        this._options = datasetToOptions(this.dataset, defaultOptions);

        this._enabled = false;
        
        this.hanldeCarouselStore = _handleCarouselStore.bind(this);
        this.handleCarouselClick = _handleCarouselClick.bind(this);
	}
	connectedCallback() {
        if (this._options.enabled === 'true') {
            this._enabled = true;
            this.addEventListener('click', this.handleCarouselClick);
        } else {
            this._enabled = false;
            this.setAttribute('disabled', '');

            if (this._options.enabled === 'auto') {
                this.hanldeCarouselStore();
                carouselStore.subscribe(this.hanldeCarouselStore);
            }
        }
	}
	disconnectedCallback() {
        if (this._options.enabled === 'auto') {
            carouselStore.unsubscribe(this.hanldeCarouselStore);
        } 
	}
}

customElements.define('carousel-control', ElementClass, {
    extends: 'button'
});

export default ElementClass;