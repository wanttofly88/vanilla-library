import dispatcher from '../../dispatcher.js';
import resizeStore from '../../resize/resize.store.js';
import {datasetToOptions} from '../../utils/component.utils.js';
import {attach, detach} from '../../utils/decorator.utils.js';

let name = 'simple-resize';

const defaultOptions = {
	name: name,
	resize: 'auto', // auto || none - авторесайзит элементы, например если нужно 3 в строку
	// следующие опции не релевантны, если resize: 'none'
	maxWidth: 350,  // максимальный размер элемента.
	perWrapper: [{bp: 320, num: 1}, {bp: 640, num: 2}, {bp: 1000, num:3}], // элементов на ширину
}

let Decorator = function(parent, options) {
	this._parent = parent;

	this._options = datasetToOptions(parent.dataset, defaultOptions);
	this._options.itemsPerWrapperSettings = defaultOptions.perWrapper;

	this.resize = function() {
		let ww = resizeStore.getData().width;
		let perScreen;
		let self = this;

		this._total = this._parent._total;
		this._gap = this._parent._gap;

		if (this._options.resize === 'auto') {
			this._itemsPerWrapper = this._options.itemsPerWrapperSettings.reduce(function(prev, cur) {
				return cur.bp < ww ? cur: prev;
			});

			this._itemsPerWrapper = this._itemsPerWrapper.num;

			this._itemWidth = (this._parent.clientWidth - this._parent._options.gap * (this._itemsPerWrapper - 1)) / this._itemsPerWrapper;
			this._itemWidth = Math.floor(Math.min(this._maxWidth, this._itemWidth));

			this._parent._elements.forEach(function(el) {
				el.style.width = self._itemWidth + 'px';
			});
		} else if (this._options.resize === 'none') {
			this._itemWidth = this._parent._elements[0].clientWidth;
			this._itemsPerWrapper = Math.floor(this._parent.clientWidth / (this._itemWidth + this._gap)) + 1;
		} else {
			console.error('resize value is invalid');
		}

		let totalWidth = (this._itemWidth + this._gap) * (this._total - 1) + this._itemWidth;
		this._parent._container.style.width = totalWidth + 'px';

		this._parent._sizes = {
			containerWidth: this._parent.clientWidth,
			itemWidth: this._itemWidth,
			gapWidth: this._gap,
			totalWidth: totalWidth
		}
	}.bind(this);

	this.init = function() {
		this._maxWidth = parseInt(this._parent.getAttribute('data-maxWidth') ||
			this._options.maxWidth);

		let perWrapperString = this._parent.getAttribute('data-perWrapper');
		this._options.itemsPerWrapperSettings = perWrapperString ? perWrapperString.split(';').map(function(item) {
			let vals = item.split(':');
			return {
				bp: parseInt(vals[0]),
				num: parseInt(vals[1])
			}
		}): this._options.itemsPerWrapperSettings;
	}.bind(this);

	this.destroy = function() {
	}.bind(this);
}

export default {
    attach: (parent, options) => {
        attach(Decorator, parent, Object.assign({}, defaultOptions, options));
    },
    detach: (parent, options) => {
        detach(parent, Object.assign({}, defaultOptions, options));
    },
};