import { datasetToOptions } from '../utils/component.utils.js';

const defaultOptions = {
	__namespace: '',
	lat: 0,
	lng: 0
};

function getMobileOperatingSystem() {
	let userAgent = navigator.userAgent || navigator.vendor || window.opera;

	if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
		return 'iOS';
	} else if (userAgent.match(/Android/i)) {
		return 'Android';
	} else {
		return 'unknown';
	}
}

class ElementClass extends HTMLAnchorElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
	}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		let lat  = this._options.lat;
		let lng  = this._options.lng;
		let operatingSystem = getMobileOperatingSystem();

		if (operatingSystem.toLowerCase() === 'android') {
			this.setAttribute('href', 'https://maps.google.com/?q=' + lat + ',' + lng);
		} else if (operatingSystem.toLowerCase() === 'ios') {
			this.setAttribute('href', 'https://maps.apple.com/?q=' + lat + ',' + lng);
		} else {
			this.setAttribute('href', 'https://maps.google.com/?q=' + lat + ',' + lng);
		}
	}

	disconnectedCallback() {

	}
}

customElements.define('map-link', ElementClass, {
	extends: 'a'
});

export default ElementClass;