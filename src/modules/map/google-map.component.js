import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import mapStore from "./google-map.store.js";

import googleMapRoutesDecorator from "./decorators/google-map-routes.decorator.js";

const decorators = [googleMapRoutesDecorator];

const defaultOptions = {
	__namespace: "",
	lat: 0,
	lng: 0,
	zoom: 12,
	key: "",
	containerClass: "container",
	markerClass: "marker",
	type: "roadmap",
	background: "#dfdfdf",
	markerIco: "../../static/images/map-marker.svg",
	markerSize: [70, 88],
};

const styles = null;

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this.buildMap = this.buildMap.bind(this);
		this.handleMapStore = this.handleMapStore.bind(this);
		this.addMarker = this.addMarker.bind(this);

		this._loaded = false;
		this._markers = [];
	}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		const lang = document.documentElement.getAttribute("lang");

		this._container = this.getElementsByClassName(
			this._options.containerClass
		)[0];
		this._markerElements = Array.prototype.slice.call(
			this.getElementsByClassName(this._options.markerClass)
		);

		this._mode = this._options.mode;

		let self = this;

		if (!window._googleLoaderReady) {
			window._googleLoaderReady = () => {
				window.google.load("maps", "3", {
					callback: () => {
						dispatcher.dispatch({
							type: "map:apiLoaded",
						});
					},
					other_params: `key=${self._options.key}&language=${lang}`,
				});
			};

			this.loadAPI();
		}

		if (!this._container) {
			console.error(
				`element with class ${this._options.containerClass} was not found in map container`
			);
			return;
		}

		this.handleMapStore();
		mapStore.subscribe(this.handleMapStore);
	}

	disconnectedCallback() {
		mapStore.unsubscribe(this.handleMapStore);

		if (this._loaded) {
			decorators.forEach((d) => d.detach(this));
		}
	}

	handleMapStore() {
		let loaded = mapStore.getData().apiLoaded;
		if (loaded && !this._loaded) {
			this.buildMap();
		}

		if (!this._loaded) return;

		if (mapStore.getData().mode !== this._mode) {
			this._mode = mapStore.getData().mode;

			this._map.setMapTypeId(this._mode);
		}
	}

	loadAPI() {
		let script = document.createElement("script");
		script.type = "text/javascript";
		script.src = `https://www.google.com/jsapi?key=${this._options.key}&callback=_googleLoaderReady`;
		script.setAttribute("async", "");
		document.body.appendChild(script);
	}

	buildMap() {
		if (this._loaded) return;
		this._loaded = true;

		let config = {
			zoom: this._options.zoom,
			scrollwheel: false,
			disableDefaultUI: true,
			center: new google.maps.LatLng(
				this._options.lat,
				this._options.lng
			),
			gestureHandling: "cooperative",
			mapTypeId: this._options.mode,
		};

		this._map = new google.maps.Map(this._container, config);
		if (styles) {
			this._map.setOptions({ styles: styles });
		}

		this._container.style.background = this._options.background;

		if (this._markerElements) {
			this._markerElements.forEach((m, i) => {
				this.addMarker(m, i);
			});
		}

		decorators.forEach((d) => d.attach(this));
	}

	addMarker(m, i) {
		let lat = m.getAttribute("data-lat");
		let lng = m.getAttribute("data-lng");
		let title = m.getAttribute("data-title");

		if (!lat || !lng) {
			console.warn("marker lat or/and lng is missing");
		}

		let markerSize = this._options.markerSize;

		let ico = {
			url: this._options.markerIco,
			size: new google.maps.Size(markerSize[0], markerSize[1]),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(markerSize[0] / 2, markerSize[1]),
			scaledSize: new google.maps.Size(markerSize[0], markerSize[1]),
			labelOrigin: new google.maps.Point(100, 100),
		};

		let latLng = new google.maps.LatLng(lat, lng);

		let marker = new google.maps.Marker({
			position: latLng,
			map: this._map,
			icon: ico,
			// title: "marker",
			// label: { color: '#FFFFFF', fontSize: '14px', fonFamily: "SussieIntl, sans-serif", width: "100px", text: `${lat} ${lng}` },
		});

		let element = this._container;

		this._markers.push(marker);
	}
}

customElements.define("map-component", ElementClass);

export default ElementClass;