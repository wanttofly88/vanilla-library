import { attach, detach, update } from "../../utils/decorator.utils.js";

const name = "routes"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.addRoute = this.addRoute.bind(this);
    }

    init() {
        let routes = Array.prototype.slice.call(
            this._parent.getElementsByClassName("route")
        );

        this._directionsServices = [];
        this._directionsDisplays = [];

        routes.forEach(this.addRoute);
    }

    addRoute(routeElement) {
        let startElement = routeElement.getElementsByClassName('from')[0];
        let endElement = routeElement.getElementsByClassName('to')[0];

        let startPos = new google.maps.LatLng(parseFloat(startElement.getAttribute('data-lat')), parseFloat(startElement.getAttribute('data-lng')));
        let endPos = new google.maps.LatLng(parseFloat(endElement.getAttribute('data-lat')), parseFloat(endElement.getAttribute('data-lng')));

        let request = {
            origin: startPos,
            destination: endPos,
            travelMode: google.maps.TravelMode.WALKING
        };

        let strokeColor = routeElement.getAttribute('data-stroke-color');

        let directionsService = new google.maps.DirectionsService(),
            directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });

        this._directionsServices.push(directionsService);
        this._directionsDisplays.push(directionsDisplay);

        directionsDisplay.setMap(this._parent._map);

        if (!!strokeColor) {

            directionsDisplay.setOptions({
                polylineOptions: {
                    strokeColor: strokeColor,
                    strokeOpacity: .6,
                    strokeWeight: 6
                }
            });

        }

        let markerOptions = {
                position: startPos,
                map: this._parent._map
            },
            markerLabel,
            marker;

        markerLabel = routeElement.getAttribute('data-marker-label');

        if (!!markerLabel) markerOptions.label = markerLabel;

        directionsService.route(request, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {

                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(this._parent._map);

                markerOptions.position = response.routes[0].overview_path[0];
                marker = new google.maps.Marker(markerOptions);

            } else {
                console.error("Directions Request from " + startPos.toUrlValue(6) + " to " + endPos.toUrlValue(6) + " failed: " + status);
            }
        });

    }

    destroy() {}
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};