# Компоненты карты

---

## google-map.component

---

Компонента гугл карт

### Опции

**data-key**: **String** - ключ гугл карт. **Обязательно** запросить новый ключ при создании  
**data-lat**: **Number** - default **0**. Широта карты  
**data-lng**: **Number** - default **0**. Долгота карты  
**data-zoom**: **Number** - default **12**. Приближение карты  
**data-marker-class**: **String** - default **marker**. Класс элементов с данными маркеров  
**data-marker-ico**: **String** - путь к иконке маркера  
**data-marker-size**: **Array** - default: **[70, 88]** размер иконки маркера  
**data-container-class**: **String** - default **container**. Класс контейнера для карты маркера  
**data-type**: **String** - default **roadmap**. Тип карты. Доступные типы: roadmap, satellite, hybrid, terrain  
**data-background**: **String** - default **#dfdfdf**. Фон карты  

### Маркер

Для каждого элемента с классом, указанных в опциях карты (default: "marker"), будет создан новый маркер на карте.  
**data-lat**: **Number** - широта маркера  
**data-lng**: **Number** - долгота маркера  
**data-title**: **String** - текст маркера  

### Маршруты

Для каждого элемента с классом **route**, будет создан новый маршрут на карте.
И этого элемента:  
**data-marker-label**: **String** - наименование маршрута  
  
Дочерний элемент с классом **from** - исходная точка  
Дочерний элемент с классом **to** - исходная точка  
У 2-х вышеуказанных элементов:  
**data-lat**: **Number** - широта точки маршрута  
**data-lng**: **Number** - долгота точки маршрута  
**data-title**: **String** - текст маркера  


### Пример

```html 
<map-component class="map" 
	data-key="google_map_key"
	data-lat="55.747024" 
	data-lng="37.581523" 
	data-zoom="16" 
	data-marker-ico="/static/images/map-marker.svg">
    <div class="container"></div>
    <div class="marker" data-lat="55.747024" data-lng="37.581523"></div>
    <div class="route" data-marker-label="A">
        <div class="from" data-lat="55.747745" data-lng="37.583824"></div>
        <div class="to" data-lat="55.747165" data-lng="37.581660"></div>
    </div>
    <div class="route" data-stroke-color="red" data-marker-label="B">
        <div class="from" data-lat="55.748832" data-lng="37.582645"></div>
        <div class="to" data-lat="55.747165" data-lng="37.581660"></div>
    </div>
</map-component>
```

```CSS
.map {
	display: block;
}

.map .container {
	height: 480px;
}
```