import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import { offset } from "../utils/dom.utils.js";

const decorators = [];

const defaultOptions = {
	__namespace: "",
};

class ElementClass extends HTMLButtonElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this.handleClick = this.handleClick.bind(this);
	}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		this.addEventListener('click', this.handleClick);

		// code goes here
		decorators.forEach((d) => d.attach(this));
	}

	disconnectedCallback() {
		// code goes here
		decorators.forEach((d) => d.detach(this));
	}

	handleClick() {
		let map = this.closest(".map");
		if (!map) return;

		if (map.classList.contains("unlocked")) {
			map.classList.remove("unlocked");
		} else {
			map.classList.add("unlocked");

			dispatcher.dispatch({
				type: "scroll:to",
				position: offset(map).top - 100
			});
		}
	}
}

customElements.define("map-unlock", ElementClass, {
	extends: "button"
});

export default ElementClass;