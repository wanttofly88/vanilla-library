import deepCopy from './object.utils/deepCopy.js';
import mixObjects from './object.utils/mixObjects.js';
import subObjects from './object.utils/subObjects.js';
import addObjects from './object.utils/addObjects.js';

export { deepCopy, mixObjects, subObjects, addObjects };