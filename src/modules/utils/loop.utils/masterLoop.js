import {FunctionArray} from "../array.utils.js";
import animationConfig from "../../../config/animation.config.js";
import createLoop from "./createLoop.js";

class MasterLoop {
	constructor () {
		this.init = this.init.bind(this);
		this.loop = this.loop.bind(this);
		this.push = this.push.bind(this);
		this.remove = this.remove.bind(this);

		this._loop = createLoop(this.loop, true);
		this._loopArray = new FunctionArray();
		this._initialized = false;
	}

	init() {
		if (this._initialized) return;
		this._initialized = true;

		this._loop.start();
	}

	loop(multiplier) {
		this._loopArray.forEach((f) => {
			if (animationConfig.asyncMasterLoop) {
				setTimeout(function() {
					f(multiplier);
				}, 0);
			} else {
				f(multiplier);
			}
		});
	}

	push(func) {
		return this._loopArray.push(func);
	}

	remove(func) {
		return this._loopArray.remove(func);
	}
}

let masterLoop = new MasterLoop();
masterLoop.init();

window._masterLoop = masterLoop;

export default masterLoop;