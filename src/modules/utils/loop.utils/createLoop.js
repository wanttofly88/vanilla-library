import fps from "../animation.utils/fps.js";

class Loop {
    constructor(callback, master) {
        this.callback = callback;
        
        this.loop = this.loop.bind(this);
        this.start = this.start.bind(this);
        this.kill = this.kill.bind(this);

        this.master = master;

        this._targetFps = 60;
    }

    loop() {
        if (this._destroy) {
            return;
        }

        this.callback(this._targetFps / fps.get());

        if (!window._masterLoop || this.master) {
            requestAnimationFrame(this.loop);
        }
    }

    start() {
        this.loop();

        if (window._masterLoop && !this.master) {
            window._masterLoop.push(this.loop);
        }
    }

    kill() {
        this._destroy = true;

        if (window._masterLoop && !this.master) {
            window._masterLoop.remove(this.loop);
        }
    }
}

export default function(callback, master) {
    if (typeof callback !== "function") {
        console.error("callback has to be a function");
        return;
    }

    return new Loop(callback, master);
}