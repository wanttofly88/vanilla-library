const arrayToMap = <T extends { id: number | string }>(
    array: Array<T>
): Map<string | number, T> => {
    const map = new Map();

    array.forEach((item) => map.set(item.id, item));

    return map;
};

export default arrayToMap;
