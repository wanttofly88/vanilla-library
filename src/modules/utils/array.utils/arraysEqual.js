export default function(a, b) {
	if (a === b) return true;
	if (a == null || b == null) return false;
	if (a.length !== b.length) return false;

	let a1 = a.slice().sort();
	let b1 = b.slice().sort();

	for (var i = 0; i < a1.length; ++i) {
		if (a1[i] !== b1[i]) return false;
	}
	return true;
}