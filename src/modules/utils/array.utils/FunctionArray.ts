type Element = <T>(...args: Array<T>) => void | boolean;

type ArrayFunc = (value: Element, index: number, array: Array<Element>) => void;

type ReduceFunc = (
    previousValue: Element,
    currentValue: Element,
    currentIndex: number,
    array: Array<Element>
) => any;

interface FunctionArrayInterface {
    elements: Array<Element>;
    forEach(func: ArrayFunc): void;
    reduce(func: ReduceFunc): any;
    map(func: ArrayFunc): Array<unknown>;
    push(func: Element): void;
    remove(func: Element): void;
    boolReduce(): any;
}

class FunctionArray implements FunctionArrayInterface {
    elements: Array<Element>;

    constructor() {
        this.elements = [];
    }

    forEach(func: ArrayFunc): void {
        return this.elements.forEach(func);
    }

    reduce(func: ReduceFunc): Element {
        return this.elements.reduce(func);
    }

    map(func: ArrayFunc): Array<void | boolean> {
        return this.elements.map(func);
    }

    push(func: Element): void {
        this.elements.push(func);
    }

    remove(func: Element): void {
        this.elements = this.elements.filter(function (el) {
            return el !== func;
        });
    }

    boolReduce(): any {
        return this.elements.reduce((prev: Element, flag: Element): any => {
            return prev && flag();
        });
    }
}

export default FunctionArray;
