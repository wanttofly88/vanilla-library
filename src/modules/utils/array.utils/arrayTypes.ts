export type ArrayFunc = (
    value: unknown,
    index: number,
    array: unknown[]
) => void;
export type ReduceFunc = (
    previousValue: unknown,
    currentValue: unknown,
    currentIndex: number,
    array: Array<unknown>
) => unknown;
