// push({name: 'name', value: {x: 0, y: 0, z: 0})
// remove({name: 'name'})
// set({name: 'name', value: {x: 1, y: 1, x: 1})

import { ArrayFunc } from "./arrayTypes";

type vec3 = {
    [index: string]: number;
    x: number;
    y: number;
    z: number;
};

type arrayItem = {
    [index: string]: vec3 | string;
    name: string;
    value: vec3;
};

interface VectorArrayInterface {
    elements: Array<arrayItem>;
    forEach(func: ArrayFunc): void;
    push(value: arrayItem): void;
    remove(value: arrayItem): void;
    set(value: arrayItem): void;
    get(value: arrayItem): vec3 | null;
    summ(): vec3;
    multiply(): vec3;
}

class VectorArray implements VectorArrayInterface {
    elements: Array<arrayItem>;

    constructor() {
        this.elements = [];
    }

    forEach(func: ArrayFunc): void {
        return this.elements.forEach(func);
    }

    push(val: arrayItem): void {
        if (
            this.elements.find((el) => {
                return el.name === val.name;
            })
        ) {
            throw new Error(`element with name ${val.name} already exists`);
        }

        this.elements.push(val);
    }

    remove(val: arrayItem): void {
        this.elements = this.elements.filter(function (el) {
            return el.name !== val.name;
        });
    }

    set(val: arrayItem): void {
        const test: arrayItem | undefined = this.elements.find((el) => {
            return el.name === val.name;
        });

        if (test) {
            test.value = val.value;
        } else {
            throw new Error(`element with name ${val.name} is not found`);
        }
    }

    get(val: arrayItem): vec3 | null {
        if (!val) {
            throw new Error("no value given");
        } else {
            const element = this.elements.find((el) => {
                return el.name === val.name;
            });

            if (element) {
                return element.value;
            } else {
                return null;
            }
        }
    }

    summ(): vec3 {
        return this.elements
            .map((val) => {
                return val.value;
            })
            .reduce(
                (total, value) => {
                    return {
                        x: total.x + value.x,
                        y: total.y + value.y,
                        z: total.z + value.z,
                    };
                },
                { x: 0, y: 0, z: 0 }
            );
    }

    multiply(): vec3 {
        return this.elements
            .map((val) => {
                return val.value;
            })
            .reduce(
                function (total, value) {
                    return {
                        x: total.x * value.x,
                        y: total.y * value.y,
                        z: total.z * value.z,
                    };
                },
                { x: 1, y: 1, z: 1 }
            );
    }
}

export default VectorArray;
