import attach from './decorator.utils/attach.js';
import detach from './decorator.utils/detach.js';
import update from './decorator.utils/update.js';
import attachPromise from './decorator.utils/attachPromise.js';
import detachPromise from './decorator.utils/detachPromise.js';

export {
	attach,
	detach,
	update,
	attachPromise,
	detachPromise
}