const mapToArray = <T>(map: Map<string | number, T>): Record<number, T> => {
    return Array.from(map.values());
};

export default mapToArray;
