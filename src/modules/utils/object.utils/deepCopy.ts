type inputObject = Record<string | number, unknown>;

export default (original: unknown): unknown => {
    if (typeof original !== "object" || original === null) {
        return original;
    }

    const out: inputObject = Array.isArray(original) ? [] : {};

    for (const key in original) {
        const value = original[key];
        out[key] = deepCopyFunction(value);
    }

    return out;
};
