const hasOwnProperty = <T>(
    object: Record<string, T>,
    key: string
): boolean => {
    return Object.prototype.hasOwnProperty.call(object, key);
};

export default hasOwnProperty;
