import hasOwnProperty from "./hasOwnProperty";

const toFormData = (object : Record<string, string | Blob>) => {
    const data = new FormData();

    for (const key in object) {
        if (hasOwnProperty(object, key)) {
            data.append(key, object[key]);
        }
    }

    return data;
};

export default toFormData;
