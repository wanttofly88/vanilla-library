import camelize from "../string.utils/camelize";

const camelizeObjectProperties = (
    object: Record<string | number, unknown> | Array<any>,
    separator = "_"
): Record<string | number, unknown> => {
    const camelizedObject: Record<string | number, unknown> = {};

    Object.entries(object).forEach(([key, value]) => {
        const camelizedKey = camelize(key, separator);

        if (typeof value === "object" && value !== null) {
            if (value instanceof Array) {
                camelizedObject[camelizedKey] = Object.values(
                    camelizeObjectProperties(value, separator)
                );
            } else {
                camelizedObject[camelizedKey] = camelizeObjectProperties(
                    value,
                    separator
                );
            }
        } else {
            camelizedObject[camelizedKey] = value;
        }
    });

    return camelizedObject;
};

export default camelizeObjectProperties;
