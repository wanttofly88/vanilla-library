let detachPromise = function (parent, options) {
    let name = options.name;
    if (!name) {
        console.error('decorator has to provide "name" option');
        return;
    }

    if (!parent._decorators) return Promise.resolve();

    let decorator = parent._decorators[name];
    if (decorator) return Promise.resolve();

    return decorator.destroy()
    .then(() => {
    	delete parent._decorators[name];
    });
};

export default detachPromise;