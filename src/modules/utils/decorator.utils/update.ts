import { DecoratorClass, DecoratorOptions } from "./decorator.types";
import { Component } from "../component.utils/component.types";

const update = (
    parent: Component,
    options: DecoratorOptions
): DecoratorClass => {
    const name = options.name;

    if (!name) {
        throw new Error("decorator has to provide name option");
    }

    if (parent._decorators) {
        const decorator: DecoratorClass | undefined = parent._decorators[name];

        if (decorator) {
            decorator._options = Object.assign(decorator._options, options);
            return decorator;
        } else {
            throw new Error("decorator is not found");
        }
    } else {
        throw new Error("component doesnt have any decorators");
    }
};

export default update;
