import {
    Component,
    ComponentOptions,
} from "../component.utils/component.types";

export type DecoratorOptions = {
    [index: string]: any;
    name: string;
};

export interface DecoratorInterface {
    init(): void | Promise<void>;
    destroy(): void | Promise<void>;
    _parent: Component;
    _options?: DecoratorOptions;
}

export declare class DecoratorClass implements DecoratorInterface {
    _parent: Component;
    _options?: DecoratorOptions;

    constructor(parent: Component, options?: ComponentOptions);

    init(): void | Promise<void>;

    destroy(): void | Promise<void>;
}
