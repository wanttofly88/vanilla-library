let detach = function (parent, options) {
    let name = options.name;
    if (!name) {
        console.error('decorator has to provide "name" option');
        return;
    }
	
	if (!parent._decorators) return;

    let decorator = parent._decorators[name];
	if (!decorator) return;

    decorator.destroy();

    delete parent._decorators[name];
};

export default detach;