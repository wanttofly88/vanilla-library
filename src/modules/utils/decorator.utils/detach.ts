import { DecoratorOptions } from "./decorator.types";
import { Component } from "../component.utils/component.types";

const detach = (parent: Component, options: DecoratorOptions): void => {
    const name = options.name;

    if (!name) {
        throw new Error("decorator has to provide name option");
    }

    if (!parent._decorators) return;

    const decorator = parent._decorators[name];

    if (!decorator) return;

    decorator.destroy();

    delete parent._decorators[name];
};

export default detach;
