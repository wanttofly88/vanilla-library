import { DecoratorClass, DecoratorOptions } from "./decorator.types";
import { Component } from "../component.utils/component.types";

const attach = (
    Decorator: typeof DecoratorClass,
    parent: Component,
    options: DecoratorOptions
): DecoratorClass => {
    const name = options.name;

    if (!name) {
        throw new Error('decorator has to provide "name" option');
    }

    if (!parent._decorators) {
        parent._decorators = {};
    }
    if (!parent._decorators[name]) {
        parent._decorators[name] = new Decorator(parent, options);
        parent._decorators[name].init();
    }

    return parent._decorators[name];
};

export default attach;
