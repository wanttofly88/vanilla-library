import { DecoratorClass, DecoratorOptions } from "./decorator.types";
import { Component } from "../component.utils/component.types";

const detachPromise = (
    parent: Component,
    options: DecoratorOptions
): Promise<void> => {
    const name = options.name;

    if (!name) {
        throw new Error("decorator has to provide name option");
    }

    if (!parent._decorators) {
        return Promise.resolve();
    } else {
        const decorator: DecoratorClass | undefined = parent._decorators[name];

        if (!decorator) return Promise.resolve();

        return Promise.resolve(decorator.destroy()).then(() => {
            if (parent._decorators) delete parent._decorators[name];
        });
    }
};

export default detachPromise;
