// все свойства в css формате

// transition(element, 0.3, {
//		transform: 'translateX(' + 200 + 'px)',
//		opacity: 0
//});

// или можно массивами

// transition(element, [0.3, 0.4], {
//		transform: 'translateX(' + 200 + 'px)',
//		opacity: 0
// }, {
//		delay: [0, 0.3],
//		ease: ['ease-in', 'cubic-bezier(1, 0, 0.3, 1)']
// });

interface TransitionOptions {
    ease?: string | Array<string>;
    delay?: number | Array<number>;
}

const defaultOptions: TransitionOptions = {
    delay: 0,
    ease: "ease",
};

const isChrome =
    /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

const transition = <K extends keyof HTMLElementTagNameMap>(
    elements:
        | HTMLElement
        | Array<HTMLElement>
        | NodeListOf<HTMLElementTagNameMap[K]>,
    speed: number | Array<number>,
    styles: Record<string, string | number>,
    inputOptions: TransitionOptions = {}
): void => {
    if (!elements) return;

    const getStylePropertiesNames = (obj: Record<string, string>): string => {
        let propertiesNames = "";
        let i = 0;

        for (const key in obj) {
            if (i !== 0) {
                propertiesNames += ", ";
            }

            propertiesNames += key;
            i++;
        }
        return propertiesNames;
    };

    const getStylePropertiesValues = (
        arr: string | number | Array<string | number>,
        suffix = "",
        numStyles: number
    ): string => {
        let propertiesValues = "";
        let i;

        if (!Array.isArray(arr)) {
            arr = [arr];
        }
        for (i = arr.length - 1; i < numStyles; i++) {
            arr[i] = arr[arr.length - 1];
        }
        for (i = 0; i < numStyles; i++) {
            if (i !== 0) {
                propertiesValues += ", ";
            }
            propertiesValues += arr[i] + suffix;
        }

        return propertiesValues;
    };

    const parseEase = (
        ease: string
    ):string => {
        if (ease.indexOf("cubic-bezier") !== 0) {
            if (!window._vars || !window._vars.ease) {
                console.error("Ease is specified as a string, but global easings are missing. Try passing a bezier function or enabling the ease.config script.");
                return "";
            }

            return window._vars.ease.css[ease];
        } else {
            return ease;
        }
    };

    if (elements instanceof NodeList) {
        elements = Array.prototype.slice.call(elements);
    } else if (!Array.isArray(elements)) {
        elements = [elements];
    }

    // переводим все свойства в строки, чтобы потом можно было присваивать их стилям
    const stringStyles: { [index: string]: string } = {};

    Object.entries(styles).forEach(([key, value]) => {
        stringStyles[key] = value.toString();
    });

    const options = Object.assign(defaultOptions, inputOptions);

    if (Array.isArray(options.ease)) {
        options.ease = options.ease.map(parseEase);
    } else {
        options.ease = parseEase(options.ease ? options.ease : "default");
    }

    const numStyles = getStylePropertiesNames(stringStyles).split(" ")
        .length;

    elements.forEach((element) => {
        element.style.webkitTransitionProperty = element.style.transitionProperty = getStylePropertiesNames(
            stringStyles
        );

        element.style.webkitTransitionDuration = element.style.transitionDuration = getStylePropertiesValues(
            speed,
            "s",
            numStyles
        );

        element.style.webkitTransitionDelay = element.style.transitionDelay = getStylePropertiesValues(
            options.delay,
            "s",
            numStyles
        );

        element.style.webkitTransitionTimingFunction = element.style.transitionTimingFunction = getStylePropertiesValues(
            options.ease,
            "",
            numStyles
        );

        for (const property in stringStyles) {
            if (
                isChrome &&
                !Object.prototype.hasOwnProperty.call(
                    element.style,
                    property
                )
            ) {
                console.warn(
                    "style " + property + " does not exist on element"
                );
            } else {
                if (property === "transform") {
                    element.style.webkitTransform = element.style.transform =
                        stringStyles.transform;
                } else {
                    element.style.setProperty(
                        property,
                        stringStyles[property]
                    );
                }
            }
        }
    });
};

export default transition;
