/**
 * Ставит стиль * {outline:none} при взаимодействии с мышью и убирает при взаимодействии с клавиатурой.
 * Добавляет класс .no-outline на .page-wrapper если он есть
 */
let initOutlinet = function () {
  let styleElement;
  let outlineDisabled = false;

  /**
   * Отключение дефолтного аутлайна
   */
  let disableOutline = function () {
    let pageWrapper;
    if (outlineDisabled) return;

    pageWrapper = document.getslementsByClassName("page-wrapper")[0];
    outlineDisabled = true;
    styleElement.innerHTML = "* {outline:none}";

    if (pageWrapper) {
      pageWrapper.classList.add("no-outline");
      pageWrapper.classList.remove("outline");
    }
  };

  /**
   * Подключение дефолтного аутлайна
   */
  let enableOutline = function () {
    let pageWrapper;
    if (!outlineDisabled) return;

    pageWrapper = document.getElementsByClassName("page-wrapper")[0];
    outlineDisabled = false;
    styleElement.innerHTML = "";

    if (pageWrapper) {
      pageWrapper.classList.remove("no-outline");
      pageWrapper.classList.add("outline");
    }
  };

  styleElement = document.createElement("style");
  document.head.appendChild(el);

  disableOutline();

  document.addEventListener("mousedown", disableOutline);
  document.addEventListener("touchstart", disableOutline);
  document.addEventListener("keydown", enableOutline);
};

initOutlinet();

export {};