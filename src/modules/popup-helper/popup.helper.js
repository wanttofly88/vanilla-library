import dispatcher from "../dispatcher.js";
import popupStore from "../popup/popup.store.js";
import defaultAnimation from "./animations/default.animation.js";

import { getByDataId } from "../utils/dom.utils.js";
import { transition } from "../utils/animation.utils.js";
import { getFocusable } from "../utils/dom.utils.js";

const defaultOptions = {
	closeAllClass: "popup-close-all", // кнопка, закрывающая все попапы (например, меню)
	popupContentClass: "popup-inner", // класс внутреннего контента попапа, клик вне его закроет попап
};

// функция выбора анимации. Принимает предыдущий и текущий id.
// для закрытого попапа id === null
// требуется вернуть объект с анимациями открытия, закрытия и изменения

function getAnimation(previousId, currentId) {
	// пример
	// if (previousId === "letter-popup" || currentId === "letter-popup") {
	// 	return {
	// 		open: letterAnimation,
	// 		close: letterAnimation,
	// 		change: letterAnimation
	// 	}
	// }

	return {
		open: defaultAnimation,
		close: defaultAnimation,
		change: defaultAnimation
	}
}

// управление с клаывиатуры, таблок

let lastFocus = null;
let firstFocusable;
let lastFocusable;
let focusIndex = 0;
let focusable;
let wasActive = null;
let tabLockSubscribed = false;

let _lockFocus = function (e) {
	if (!focusable) return;

	if (e.type === "keyboard:tab") {
		e.event.preventDefault();
		focusIndex++;

		if (!focusable[focusIndex]) {
			focusIndex = 0;
		}

		focusable[focusIndex].focus({ preventScroll: true });
	}

	if (e.type === "keyboard:shift-tab") {
		e.event.preventDefault();
		focusIndex--;

		if (!focusable[focusIndex]) {
			focusIndex = focusable.length - 1;
		}

		focusable[focusIndex].focus({ preventScroll: true });
	}
};

let _tabLock = function () {
	let active = popupStore.getData().active;
	let popup = null;
	let first, last;
	let popupFocusable = document.getElementsByClassName("popup-focusable");
	popupFocusable = Array.prototype.slice.call(popupFocusable, 0);

	popup = getByDataId("popup-component", active);

	if (!popup) {
		console.warn("hm...");
		return;
	}

	lastFocus = document.activeElement;

	let closeButton = popup.getElementsByClassName(
		defaultOptions.closeClass
	)[0];
	let closeAllButton = document.getElementsByClassName(
		defaultOptions.closeAllClass
	)[0];

	focusable = getFocusable(popup);

	focusable = focusable.concat(popupFocusable);
	if (
		closeAllButton &&
		Array.prototype.indexOf.call(popupFocusable, closeButton) === -1
	) {
		focusable.push(closeAllButton);
	}

	focusable.sort(function (a, b) {
		let ta, tb;
		ta = a.tabIndex || 0;
		tb = b.tabIndex || 0;

		if (
			(a.tagName.toLowerCase() === "input" ||
				a.tagName.toLowerCase() === "textarea") &&
			b.tagName.toLowerCase() !== "input" &&
			b.tagName.toLowerCase() !== "input"
		) {
			return -1;
		}

		return ta - tb;
	});

	tabLockSubscribed = true;

	dispatcher.subscribe("keyboard", _lockFocus);
	focusable[0].focus({ preventScroll: true });
	focusIndex = 0;
};

let _tabUnlock = function () {
	if (!tabLockSubscribed) return;

	dispatcher.unsubscribe("keyboard", _lockFocus);

	if (lastFocus) {
		lastFocus.focus({ preventScroll: true });
	}
	lastFocus = null;
};

let _handleKeyboard = function (e) {
	let active = popupStore.getData().active;

	if (!active) return;

	if (e.type === "keyboard:esc") {
		dispatcher.dispatch({
			type: "popup:close-all",
		});
	}
};

let _handleClick = function (e) {
	let target = e.target;
	if (
		target.classList.contains(defaultOptions.popupContentClass) ||
		target.closest("." + defaultOptions.popupContentClass) ||
		target.closest("a") ||
		target.closest("button")
	) {
	} else {
		dispatcher.dispatch({
			type: "popup:close-all",
		});
	}
};

let _handlePopup = function () {
	const popupData = popupStore.getData();
	const active = popupData.active;
	const pw = document.getElementsByClassName("page-wrapper")[0];
	const popup = getByDataId("popup-component", active);

	let previousPopup = null;

	if (wasActive) {
		previousPopup = getByDataId("popup-component", wasActive);
	}

	const animationObject = {
		activeId: active,
		previousId: wasActive,
		activePopup: popup,
		previousPopup: previousPopup,
	};

	let currentAnimations = getAnimation(wasActive, active);

	if (active) {
		setTimeout(function () {
			document.addEventListener("click", _handleClick);
		}, 300);

		if (pw) {
			pw.classList.add("popup-active");
		}

		_tabLock();
		dispatcher.dispatch({
			type: "scroll:lock",
		});

		if (wasActive) {
			// change animation
			currentAnimations.change.change(animationObject);
		} else {
			// open animation
			currentAnimations.open.open(animationObject);
		}
	} else {
		document.removeEventListener("click", _handleClick);
		// close animation
		currentAnimations.close.close(animationObject);

		if (pw) {
			pw.classList.remove("popup-active");
		}

		setTimeout(function () {
			_tabUnlock();
		}, 300); // wierd bugfix..

		dispatcher.dispatch({
			type: "scroll:unlock",
		});
	}

	wasActive = active;
};

let init = function () {
	_handlePopup();
	popupStore.subscribe(_handlePopup);
	dispatcher.subscribe("keyboard", _handleKeyboard);
};

export default {
	init: init,
};