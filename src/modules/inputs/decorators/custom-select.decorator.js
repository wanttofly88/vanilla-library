import { attach, detach, update } from "../../utils/decorator.utils.js";
import { datasetToOptions } from "../../utils/component.utils.js";
import touchStore from "../../touch/touch.store.js";
import dispatcher from "../../dispatcher.js";
import * as setUtils from "../../utils/set.utils.js";

import dropDownDecorator from "../../drop-down/decorators/drop-down.decorator.js";

const name = "custom-select"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
    closeOutside: true, // закрытие при клике снаружи
    closeChoose: true, // закрытие при выборе элемента
    classClear: "clear", // класс кнопки сброса (кнопка не создается автоматически)

    classButton: "button input-select", // класс, который будет добавлен на кнопку открытия. Требуется хотя бы один.
    classOuter: "outer", // класс, который будет добавлен на внешний контейнер дроп-дауна. Требуется хотя бы один.
    classInner: "inner", // класс, который будет добавлен на внутренний контейнер дроп-дауна. Требуется хотя бы один.
    classElement: "option", // класс, который будет добавлен на элементы опций
    maxHeight: 300, // максимальная высота

    openDuration: 0.3, // скорость открытия в секнудах
    closeDuration: 0.45, // скорость закрытия в секнудах
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clear = this.clear.bind(this);
    }

    init() {
        this._options = datasetToOptions(this._parent.dataset, this._options);
        this._selectElement = this._parent.getElementsByTagName("select")[0];
        
        this.build();
        this.handleUpdate();
        this._selectElement.addEventListener("update", this.handleUpdate);
        this._selectElement.addEventListener("change", this.handleChange);

        dropDownDecorator.attach(this._parent, {
            closeOutside: this._options.closeOutside,
            classButton: this._options.classButton.split(" ")[0],
            classOuter: this._options.classOuter.split(" ")[0],
            classInner: this._options.classInner.split(" ")[0],
            maxHeight: this._options.maxHeight,
            openDuration: this._options.openDuration,
            closeDuration: this._options.closeDuration
        });
    }

    destroy() {
        dropDownDecorator.detach(this._parent);
    }

    build() {
        this._selectElement.classList.add("hidden");
        this._displayElement = document.createElement("button");
        this._displayElement.className = this._options.classButton;
        this._displayElement.setAttribute("type", "button");

        this._parent.appendChild(this._displayElement);

        this._parent.setAttribute("data-close", "auto");

        let outer = document.createElement("div");
        outer.className = this._options.classOuter;
        let inner = document.createElement("div");
        inner.className = this._options.classInner;

        this._clear = this._parent.getElementsByClassName(this._options.classClear)[0];
        if (this._clear) {
            this._clear.addEventListener("click", this.clear);
        }

        this._outer = outer;
        this._inner = inner;

        outer.appendChild(inner);
        this._parent.appendChild(outer);
    }

    getValues() {
        this._values = new Set();

        this._optionElements.forEach((op) => {
            if (op.selected && 
                op.value !== "" &&
                op.value !== "_") {
                this._values.add(op.value);
            }
        });
    }

    getTextByValues() {
        let text = "";

        if (this._values.size === 0) {
            if (this._defaultItem) {
                text = this._defaultItem.option.innerHTML;
            }
        } else {
            this._optionElements.forEach((op) => {
                if (this._values.has(op.value)) {
                    if (text !== "") {
                        text += ", ";
                    }
                    text += op.innerHTML;
                }
            });
        }

        return text;
    }

    setOptionsByValues() {
        if (this._values.size === 0) {
            this._parent.classList.remove("not-empty");
        } else {
            this._parent.classList.add("not-empty");
        }

        if (this._defaultItem) {
            if (this._values.size === 0) {
                this._defaultItem.element.classList.add("selected");
            } else {
                this._defaultItem.element.classList.remove("selected");
            }
        }

        this._items.forEach((item) => {
            if (item.default) return;

            if (this._values.has(item.value)) {
                item.element.classList.add("selected");
            } else {
                item.element.classList.remove("selected");
            }
        });
    }

    handleUpdate() {
        this._multi = this._selectElement.multiple;

        if (this._multi) {
            this._parent.classList.add("multi");
        } else {
            this._parent.classList.remove("multi");
        }

        this._optionElements = Array.prototype.slice.call(
            this._selectElement.options
        );

        this._inner.innerHTML = "";
        this._defaultItem = null;
        this._items = [];

        this.getValues();

        this._optionElements.forEach((op) => {
            let item = document.createElement("button");
            item.className = this._options.classElement;
            item.innerHTML = op.innerHTML;
            item.setAttribute("type", "button");
            item.setAttribute("data-value", op.value);

            let obj = {}

            if (op.value === "" ||
                op.value === "_") {
                item.classList.add("default");

                obj = {
                    element: item,
                    value: op.value,
                    option: op,
                    default: true
                }

                this._defaultItem = obj;

                this._items.push(obj);
            } else {
                obj = {
                    element: item,
                    option: op,
                    value: op.value
                }

                this._items.push(obj);
            }

            item.addEventListener("click", () => {
                this.handleClick(obj);
            });

            this._inner.appendChild(item);
        });

        let text = this.getTextByValues();
        this._displayElement.innerHTML = text;

        this.setOptionsByValues();

        this._parent.dispatchEvent(new Event("custom-select-update"));
    }

    handleChange() {
        this._prevValues = new Set(this._values);

        this.getValues();

        if (setUtils.equals(this._prevValues, this._values)) return;

        if (this._options.closeChoose) {
            dispatcher.dispatch({
                type: "drop-down:close",
                element: this._parent
            });
        }

        this.setOptionsByValues();

        let text = this.getTextByValues();
        this._displayElement.innerHTML = text;
    }

    handleClick(item) {
        let lastOption = null;

        if (item.default) {
            if (this._values.size > 0) {
                this._optionElements.forEach((op) => {
                    if (op !== item.default.option) {
                        op.removeAttribute("selected");
                    }
                });
            } else {
                return;
            }

            item.option.setAttribute("selected", "selected");
            lastOption = item.option;
        } else {
            if (this._multi) {
                if (item.option.selected) {
                    item.option.removeAttribute("selected");
                } else {
                    item.option.setAttribute("selected", "selected");
                    lastOption = item.option;
                }
            } else {
                if (this._selectElement.value === item.option.value) return;
                this._selectElement.value = item.option.value;
            }
        }

        if (lastOption && !this._multi) {
            this._selectElement.value = lastOption.value;
        }

        this._selectElement.dispatchEvent(new Event("change"));
    }

    clear() {
        if (this._values.size === 0) return;

        this._optionElements.forEach((op) => {
            if (op.value === "" ||
                op.value === "_") {
                op.setAttribute('selected', 'selected');
                this._selectElement.value = op.value;
            } else {
                op.removeAttribute('selected');
            }
        })

        this._selectElement.dispatchEvent(new Event("change"));
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};