import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import customSelectDecorator from "./decorators/custom-select.decorator.js";

const decorators = [customSelectDecorator];

const defaultOptions = {
	__namespace: "",
};

class ElementClass extends HTMLLabelElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		// code goes here
		decorators.forEach((d) => d.attach(this, this._options));
	}

	disconnectedCallback() {
		// code goes here
		decorators.forEach((d) => d.detach(this, this._options));
	}
}

customElements.define("custom-select", ElementClass, {
	extends: "label",
});

export default ElementClass;