import dispatcher from "../../dispatcher.js";
import sliderStore from "../../slider/slider.store.js";
import * as THREE from "/node_modules/three/build/three.module.js";

import { attach, detach, update } from "../../utils/decorator.utils.js";
import { simpleTween } from "../../utils/animation.utils.js";
import { parseUniforms} from "../webgl.utils.js";

let loadingManager = new THREE.LoadingManager(function () {});
let texloader = new THREE.TextureLoader(loadingManager);

const name = "webgl-slider";

const defaultOptions = {
    name: name,
    duration: 0.3,
    ease: null,
    textureUniforms: ["tDiffuse1", "tDiffuse2"],

    valueUniform: "uSlider",
    slides: null,
    continuous: true,
};

function getTexture(src) {
    let texture = parseUniforms({
        tDiffuse: {type: "t", src: src}
    }).tDiffuse.value;

    return texture;
}

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleSlideStore = this.handleSlideStore.bind(this);

        this._values = [];
        this._valuesShift = 0;
        this._tweens = [];
    }

    init() {
        this._values = [];

        this._currentSlide = 0;
        this._currentTexture = 0;

        this._sliderId = this._options.sliderId;

        if (!this._sliderId) {
            console.error("unique sliderId should be provided as slides");
            return;
        }

        this._options.textureUniforms.forEach((t) => {
            this._values.push(1);
        });

        this._slides = this._options.slides;

        if (!this._slides) {
            console.error(
                "array of image sources should be provided as slides"
            );
            return;
        }

        this._slides = this._slides.map((s, i) => {
            return {
                index: i,
                src: s,
                texture: s ? getTexture(s) : null,
            };
        });

        this._totalSlides = this._slides.length;

        dispatcher.dispatch({
            type: "slider:add",
            id: this._sliderId,
            total: this._totalSlides,
            index: this._currentSlide,
            continuous: this._options.continuous,
        });

        // set all textures to initial and all values to 1
        // basicaly eveny next texture should cover previous one to let us use more then 1 animation

        let newUniforms = {};

        this._options.textureUniforms.forEach((tn) => {
            newUniforms[tn] = this._slides[this._currentSlide].texture;
        });

        this._oldTextureUniforms = Object.assign({}, newUniforms);

        newUniforms[this._options.valueUniform] = this._values.slice();

        this._parent.updateUniforms(newUniforms);

        sliderStore.subscribe(this.handleSlideStore);
    }

    destroy() {
        this._oldTextureUniforms = {};

        this._slides.forEach((s) => {
            if (s.texture) {
                s.texture.dispose();
            }
        });

        dispatcher.dispatch({
            type: "slider:remove",
            id: this._sliderId,
        });
    }

    handleSlideStore() {
        let data = sliderStore.getData()[this._sliderId];

        if (!data) return;
        if (data.index === this._currentSlide) return;
        if (data.index === undefined) return;
        this._currentSlide = data.index;

        this._currentTexture++;
        if (this._currentTexture >= this._options.textureUniforms.length) {
            this._currentTexture = 0;
        }

        // shift by 1
        let newUniforms = {};

        this._options.textureUniforms.forEach((tn, i) => {
            let nextName = this._options.textureUniforms[i + 1];
            if (nextName) {
                newUniforms[tn] = this._oldTextureUniforms[nextName];
            } else {
                newUniforms[tn] = this._slides[this._currentSlide].texture;
            }
        });

        this._oldTextureUniforms = Object.assign({}, newUniforms);

        this._parent.updateUniforms(newUniforms);

        newUniforms = {};

        this._values.push(0);
        this._values.shift();

        // we are shifting tweens by 1 to the left

        this._tweens.forEach((tweenObject) => {
            tweenObject.i--;
        });

        let tweenObject = {
            i: 0,
            tween: null,
        };

        tweenObject.tween = simpleTween(
            {
                from: 0,
                to: 1,
                duration: this._options.duration,
                ease: this._options.ease,
            },
            (animationData) => {
                this._values[this._values.length - 1 + tweenObject.i] =
                    animationData.value;
                newUniforms[this._options.valueUniform] = this._values.slice();
                this._parent.updateUniforms(newUniforms);
            }
        );

        this._tweens.push(tweenObject);
        if (this._tweens.length > this._values.length) {
            this._tweens[0].tween.kill();
            this._tweens.shift();
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};