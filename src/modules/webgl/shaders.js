import simpleFragmentShader from "./shaders/simple.fragment.js";
import simpleVertexShader from "./shaders/simple.vertex.js";
import matcapFragmentChunk from './shaders/chunks/matcap.fragment.chunk.js';
import matcapVertexChunk from './shaders/chunks/matcap.vertex.chunk.js';
import FXAAFragmentChunk from './shaders/chunks/FXAA.fragment.chunk.js';
import premultiplyAlphaChunk from "./shaders/chunks/premultiplyAlpha.fragment.chunk.js";
import randomChunk from './shaders/chunks/random.fragment.chunk.js';
import sliderChunk from './shaders/chunks/slider.fragment.chunk.js';
import coverUvChunk from "./shaders/chunks/coverUv.chunk.js";
import containUvChunk from "./shaders/chunks/containUv.chunk.js";
import ditheringChunk from "./shaders/chunks/dithering.fragment.chunk.js";

export {
	simpleFragmentShader,
	simpleVertexShader,
	matcapFragmentChunk,
	matcapVertexChunk,
	FXAAFragmentChunk,
	premultiplyAlphaChunk,
	randomChunk,
	sliderChunk,
	coverUvChunk,
	containUvChunk,
	ditheringChunk
};