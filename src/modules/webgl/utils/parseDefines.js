let parseDefines = function (defines) {
	let result = '';

	if (!defines) return '';

	Object.keys(defines).forEach(function (key) {
		let type = defines[key].type;
		let value = defines[key].value;

		if (Array.isArray(value)) {
			value = value
				.map(function (v) {
					return v % 1 === 0 ? v + '.0' : v.toString();
				})
				.join(',');
		} else if (type === 'f') {
			value = value % 1 === 0 ? value + '.0' : value.toString();
		} else if (type === 'i') {
			value = parseInt(value);
		} else if (type === 'b') {
			value = !!value;
		} else {
			console.error(`unknown type "${type}"`);
			return;
		}

		result +=
			type === 'v2'
				? '#define ' + key + ' vec2(' + value + ')\n'
				: type === 'v3'
				? '#define ' + key + ' vec3(' + value + ')\n'
				: type === 'v4'
				? '#define ' + key + ' vec4(' + value + ')\n'
				: '#define ' + key + ' ' + value + '\n';
	});

	return result;
};

export default parseDefines;
