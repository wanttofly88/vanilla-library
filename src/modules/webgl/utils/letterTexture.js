import * as THREE from '/node_modules/three/build/three.module.js';

//

// letterTexture(text, styles, size)
// возвращает текстуру с текстом для THREE.js
// text - сам текст
// styles - getComputedStyle
//	.blur - размытие текста, экономит место и уменьшает качестово.
//		реально использовать можно для текстур теней
//		значение можно начать с 4
//	.verticalAlign - используется middle
// size = {w: 100, h: 100} - целевые размеры текстуры. подгонит все равно под ближайшую степень двойки

// options
//	.debug - выведет канвасы в body
//	.filter - linear - линейный, mipMap - по-умолчанию
//  .scale - увеличить разрешение текстуры

// ctx.fillText - здесь возможно немного нужно подправить координаты

var letterTexture = function (text, styles, size, options) {
	options = Object.assign(
		{
			debug: false,
			filter: 'mipMap',
			scale: 1,
		},
		options
	);

	var texture;
	var canvas, ctx;
	var w = size.w || 2048;
	var h = size.h || 512;
	var tw, th;
	var startWord = 0;
	var curWord = 0;
	var totalWords;
	var scale = [1, 1];
	var globalScale = options.scale;

	var font = {
		size: parseFloat(styles.fontSize),
		weight: parseInt(styles.fontWeight),
		family: styles.fontFamily,
		align: styles.textAlign || 'center',
		spacing:
			styles.letterSpacing === 'normal'
				? 0
				: parseFloat(styles.letterSpacing),
		color: '#F51B1B',
		lineHeight:
			styles.lineHeight === 'normal'
				? 1.2
				: parseFloat(styles.lineHeight) / parseFloat(styles.fontSize),
		textTransform: styles.textTransform || 'none',
		verticalAlign: 'middle',
	};

	if (font.spacing < 0) {
		w -= font.spacing;
	}

	// font.align = 'left';
	// font.spacing += 0.4;

	var words;
	var line = '';
	var lines = [];
	var lineSize = 0;
	var y = 0;
	var x = 0;
	var yShift = 0;

	function getClosestPowerTwo(num) {
		var res = 1;
		while (res < num) {
			res *= 2;
		}
		return res;
	}

	function measureWord(word) {
		var txt = word.split('');
		var ln = txt.length;
		var w = 0;

		txt.forEach(function (lt, i) {
			w += ctx.measureText(lt).width;
		});

		return {
			w: w + font.spacing * (ln - 1),
			h: font.size * font.lineHeight,
		};
	}

	function drawWithLetterSpacing(text, y) {
		var txt = text.split('');
		var txtStart;

		if (font.align === 'left' || font.align === 'start') {
			ctx.textAlign = 'left';
			txtStart = 0;
		} else if (font.align === 'right') {
			txtStart = w - font.spacing;
		} else {
			txtStart =
				(w - ctx.measureText(text).width - text.length * font.spacing) /
				2;
			ctx.textAlign = 'left';
		}

		txt.forEach(function (lt, i) {
			ctx.fillText(
				lt,
				txtStart,
				y + font.size * font.lineHeight * 0.7 + 3
			);
			txtStart = txtStart + font.spacing + ctx.measureText(lt).width;
		});
	}

	function replaceAll(str, find, replace) {
		var re = new RegExp(find, 'g');
		return str.replace(re, replace);
	}

	text = replaceAll(text, '&nbsp;', '\u00a0');
	text = replaceAll(text, '&ndash;', '\u2013');
	text = replaceAll(text, '&mdash;', '\u2014');
	text = replaceAll(text, '&amp;', '\u0026');
	text = replaceAll(text, '&lsquo;', '\u2018');
	text = replaceAll(text, '&rsquo;', '\u2019');
	text = replaceAll(text, '&laquo;', '\u00ab');
	text = replaceAll(text, '&raquo;', '\u00bb');

	tw = getClosestPowerTwo(w * globalScale);
	th = getClosestPowerTwo(h * globalScale);

	canvas = document.createElement('canvas');

	scale[0] = tw / w;
	scale[1] = th / h;

	if (font.blur) {
		scale[0] /= font.blur;
		scale[1] /= font.blur;
	}

	canvas.width = Math.ceil(w * scale[0]);
	canvas.height = Math.ceil(h * scale[1]);

	ctx = canvas.getContext('2d');
	ctx.scale(scale[0], scale[1]);

	if (options.debug) {
		document.body.appendChild(canvas);
	}

	if (font.blur) {
		ctx.shadowColor = font.color;
		ctx.shadowBlur = font.blur * 2;
	} else {
		ctx.shadowBlur = 0;
	}

	// ctx.shadowBlur = 100;
	// ctx.shadowColor = font.color;

	ctx.font = font.weight + ' ' + font.size + 'px ' + font.family;
	ctx.fillStyle = font.color;
	ctx.textAlign = font.align;

	if (font.textTransform === 'uppercase') {
		text = text.toUpperCase();
	}

	y = 0;
	words = text.split(' ');
	totalWords = words.length;

	while (curWord < totalWords) {
		line = words[startWord];
		lineSize = measureWord(line);
		startWord++;
		curWord = startWord;

		x = Math.max(lineSize.w, x);

		while (lineSize.w < w) {
			if (!words[curWord]) break;

			x = Math.max(lineSize.w, x);

			lineSize = measureWord(line + ' ' + words[curWord]);
			if (lineSize.w < w) {
				line += ' ' + words[curWord];
				curWord++;
			}
		}

		startWord = curWord;

		lines.push({
			text: line,
			y: y,
		});

		y += lineSize.h;
	}

	if (font.verticalAlign === 'middle' || font.verticalAlign === 'center') {
		yShift = (h - y) / 2;
	}

	lines.forEach(function (line) {
		drawWithLetterSpacing(line.text, line.y + yShift);
	});

	texture = new THREE.Texture(canvas);

	texture.repeat.set(1, 1);
	texture.offset.set(0, 0);
	texture.wrapS = THREE.RepeatWrapping;
	texture.wrapT = THREE.RepeatWrapping;

	if (options.filter === 'linear') {
		texture.magFilter = THREE.LinearFilter;
		texture.minFilter = THREE.LinearFilter;
	}

	texture.needsUpdate = true;
	texture._size = [tw, th];
	texture._textWidth = x;

	return texture;
};

export default letterTexture;
