export default class MaterialCache {
    constructor(options) {
        this._elements = [];
        this._debug = options.debug;

        this.get = this.get.bind(this);
        this.push = this.push.bind(this);
    }

    get(data) {
        if (this._debug) {
            console.log("Getting from cache " + data.name);
        }

        let result = this._elements.find((el) => {
            return (el.name === data.name);
        });

        return result ? result.material : null; 
    }
    
    push(data) {
        if (this._debug) {
            console.log("Adding to cache " + data.name);
        }

        this._elements.forEach((el) => {
            if (el.name === data.name) return;
        })

        this._elements.push({
            material: data.material,
            name: data.name
        });
    }
}