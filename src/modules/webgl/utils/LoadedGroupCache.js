export default class ObjCache {
    constructor(options) {
        this._elements = [];
        this._debug = options.debug;
        this._selfTerminate = false;
        this._selfTerminateTime = 1000;
        this._selfTerminateTimeout = null;

        this.get = this.get.bind(this);
        this.push = this.push.bind(this);
        this.load = this.load.bind(this);
        this.terminate = this.terminate.bind(this);
    }

    get(data) {
        if (this._debug) {
            console.log("Getting from cache " + data.src);
        }

        if (this._selfTerminate) {
            clearTimeout(this._selfTerminateTimeout);
            this._selfTerminateTimeout = setTimeout(this.terminate, this._selfTerminateTime);
        }
        
        return this._elements.find((el) => {
            return el.src === data.src;
        });
    }
    
    push(data) {
        if (this._debug) {
            console.log("Adding to cache " + data.src);
        }

        if (this._selfTerminate) {
            clearTimeout(this._selfTerminateTimeout);
            this._selfTerminateTimeout = setTimeout(this.terminate, this._selfTerminateTime);
        }

        this._elements.push({
            src: data.src,
            loaded: false,
            group: null,
            handlers: []
        });
    }

    load(data) {
        if (this._debug) {
            console.log("Loading cache " + data.src);
        }

        if (this._selfTerminate) {
            clearTimeout(this._selfTerminateTimeout);
            this._selfTerminateTimeout = setTimeout(this.terminate, this._selfTerminateTime);
        }

        let el = this.get({src: data.src});
        if (el.loaded) return;
        el.loaded = true;
        el.group = data.group;
        el.handlers.forEach((h) => {
            h(el.group);
        });
    }

    terminate() {
        this._elements = [];

        if (this._debug) {
            console.log("Self-terminated cache.");
        }
    }
}