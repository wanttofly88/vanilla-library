import * as THREE from "/node_modules/three/build/three.module.js";

let loadingManager = new THREE.LoadingManager(function () {});
let texloader = new THREE.TextureLoader(loadingManager);

let imageTexturesCache = {};
let VideoTexturesCache = {};

function getTexture(src) {
    let texture;

    if (imageTexturesCache[src]) {
        texture = imageTexturesCache[src];
    } else {
        texture = texloader.load(src, function () {
            // texture.magFilter = THREE.LinearFilter;
            // texture.minFilter = THREE.LinearFilter;
            texture.name = src.name || "";

            texture._size[0] = texture.image.naturalWidth;
            texture._size[1] = texture.image.naturalHeight;
            texture.needsUpdate = true;
            texture._loaded = true;
            texture._onload();
        });

        texture._size = [];
        texture._type = "image";
        texture._onload = function () {};

        imageTexturesCache[src] = texture;
    }

    return texture;
}

function getVideo(src) {
    let texture;

    if (VideoTexturesCache[src]) {
        texture = VideoTexturesCache[src];
    } else {
        let video = document.createElement("video");
        texture = new THREE.VideoTexture(video);
        texture._onload = function () {};
        texture._type = "video";
        texture._size = [];

        video.addEventListener("canplay", () => {
            texture._size[0] = video.videoWidth;
            texture._size[1] = video.videoHeight;
            texture._loaded = true;
            texture._onload();
        });

        video.src = src;
        // Probably an overkill
        video.muted = true;
        video.playsinline = true;
        video.crossorigin = "anonymous";
        video.setAttribute("webkit-playsinline", "true");
        video.setAttribute("playsinline", "true");
        video.setAttribute("loop", "");
        video.setAttribute("autoplay", "true");
        video.setAttribute("muted", "muted");
        video.play();

        texture.name = src.name || "";

        VideoTexturesCache[src] = texture;
    }

    return texture;
}

let parseUniforms = function (uniforms) {
    let result = {};

    Object.keys(uniforms).forEach(function (key) {
        let value = uniforms[key];

        result[key] =
            value.type === "t"
                ? uniforms[key].src
                    ? { type: value.type, value: getTexture(uniforms[key].src) }
                    : uniforms[key].video
                    ? { type: value.type, value: getVideo(uniforms[key].video) }
                    : { type: value.type, value: uniforms[key].value }
                : value.type === "v2" ||
                  value.type === "v3" ||
                  value.type === "v4"
                ? { type: value.type, value: uniforms[key].value.slice() }
                : { type: value.type, value: uniforms[key].value };
    });

    return result;
};

export default parseUniforms;