export default function(uniforms) {
	let result = {}
	Object.keys(uniforms).forEach((key) => {
		if (uniforms[key].value !== undefined) {
			result[key] = uniforms[key].value
		}
	});

	return result;
}