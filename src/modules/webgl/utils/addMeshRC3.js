import * as THREE from "/node_modules/three/build/three.module.js";
import LoadedGroupCache from "./LoadedGroupCache.js";
import MaterialCache from "./MaterialCache.js";
import { parseUniforms, parseDefines } from "../webgl.utils.js";
import { simpleFragmentShader, simpleVertexShader } from "../shaders.js";

import { RC3Loader } from "../../../libs/RC3Loader.js";

const DEBUG = false;

let cacheGroupBuffer = new LoadedGroupCache({ debug: DEBUG });
let cacheMaterial = new MaterialCache({ debug: DEBUG });

const defaultOptions = {
  position: { x: 0, y: 0, z: 0 },
  size: { x: 1, y: 1, z: 1 },
  rotation: { x: 0, y: 0, z: 0 },

  vertexShader: simpleVertexShader,
  fragmentShader: simpleFragmentShader,

  uniforms: {},
  defines: {},
  material: {},
};

let addMesh = function (src, owerriteOptions) {
  let options = Object.assign({}, defaultOptions, owerriteOptions);

  function getShaderMaterial(options) {
    let fragmentShader = parseDefines(options.defines);
    fragmentShader += options.fragmentShader;

    let material;

    if (options.material.name) {
      material = cacheMaterial.get({
        name: options.material.name,
      });

      if (material) {
        material = material.clone();
        material.uniforms = parseUniforms(options.uniforms);
        material.needsUpdate = true;
      }
    }

    if (!material) {
      material = new THREE.ShaderMaterial({
        uniforms: parseUniforms(options.uniforms),
        vertexShader: options.vertexShader,
        fragmentShader: fragmentShader,
      });

      cacheMaterial.push({
        material: material,
        name: options.material.name,
      });
    }

    return Object.assign(material, options.material);
  }

  return new Promise((resolve) => {
    function handleLoad(rc3) {
      let group = new THREE.Group();

      if (DEBUG) {
        console.log("Loaded " + src);
      }

      rc3.traverse(function (child) {
        if (child instanceof THREE.Mesh) {
          let clone = child.clone();
          clone.material = getShaderMaterial(options);
          group.add(clone);
        }
      });

      group.scale.set(options.size.x, options.size.y, options.size.z);

      group.position.set(
        options.position.x,
        options.position.y,
        options.position.z
      );

      group.rotation.set(
        options.rotation.x,
        options.rotation.y,
        options.rotation.z
      );

      resolve({
        group: group,
      });
    }

    if (!src) {
      console.warn("Src property is missing");
      resolve(null);
      return;
    }

    let bufferCached = cacheGroupBuffer.get({ src: src });

    if (bufferCached) {
      if (DEBUG) {
        console.log("Buffer cached. Adding handler. " + src);
      }

      if (bufferCached.loaded) {
        handleLoad(bufferCached.group);
      }

      bufferCached.handlers.push(handleLoad);
    } else {
      if (DEBUG) {
        console.log("Buffer not cached. Creating cache. " + src);
      }

      cacheGroupBuffer.push({ src: src });
      bufferCached = cacheGroupBuffer.get({ src: src });
      bufferCached.handlers.push(handleLoad);

      let loader = new RC3Loader();
      loader.load(src, (group) => {
        cacheGroupBuffer.load({
          src: src,
          group: group,
        });
      }, "standart");
    }
  });
};

export default addMesh;
