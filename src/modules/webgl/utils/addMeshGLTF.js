import * as THREE from "/node_modules/three/build/three.module.js";

import { GLTFLoader } from "/node_modules/three/examples/jsm/loaders/GLTFLoader.js";
import { parseUniforms, parseDefines } from "../webgl.utils.js";
import { simpleFragmentShader, simpleVertexShader } from "../shaders.js";

let loadingManager = new THREE.LoadingManager(function () {});
let texloader = new THREE.TextureLoader(loadingManager);

let meshLoader = new GLTFLoader(loadingManager);

// TODO: MOVE DPI TO PARENT
// DOESN'T WORK FOR UNKNOWN REASON!!!

const defaultOptions = {
	position: { x: 0, y: 0, z: 0 },
	size: { x: 1, y: 1, z: 1 },
	vertexShader: simpleVertexShader,
	fragmentShader: simpleFragmentShader,
	uniforms: {},
	material: {},
};

function isTouchDevice() {
	let prefixes = " -webkit- -moz- -o- -ms- ".split(" ");
	let mq = function (query) {
		return window.matchMedia(query).matches;
	};

	if (
		"ontouchstart" in window ||
		(window.DocumentTouch && document instanceof DocumentTouch)
	) {
		return true;
	}

	let query = ["(", prefixes.join("touch-enabled),("), "heartz", ")"].join(
		""
	);
	return mq(query);
}

let addMesh = function (src, owerriteOptions) {
	let options = Object.assign({}, defaultOptions, owerriteOptions);

	let self = this;
	let promises = [];
	let geometry, material, mesh;

	return new Promise(function (resolve, reject) {
		let fragmentShader = options.fragmentShader;
		let optionalUniforms = parseUniforms(
			Object.assign({}, options.uniforms)
		);
		let dpi = window.devicePixelRatio;
		let size;

		if (!src) {
			console.warn("url property is missing");
			resolve(null);
			return;
		}

		fragmentShader =
			"#define DPI " +
			(dpi % 1 === 0 ? dpi + ".0" : dpi) +
			" \n" +
			options.fragmentShader;

		if (isTouchDevice()) {
			fragmentShader = "#define MOBILE 1.0\n" + fragmentShader;
		}

		material = new THREE.ShaderMaterial({
			uniforms: Object.assign(
				{
					screenResolution: { type: "v2", value: [1, 1] },
					textureSize: { type: "v2", value: size },
				},
				optionalUniforms
			),
			vertexShader: options.vertexShader,
			fragmentShader: fragmentShader,
			transparent: true,
		});

		material = Object.assign(material, options.material);

		meshLoader.load(src, function (data) {
			let scene = data.scene;
			let animations = data.animations;
			let group = new THREE.Group();

			let toAdd = [];

			scene.traverse(function (child) {
				if (child.type === "Mesh") {
					child.material = material;
					toAdd.push(child);
				}
			});

			toAdd.forEach(function (m) {
				group.add(m);
			});

			group.scale.set(options.size.x, options.size.y, options.size.z);
			group.position.set(
				options.position.x,
				options.position.y,
				options.position.z
			);
			group.rotation.set(
				options.rotation.x,
				options.rotation.y,
				options.rotation.z
			);

			// if (options.size.length < 3) {
			// 	console.error('size option array needs 3 elements');
			// }

			resolve({
				group: group,
				animations: animations,
			});
		});
	});
};

export default addMesh;