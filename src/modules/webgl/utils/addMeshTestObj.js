import * as THREE from '/node_modules/three/build/three.module.js';
import { parseUniforms, parseDefines } from "../webgl.utils.js";
import { simpleFragmentShader, simpleVertexShader } from "../shaders.js";

// import OBJLoader from '../../../libs/OBJLoaderWithWorker.js';
import { OBJLoader2Parallel } from "../../../libs/OBJLoader2Parallel.js";

// let meshLoader = new OBJLoader();

const fileLoader = new THREE.FileLoader();

const defaultOptions = {
	position: { x: 0, y: 0, z: 0 },
	size: { x: 1, y: 1, z: 1 },
	rotation: { x: 0, y: 0, z: 0 },

	vertexShader: simpleVertexShader,
	fragmentShader: simpleFragmentShader,

	uniforms: {},
	defines: {},
	material: {},
};

let pending = [];
let working = false;

let parseMesh = function(data) {
	let options = data.options;
	let callback = data.callback;
	let buffer = data.buffer;

	let objLoader2Parallel = new OBJLoader2Parallel();
	objLoader2Parallel.setCallbackOnLoad((group) => {
		console.dir(group);

		let fragmentShader = parseDefines(options.defines);
		fragmentShader += options.fragmentShader;

		let material = new THREE.ShaderMaterial({
			uniforms: parseUniforms(options.uniforms),
			vertexShader: options.vertexShader,
			fragmentShader: fragmentShader,
		});

		material = Object.assign(material, options.material);

		group.traverse(function (child) {
			if (child instanceof THREE.Mesh) {
				child.material = material;
			}
		});

		group.scale.set(options.size.x, options.size.y, options.size.z);
		group.position.set(
			options.position.x,
			options.position.y,
			options.position.z
		);
		group.rotation.set(
			options.rotation.x,
			options.rotation.y,
			options.rotation.z
		);

		callback({
			group: group,
		});

		working = false;
		checkIfPending();
	});

	objLoader2Parallel.parse(buffer);
}

let checkIfPending = function() {
	if (working) return;
	if (pending.length === 0) return;
	
	let data = pending.shift();
	working = true;
	parseMesh(data);
}

let addMesh = function (src, owerriteOptions) {
	let options = Object.assign({}, defaultOptions, owerriteOptions);

	return new Promise((resolve) => {
		if (!src) {
			console.warn('src property is missing');
			resolve(null);
			return;
		}

		fileLoader.setPath('');
		fileLoader.setResponseType('arraybuffer');
		fileLoader.load(src, (content) => {
			pending.push({
				buffer: content,
				callback: resolve,
				options: options
			});

			checkIfPending();
		});
	});
};

export default addMesh;
