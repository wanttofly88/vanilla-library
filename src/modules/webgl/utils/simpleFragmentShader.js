export default `
varying vec2 vUv;
uniform sampler2D tDiffuse;

#ifdef COVER
	uniform vec2 textureSize;
	uniform vec2 resolution;
#endif
#ifdef CONTAIN
	uniform vec2 textureSize;
	uniform vec2 resolution;
#endif

vec2 getCoverCoordinates(vec2 p, vec2 resolution, vec2 textureSize) {
	float rs = resolution.x / resolution.y;
	float ri = textureSize.x / textureSize.y;

	vec2 new = rs < ri ? 
		vec2(textureSize.x * resolution.y / textureSize.y, resolution.y):
		vec2(resolution.x, textureSize.y * resolution.x / textureSize.x);

	vec2 offset = (rs < ri ? 
		vec2((new.x - resolution.x) / 2.0, 0.0):
	 	vec2(0.0, (new.y - resolution.y) / 2.0)) / new;

	return p * resolution / new + offset;
}

vec2 getContainCoordinates(vec2 p, vec2 resolution, vec2 textureSize) {
	float rs = resolution.x / resolution.y;
	float ri = textureSize.x / textureSize.y;

	vec2 new = rs > ri ? 
		vec2(textureSize.x * resolution.y / textureSize.y, resolution.y):
		vec2(resolution.x, textureSize.y * resolution.x / textureSize.x);

	vec2 offset = (rs > ri ? 
		vec2((new.x - resolution.x) / 2.0, 0.0):
	 	vec2(0.0, (new.y - resolution.y) / 2.0)) / new;

	return p * resolution / new + offset;
}

void main() {
	vec2 uv = vUv.xy;

	#ifdef COVER
		uv = getCoverCoordinates(vUv.xy, resolution, textureSize);
	#endif

	#ifdef CONTAIN
		uv = getContainCoordinates(vUv.xy, resolution, textureSize);
	#endif

	vec4 color = texture2D(tDiffuse, uv.xy);
	// vec4 color = vec4(1.0, 0.0, 0.0, 1.0);
    gl_FragColor = color;
}
`;
