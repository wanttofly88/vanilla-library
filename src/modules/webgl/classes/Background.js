import * as THREE from '/node_modules/three/build/three.module.js';

import dispatcher from '../../../dispatcher.js';
import resizeStore from '../../../resize/resize.store.js';

import { parseDefines, parseUniforms } from '../webgl.utils.js';
import { simpleVertexShader, simpleFragmentShader } from '../shaders';

const namePrefix = 'background';
const name = 'undefined';

const defaultOptions = {
    shortName: name,
    name: namePrefix + '-' + name,
    fragmentShader: simpleFragmentShader(),
    vertexShader: simpleVertexShader(),
    position: {x: 0, y: 0, z: -1000}
}

const decorators = [];

class Background {
    constructor(parent, options, decorators) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);
        this._embededDecorators = decorators || [];
        this._debug = this._parent._debug;

        this._shortName = this._options.shortName;
        this._namePrefix = namePrefix;
        this._options.name = namePrefix + '-' + this._shortName;
        this._name = this._options.name;

        this.handleResize = this.handleResize.bind(this);
    }

    oninit() {}
    ondestroy() {}

    init() {
        this._embededDecorators.forEach(function (d) {
            d.attach(this, this._options);
        });

        this._uniforms = parseUniforms(this._options.uniforms);

        let fragmentShader = parseDefines(this._options.defines);
        fragmentShader += this._options.fragmentShader;

        let material = new THREE.ShaderMaterial({
            uniforms: this._uniforms,
            fragmentShader: fragmentShader,
            vertexShader: simpleVertexShader,
        });

        let geometry = new THREE.PlaneBufferGeometry(1, 1, 1, 1);
        let mesh = new THREE.Mesh(geometry, material);

        let drawingBufferSize = this._parent._renderer.getDrawingBufferSize(
            new THREE.Vector2()
        );

        this.updateUniforms({
            resolution: [drawingBufferSize.x, drawingBufferSize.y],
        });

        this._mesh = mesh;
        this._parent._scene.add(this._mesh);
        this._mesh.position.set(this._options.position.x, this._options.position.y, this._options.position.z);

        this.handleResize();
        this._parent._resizeArray.push(this.handleResize);

        this.oninit();

        return Promise.resolve();
    }

    destroy() {
        this._embededDecorators.forEach(function (d) {
            d.detach(this, this._options);
        });

        this._parent._scene.remove(this._mesh);
        this._parent._resizeArray.remove(this.handleResize);

        this._mesh.material.dispose();
        this._mesh.geometry.dispose();

        this.ondestroy();

        return Promise.resolve();
    }

    handleResize() {
        let size = this._parent._renderer.getSize(new THREE.Vector2());
        this._mesh.scale.set(size.x * 2, size.y * 2, 1);
    }

    updateUniforms(uniforms) {
        for (var key in uniforms) {
            if (this._uniforms.hasOwnProperty(key)) {
                this._uniforms[key].value = uniforms[key];
            }
        }
    }
}

export default Background;
