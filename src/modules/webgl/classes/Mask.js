import dispatcher from "../../dispatcher.js";
import resizeStore from "../../resize/resize.store.js";
import scrollStore from "../../scroll/scroll.store.js";
import { positionDecorator } from "../../utils/dom.utils.js";
import { size } from "../../../config/size.config.js";

const namePrefix = "mask";
const name = "undefined";

const defaultOptions = {
    shortName: name,
    namePrefix: namePrefix,
    name: namePrefix + "-" + name,
    absolute: false,
};

const decorators = [];

const iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

function mobileAndTabletCheck() {
    let check = false;
    (function (a) {
        if (
            /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(
                a
            ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                a.substr(0, 4)
            )
        )
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

const MOBILE_ADD = mobileAndTabletCheck()
    ? iOS
        ? size.shiftIos
        : size.shiftAndroid
    : 0;

class Mask {
    constructor(constructorOptions) {
        if (!constructorOptions.parent) {
            console.error(`parent property is missing`);
        }
        if (!constructorOptions.options) {
            console.error(`options property is missing`);
        }
        if (!constructorOptions.decorators) {
            console.error(`decorators property is missing`);
        }

        this._parent = constructorOptions.parent;
        this._options = Object.assign({}, constructorOptions.options);
        this._childDecorators = constructorOptions.decorators || [];

        this._debug = this._parent._debug;

        this._shortName = this._options.shortName;
        this._namePrefix = this._options.namePrefix;
        this._options.name = this._options.namePrefix + "-" + this._shortName;
        this._name = this._options.name;

        this._scene = this._parent._scene;
        this._touch = this._parent._touch;
        this._renderer = this._parent._renderer;

        this._loaded = false;
        this._onScreen = false;
        this._elements = [];

        this.handleScroll = this.handleScroll.bind(this);
        this.handleDispatcher = this.handleDispatcher.bind(this);
        this.checkElement = this.checkElement.bind(this);

        this._className = this._options.className;
        this._uniformName = this._options.uniformName;

        if (!this._className) {
            console.error('"className" is undefined');
            return;
        }

        if (!this._uniformName) {
            console.error('"uniformName" is undefined');
            return;
        }

        this._metrics = [];
    }

    oninit() {}
    ondestroy() {}
    onChange() {}

    async init() {
        this._childDecorators.forEach(function (d) {
            d.attach(this, this._options);
        });

        this.handleScroll();
        scrollStore.subscribe(this.handleScroll);
        resizeStore.subscribe(this.handleScroll);

        dispatcher.subscribe(this.handleDispatcher);

        this.oninit();
    }

    async destroy() {
        this._elements = [];

        this._childDecorators.forEach(function (d) {
            d.detach(this, this._options);
        });

        dispatcher.unsubscribe(this.handleDispatcher);
        scrollStore.unsubscribe(this.handleScroll);
        resizeStore.unsubscribe(this.handleScroll);

        this.ondestroy();
    }

    handleContent() {
        this._elements = document.getElementsByClassName(this._className);
        this._elements = Array.prototype.slice.call(this._elements);

        if (this._elements && this._elements.length) {
            this._elements.forEach((el) => {
                positionDecorator.attach(el);
            });
        }

        this.handleScroll();
        this._parent.checkIfLoad();
    }

    handleScroll() {
        let wh = resizeStore.getData().height;
        let scrolled = scrollStore.getData().top;
        // if (scrollStore.getData().locked) return;
        let foundMask = null;

        this._elements.forEach((e) => {
            if (!e._position) return;

            let position = e._position.get();

            let top = (position.top - scrolled) / wh;
            top = 1 - Math.max(0, Math.min(1, top));

            let bottom = 1 - (position.top + position.height - scrolled) / (wh + MOBILE_ADD);
            bottom = Math.max(0, Math.min(1, bottom));

            let mask = [top, bottom];

            if (Math.abs(mask[0] - mask[1]) > 0) {
                foundMask = mask;
            }
        });

        if (foundMask) {
            this._mask = foundMask;
            this._onScreen = true;
        } else {
            this._mask = [0, 0];
            this._onScreen = false;
        }

        let uniforms = {};

        if (this._options.absolute) {
            if (this._mask[0] > 0) this._mask[0] = 1;
            this._mask[1] = 0;
        }

        uniforms[this._uniformName] = this._mask;

        this._parent.updateUniforms(uniforms);

        if (this._parent.checkIfActive) {
            this._parent.checkIfActive();
        }

        this.onChange();
    }

    handleDispatcher(e) {
        if (e.type === "content:replaced") {
            this.handleContent();
        }

        if (e.type === "mask:update-values") {
            this.handleScroll();
        }
    }

    checkElement() {
        return !!this._elements;
    }
}

export default Mask;