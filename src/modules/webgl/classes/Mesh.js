import * as THREE from "/node_modules/three/build/three.module.js";

import { ValueArray } from "../../utils/array.utils.js";
import { addMeshOBJ, addMeshRC3 } from "../webgl.utils.js";
import { uniformsToObject } from "../webgl.utils.js";
import { simpleVertexShader } from "../shaders.js";
import { simpleFragmentShader } from "../shaders.js";

import BaseClass from "./Base.js";

// possible hooks:
// onload
// onunload
// onloop
// onshow
// onhide
// onresize

const namePrefix = "mesh";
const name = "undefined";

const defaultOptions = {
    shortName: name,
    namePrefix: namePrefix,
    name: namePrefix + "-" + name,

    src: null,
    parent: null,

    autoClean: false,

    uniforms: {},
    vertexShader: simpleVertexShader(),
    fragmentShader: simpleFragmentShader(),

    position: { x: 0, y: 0, z: 0 },
    scale: { x: 1, y: 1, z: 1 },
    rotation: { x: 0, y: 0, z: 0 },
};

export default class Mesh extends BaseClass {
    constructor(constructorOptions) {
        if (!constructorOptions.parent) {
            console.error(`parent property is missing`);
        }
        if (!constructorOptions.options) {
            console.error(`options property is missing`);
        }
        if (!constructorOptions.decorators) {
            console.error(`decorators property is missing`);
        }

        super({
            parent: constructorOptions.parent,
            options: Object.assign(
                {},
                defaultOptions,
                constructorOptions.options
            ),
            decorators: constructorOptions.decorators,
        });

        this._opacityArray = new ValueArray();
        this._opacityArray.push({
            name: "original",
            value: 1,
        });

        this._meshes = [];
    }

    async init() {
        this._group = new THREE.Group();
        this._group.name = this._options.name;

        if (!this._parentObject) {
            console.warn("parent is missing both _group and _scene properties");
        }

        await Promise.all(
            Object.values(this._childDecorators).map((d) => d.attach(this, {}))
        );

        this.oninit();
    }

    async load() {
        let src = this._options.src;
        if (!src) {
            console.error("src option is missing");
            return;
        }

        let addMeshPromise;
        let format = src.split(".").pop();

        if (format === "obj") {
            addMeshPromise = addMeshOBJ(src, this._options);
        } else if (format === "rc3") {
            addMeshPromise = addMeshRC3(src, this._options);
        } else {
            console.error(`format "${format}" is not supported`);
        }

        const meshData = await addMeshPromise;

        this._activePromises = this._activePromises.filter(
            (p) => p !== addMeshPromise
        );

        if (addMeshPromise._canceled) {
            return Promise.reject("Promise canceled");
        }

        let toAdd = [];

        if (meshData.group.type === "Group") {
            meshData.group.traverse(function (m) {
                if (m.type === "Mesh") {
                    toAdd.push(m);
                }
            });
        } else if (meshData.group.type === "Mesh") {
            toAdd.push(meshData.group);
        }

        if (meshData.animations) {
            this._animations = meshData.animations;
        }

        toAdd.forEach((m) => {
            this._group.add(m);
            this._meshes.push(m);
        });

        if (!this._options.autoClean) {
            if (this._debug) {
                console.log("added ", this._options.name);
            }

            this._parentObject.add(this._group);
        }

        this.updatePosition();

        this.updateUniforms(uniformsToObject(this._options.uniforms));

        this.handleResize();

        this._parent._loopArray.push(this.loop);
        this._parent._resizeArray.push(this.handleResize);

        this.onload();
    }

    async unload() {
        this.cancelLoad();

        if (!this._options.autoClean) {
            if (this._debug) {
                console.log("removed ", this._options.name);
            }

            this._parentObject.remove(this._group);
        }

        this._meshes.forEach((m) => {
            Object.values(m.material.uniforms).forEach((u) => {
                if (u.type === "t") {
                    u.value.dispose();
                }
            });
            m.parent.remove(m);
            m.material.dispose();
            m.geometry.dispose();
        });

        this._meshes = [];

        this._parent._loopArray.remove(this.loop);
        this._parent._resizeArray.remove(this.handleResize);

        this.onunload();
    }

    activate() {
        if (this._options.autoClean) {
            if (this._debug) {
                console.log("added ", this._options.name);
            }

            this._parentObject.add(this._group);
        }
    }

    deactivate() {
        if (this._options.autoClean) {
            if (this._debug) {
                console.log("removed ", this._options.name);
            }

            this._parentObject.remove(this._group);
        }
    }
}
