import * as THREE from '/node_modules/three/build/three.module.js';
import { simpleVertexShader, simpleFragmentShader } from '../shaders.js';

// autoupdate
// auto - default. renders every time 'render' is called
// manual - renders only if 'needsUpdate' flag is set
// semi-auto - renders only if 'needsUpdate' flag is set or any uniform was changed

var Composer = function (renderer) {
	this.passes = [];
	this.renderer = renderer;
	this.autoupdate = 'auto';
	this.needsUpdate = false;
	this.previousRenderedUniforms = {};

	this.createBufferTexture = function (options) {
		var drawingBufferSize = this.renderer.getDrawingBufferSize(
			new THREE.Vector2()
		);
		var bufferTexture;

		options = options || {};

		if (this.renderer.capabilities.isWebGL2 && options.msaa) {
			options = Object.assign({
					stencilBuffer: false,
					depthBuffer: true,
					format: THREE.RGBAFormat,
				},
				options
			);

			bufferTexture = new THREE.WebGLMultisampleRenderTarget (
				drawingBufferSize.width,
				drawingBufferSize.height,
				options
			);
		} else {
			options = Object.assign(
				{
					minFilter: THREE.LinearFilter,
					magFilter: THREE.LinearFilter,
					stencilBuffer: false,
					depthBuffer: true,
					format: THREE.RGBAFormat,
				},
				options
			);

			bufferTexture = new THREE.WebGLRenderTarget(
				drawingBufferSize.width,
				drawingBufferSize.height,
				options
			);
		}

		bufferTexture.texture.generateMipmaps = false;

		return bufferTexture;
	};

	this.renderPass = function (scene, camera, options) {
		options = Object.assign({
			msaa: false
		}, options);

		var bufferTexture = this.createBufferTexture(options);

		this.passes.push({
			scene: scene,
			camera: camera,
			target: bufferTexture,
		});

		return {
			texture: bufferTexture,
			scene: scene,
			camera: camera,
		};
	};

	this.setAutoupdate = function (val) {
		if (['auto', 'manual', 'semi-auto'].indexOf(val) === -1) {
			console.warn('wrong autoupdate value');
			return;
		}
		this.autoupdate = val;
	};

	this.shaderPass = function (shader, options) {
		// can accept texture if first pass;
		let size = this.renderer.getSize(new THREE.Vector2());
		let drawingBufferSize = this.renderer.getDrawingBufferSize(
			new THREE.Vector2()
		);
		let dpr = this.renderer.getPixelRatio();

		let material = new THREE.ShaderMaterial(shader);
		// material.premultipliedAlpha = false;
		let geometry = new THREE.PlaneBufferGeometry(1, 1, 1, 1);
		let mesh = new THREE.Mesh(geometry, material);

		let scene, camera;
		let cameraDistance = 1000;
		let fov;

		options = options || {};

		let bufferTexture = this.createBufferTexture({
			msaa: options.msaa
		});

		material.transparent = true;
		material.alpha = true;

		if (options.camera === 'perspective') {
			fov =
				2 *
				(180 / Math.PI) *
				Math.atan(size.height / (2 * cameraDistance));
			camera = new THREE.PerspectiveCamera(
				fov,
				size.width / size.height,
				100,
				1500
			);
			camera.userData.distance = cameraDistance;
			camera.position.set(0, 0, cameraDistance);
			camera.up = new THREE.Vector3(0, 1, 0);
			camera.lookAt(0, 0, 0);
		} else {
			camera = new THREE.OrthographicCamera(
				size.width / -2,
				size.width / 2,
				size.height / 2,
				size.height / -2,
				-100,
				100
			);
		}

		mesh.scale.set(size.width, size.height, 1);

		if (options.scene) {
			scene = scene;
		} else {
			scene = new THREE.Scene();
			scene._name = 'composer-scene';
		}

		scene.add(mesh);
		scene._inactive = false;

		if (!options.texture) {
			material.uniforms.tDiffuse = {
				type: 't',
				value: this.passes[this.passes.length - 1].target.texture,
			};
		} else {
			material.uniforms.tDiffuse = {
				type: 't',
				value: options.texture,
			};
		}

		this.passes.push({
			scene: scene,
			camera: camera,
			mesh: mesh,
			target: bufferTexture,
		});

		return {
			mesh: mesh,
			scene: scene,
			camera: camera,
			texture: bufferTexture,
		};
	};

	this.copyPass = function (options) {
		// just empty pass. for ping-ponging for example

		return this.shaderPass(
			{
				uniforms: {},
				vertexShader: simpleVertexShader(),
				fragmentShader: simpleFragmentShader(),
			},
			options
		);
	};

	this.render = function (target) {
		var renderFlag = false;

		if (this.autoupdate === 'auto') {
			renderFlag = true;
		} else if (this.autoupdate === 'manual' && this.needsUpdate) {
			renderFlag = true;
		} else if (this.autoupdate === 'semi-auto') {
			if (this.needsUpdate || this.checkUniformUpdates()) {
				renderFlag = true;
			}
		}

		this.passes.forEach(function (pass) {
			if (pass.scene._inactive) {
				renderFlag = false;
			}
		});

		if (!renderFlag) return;

		this.needsUpdate = false;

		this.passes.forEach((pass, index) => {
			if (index === this.passes.length - 1) {
				if (target) {
					this.renderer.setRenderTarget(target);
					this.renderer.render(pass.scene, pass.camera);
				} else {
					if (pass.target) {
						pass.target.dispose();
						pass.target = null;
					}

					if (target === null) return; // многие сцены используют мэш из шейдерпаса и ставят тогда таргет в null. Нет смысла рендерить его еще раз.
					this.renderer.setRenderTarget(null);
					this.renderer.render(pass.scene, pass.camera);
				}
			} else {
				this.renderer.setRenderTarget(pass.target);
				this.renderer.render(pass.scene, pass.camera);
			}
			// debug
			// if (pass.scene._name) {
			// 	console.log(pass.scene._name);
			// }
			this.renderer.setRenderTarget(null);
		});
	};

	this.updateUniforms = function (uniforms) {
		this.passes.forEach(function (pass, index) {
			if (!pass.mesh) return;

			for (var key in uniforms) {
				if (pass.mesh.material.uniforms.hasOwnProperty(key)) {
					pass.mesh.material.uniforms[key].value = uniforms[key];
				}
			}
		});
	};

	this.checkUniformUpdates = function () {
		var flag = false;

		this.passes.forEach((pass) => {
			if (!pass.mesh) return;
			for (var key in pass.mesh.material.uniforms) {
				if (pass.mesh.material.uniforms.hasOwnProperty(key)) {
					if (
						!this.previousRenderedUniforms.hasOwnProperty(key) ||
						this.previousRenderedUniforms[key] !==
							pass.mesh.material.uniforms[key].value
					) {
						this.previousRenderedUniforms[key] =
							pass.mesh.material.uniforms[key].value;
						flag = true;
					}
				}
			}
		});

		return flag;
	};

	this.resize = function () {
		var drawingBufferSize = this.renderer.getDrawingBufferSize(
			new THREE.Vector2()
		);
		var size = this.renderer.getSize(new THREE.Vector2());

		// if (size.x === 0 || size.y === 0) {
		// 	console.error(
		// 		'renderer "' +
		// 			this.renderer._name +
		// 			'" size is 0. check CSS canvas size'
		// 	);
		// }

		this.passes.forEach((pass) => {
			if (pass.camera.type === 'OrthographicCamera') {
				pass.camera.left = size.width / -2;
				pass.camera.right = size.width / 2;
				pass.camera.top = size.height / 2;
				pass.camera.bottom = size.height / -2;
				pass.camera.updateProjectionMatrix();
			} else if (pass.camera.type === 'PerspectiveCamera') {
				pass.camera.fov =
					2 *
					(180 / Math.PI) *
					Math.atan(
						size.height / (2 * pass.camera.userData.distance)
					);
				pass.camera.aspect = size.width / size.height;
				pass.camera.updateProjectionMatrix();
			}

			if (pass.target) {
				pass.target.setSize(
					drawingBufferSize.width,
					drawingBufferSize.height
				);
			}

			if (pass.mesh) {
				if (pass.mesh.material.uniforms.resolution) {
					pass.mesh.material.uniforms.resolution.value = [
						drawingBufferSize.width,
						drawingBufferSize.height,
					];
				}
				if (pass.mesh.material.uniforms.screenResolution) {
					pass.mesh.material.uniforms.screenResolution.value = [
						size.width,
						size.height,
					];
				}
				pass.mesh.scale.set(size.width, size.height, 1);
			}
		});
	};

	this.dispose = function () {
		this.passes.forEach((pass) => {
			if (pass.mesh) {
				Object.values(pass.mesh.material.uniforms).forEach(function (
					u
				) {
					if (u.type === 't') {
						u.value.dispose();
					}
				});
				pass.mesh.material.dispose();
				pass.mesh.geometry.dispose();
			}
			if (pass.target) {
				pass.target.dispose();
			}
			if (pass.scene) {
				pass.scene.dispose();
			}
		});

		this.passes = [];
	};
};

export default Composer;
