import { offset } from "../../utils/dom.utils.js";
import scrollStore from "../../scroll/scroll.store.js";
import resizeStore from "../../resize/resize.store.js";
import Base from "./Base.js";

const name = "parallax"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
    containerClass: "",
    type: "cover",
    marginTop: 0,
    marginBottom: 0,
    abs: 0
};

class Parallax extends Base {
	constructor(constructorOptions) {
		if (!constructorOptions.parent) {
			console.error(`parent property is missing`);
		}
		if (!constructorOptions.options) {
			console.error(`options property is missing`);
		}
		if (!constructorOptions.decorators) {
			console.error(`decorators property is missing`);
        }
        
        super({
			parent: constructorOptions.parent,
			options: Object.assign(
				{},
				defaultOptions,
				constructorOptions.options
			),
			decorators: constructorOptions.decorators,
		});

        this._value = 0;

        this.handleScroll = this.handleScroll.bind(this);
        this.handleResize = this.handleResize.bind(this);
    }

    onInit() {}

    init() {
        this._container = document.getElementsByClassName(
            this._options.containerClass
        )[0];

        this.onInit();
    }

    activate() {
        this.handleResize();
        resizeStore.subscribe(this.handleResize);
        scrollStore.subscribe(this.handleScroll);
    }

    deactivate() {
        resizeStore.unsubscribe(this.handleResize);
        scrollStore.unsubscribe(this.handleScroll);
    }

    update() {
        this._container = document.getElementsByClassName(
            this._options.containerClass
        )[0];

        this.handleResize();
        this.handleScroll();
    }

    onScroll() {}

    onRawScroll() {}

    handleScroll() {
        if (!this._container) return;

        let scrolled = scrollStore.getData().top;
        let wh = resizeStore.getData().height;
        let previousValue = this._value;

        let startPoint;
        let endPoint;

        if (this._options.type === "contain") {
            startPoint = this._offset;
            endPoint = this._offset + this._height - wh;
        } else if (this._options.type === "cover") {
            startPoint = this._offset - wh;
            endPoint = this._offset + this._height;
        } else if (this._options.type === "cover-top") {
            startPoint = this._offset - wh;
            endPoint = this._offset + this._height - wh;
        } else if (this._options.type === "cover-bottom") {
            startPoint = this._offset;
            endPoint = this._offset + this._height;
        } else if (this._options.type === "screen-top") {
            startPoint = this._offset - wh;
            endPoint = this._offset;
        } else if (this._options.type === "screen-bottom") {
            startPoint = this._offset + this._height - wh;
            endPoint = this._offset + this._height;
        } else if (this._options.type === "abs-top") {
            startPoint = this._offset - wh;
            endPoint = this._offset - wh + this._options.abs;
        } else if (this._options.type === "abs-bottom") {
            startPoint = this._offset + this._height - this._options.abs;
            endPoint = this._offset + this._height;
        }

        let rawValue = (scrolled - startPoint) / (endPoint - startPoint);

        if (endPoint <= startPoint) {
            if (scrolled <= startPoint) {
                this._value = 0;
            } else {
                this._value = 1;
            }
        } else {
            this._value = Math.max(0, Math.min(1, rawValue));
        }

        this.onRawScroll({
            value: this._value,
            raw: rawValue
        });

        if (previousValue !== this._value) {
            this.onScroll({
                value: this._value,
                raw: rawValue
            });
        }
    }

    handleResize() {
        if (!this._container) return;

        this._offset = offset(this._container).top - this._options.marginTop;
        this._height =
            this._container.clientHeight +
            this._options.marginTop +
            this._options.marginBottom;

        this.handleScroll();
    }
}

export default Parallax;
