import * as THREE from "/node_modules/three/build/three.module.js";
import BaseClass from "./Base.js";

const namePrefix = "group";
const name = "undefined";

const defaultOptions = {
	shortName: name,
	namePrefix: namePrefix,
	name: namePrefix + "-" + name,
	position: { x: 0, y: 0, z: 0 },
	scale: { x: 1, y: 1, z: 1 },
	rotation: { x: 0, y: 0, z: 0 },
};

export default class Group extends BaseClass {
	constructor(constructorOptions) {
		if (!constructorOptions.parent) {
			console.error(`parent property is missing`);
		}
		if (!constructorOptions.options) {
			console.error(`options property is missing`);
		}
		if (!constructorOptions.decorators) {
			console.error(`decorators property is missing`);
		}
		super({
			parent: constructorOptions.parent,
			options: Object.assign(
				{},
				defaultOptions,
				constructorOptions.options
			),
			decorators: constructorOptions.decorators,
		});
	}

	async init() {
		this._group = new THREE.Group();
		this._group._inactive = true;
		this._group.name = this._options.name;

		if (!this._parentObject) {
			console.warn("parent is missing both _group and _scene properties");
		}

		await Promise.all(
			Object.values(this._childDecorators).map((d) => d.attach(this, {}))
		);

		this.oninit();
	}

	async load() {
		this._parentObject.add(this._group);
		this._parent._loopArray.push(this.loop);
		this._parent._resizeArray.push(this.handleResize);

		this.checkIfActive();

		this.onload();
	}

	async unload() {
		this._parentObject.remove(this._group);
		this._parent._loopArray.remove(this.loop);
		this._parent._resizeArray.remove(this.resize);

		this.checkIfActive();
		this.onunload();
	}

	activate() {
		this._group._inactive = false;
	}

	deactivate() {
		this._group._inactive = true;
	}
}