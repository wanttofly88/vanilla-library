import * as THREE from "/node_modules/three/build/three.module.js";

import { ValueArray } from "../../utils/array.utils.js";
import { uniformsToObject, parseUniforms, parseDefines } from "../webgl.utils.js";
import { simpleFragmentShader, simpleVertexShader } from "../shaders.js";

import MaterialCache from "../utils/MaterialCache.js";
import BaseClass from "./Base.js";

// possible hooks:
// onload
// onunload
// onloop
// onshow
// onhide
// onresize

const namePrefix = "meshPlane";
const name = "undefined";

const defaultOptions = {
	shortName: name,
	namePrefix: namePrefix,
	name: namePrefix + "-" + name,

	parent: null,
	
	autoClean: false,

	uniforms: {},
	defines: {},
	material: {},

	vertexShader: simpleVertexShader,
	fragmentShader: simpleFragmentShader,

	position: { x: 0, y: 0, z: 0 },
	scale: { x: 1, y: 1, z: 1 },
	rotation: { x: 0, y: 0, z: 0 },
};

export default class MeshPlane extends BaseClass {
	constructor(constructorOptions) {
		if (!constructorOptions.parent) {
			console.error(`parent property is missing`);
		}
		if (!constructorOptions.options) {
			console.error(`options property is missing`);
		}
		if (!constructorOptions.decorators) {
			console.error(`decorators property is missing`);
		}

		super({
			parent: constructorOptions.parent,
			options: Object.assign(
				{},
				defaultOptions,
				constructorOptions.options
			),
			decorators: constructorOptions.decorators,
		});

		this._opacityArray = new ValueArray();
		this._opacityArray.push({
			name: "original",
			value: 1
		});

		this._mesh = null;
	}

	async init() {
		this._group = new THREE.Group();
		this._group.name = this._options.name;
		
		const geometry = new THREE.PlaneGeometry(1, 1, 1);

		let fragmentShader = parseDefines(this._options.defines);
		fragmentShader += this._options.fragmentShader;

		let material = new THREE.ShaderMaterial({
			uniforms: parseUniforms(this._options.uniforms),
			vertexShader: this._options.vertexShader,
			fragmentShader: fragmentShader,
		});

		material = Object.assign(material, this._options.material);
		
		this._mesh = new THREE.Mesh(geometry, material);
		this._group.add(this._mesh);

		this._parentObject = this._parent._group
			? this._parent._group
			: this._parent._scene
			? this._parent._scene
			: null;

		if (!this._parentObject) {
			console.warn("parent is missing both _group and _scene properties");
		}

		Object.values(this._childDecorators).map((d) => d.attach(this, {}))

		this.oninit();
	}

	async load() {
        this.updatePosition();

		this.updateUniforms(uniformsToObject(this._options.uniforms));

		if (!this._options.autoClean) {
			if (this._debug) {
				console.log("added ", this._options.name);
			}
			
			this._parentObject.add(this._group);
		}

		this.handleResize();

		this._parent._loopArray.push(this.loop);
		this._parent._resizeArray.push(this.handleResize);

		this.onload();
	}

	async unload() {
		this.cancelLoad();

		if (!this._options.autoClean) {
			if (this._debug) {
				console.log("removed ", this._options.name);
			}
	
			this._parentObject.remove(this._group);
		}

		Object.values(this._mesh.material.uniforms).forEach((u) => {
			if (u.type === "t") {
				u.value.dispose();
			}
		});

		this._mesh.parent.remove(this._mesh);
		this._mesh.material.dispose();
		this._mesh.geometry.dispose();

		this._mesh = null;

		this._parent._loopArray.remove(this.loop);
		this._parent._resizeArray.remove(this.handleResize);

		this.onunload();
	}

	activate() {
		if (this._options.autoClean) {
			if (this._debug) {
				console.log("added ", this._options.name);
			}
			
			this._parentObject.add(this._group);
		}
	}

	deactivate() {
		if (this._options.autoClean) {
			if (this._debug) {
				console.log("removed ", this._options.name);
			}
	
			this._parentObject.remove(this._group);
		}
	}
}