import * as THREE from "/node_modules/three/build/three.module.js";

import { createLoop } from "../../utils/loop.utils.js";

import resizeStore from "../../resize/resize.store.js";
import touchStore from "../../touch/touch.store.js";
import Composer from "./Composer.js";

import BaseClass from "./Base.js";

const namePrefix = "stage";
const name = "undefined";

const defaultOptions = {
	__namespace: "stage",

	shortName: name,
	namePrefix: namePrefix,
	name: namePrefix + "-" + name,

	debug: false,
	distance: 1000,
	camera: "perspective",
	defines: {
		DPI: {
			type: "f",
			value: touchStore.getData().touch
				? Math.min(1.5, window.devicePixelRatio)
				: Math.min(2, window.devicePixelRatio),
		},
	},

	background: 0x000000,

	antialias: true,
	alpha: true,
	premultipliedAlpha: true,

	logarithmicDepthBuffer: false,
	physicallyCorrectLights: false,
	toneMapping: THREE.ACESFilmicToneMapping,
	toneMappingExposure: 1,
	toneMappingWhitePoint: 1,
	outputEncoding: THREE.sRGBEncoding,

	shadowMap: {
		enabled: true,
		type: THREE.PCFShadowMap
	}
};

THREE.Cache.enabled = true;

function isWebGLEnabled() {
	let canvas = document.createElement("canvas");
	let gl =
		canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
	return gl && gl instanceof WebGLRenderingContext;
}

function isWebGL2Available() {
	try {
		var canvas = document.createElement("canvas");
		return !!(window.WebGL2RenderingContext && canvas.getContext("webgl2"));
	} catch (e) {
		return false;
	}
}

export default class Stage extends BaseClass {
	constructor(constructorOptions) {
		if (!constructorOptions.parent) {
			console.error(`parent property is missing`);
		}
		if (!constructorOptions.options) {
			console.error(`options property is missing`);
		}
		if (!constructorOptions.decorators) {
			console.error(`decorators property is missing`);
		}

		super({
			parent: constructorOptions.parent,
			options: Object.assign(
				{},
				defaultOptions,
				constructorOptions.options
			),
			decorators: constructorOptions.decorators,
		});

		this._debug = this._options.debug;
		this._loop = createLoop(this.loop);
	}

	beforeRender() {}

	async init() {
		if (!isWebGLEnabled()) return;

		let rendererOptions = {
			antialias: this._options.antialias,
			alpha: this._options.alpha,
			premultipliedAlpha: this._options.premultipliedAlpha,
		};

		if (isWebGL2Available()) {
			let canvas = document.createElement("canvas");
			let context = canvas.getContext("webgl2", rendererOptions);

			this._renderer = new THREE.WebGLRenderer({
				canvas: canvas,
				context: context,
			});
		} else {
			this._renderer = new THREE.WebGLRenderer(rendererOptions);
		}

		this._renderer.logarithmicDepthBuffer = this._options.logarithmicDepthBuffer;
		this._renderer.physicallyCorrectLights = this._options.physicallyCorrectLights;
		this._renderer.toneMapping = this._options.toneMapping;
		this._renderer.toneMappingExposure = this._options.toneMappingExposure;
		this._renderer.toneMappingWhitePoint = this._options.toneMappingWhitePoint;
		this._renderer.outputEncoding = this._options.outputEncoding;
		this._renderer.shadowMap.enabled = this._options.shadowMap.enabled;
		this._renderer.shadowMap.type = this._options.shadowMap.type;

		this._renderer.setPixelRatio(this._options.defines.DPI.value);

		let ww = this._parent.clientWidth;
		let wh = this._parent.clientHeight;
		this._renderer.setSize(ww, wh);

		this._renderer.domElement.style.width = "100%";
		this._renderer.domElement.style.height = "100%";

		this._renderer.setClearColor(this._options.background, 0);

		this._parent.appendChild(this._renderer.domElement);
		this._renderer.name = this._options.__namespace;

		if (this._options.camera === "perspective") {
			let distance = this._options.distance;
			let size = this._renderer.getSize(new THREE.Vector2());
			let fov =
				2 * (180 / Math.PI) * Math.atan(size.height / (2 * distance));

			this._camera = new THREE.PerspectiveCamera(fov, ww / wh, 1, 10000);
			this._camera.userData.distance = distance;
			this._camera.userData.originalDistance = 1000;

			this._camera.up = new THREE.Vector3(0, 1, 0);

			this._camera.position.set(0, 0, distance);
			this._camera.lookAt(0, 0, 0);
			this._camera.updateProjectionMatrix();
		} else if (this._options.camera === "orthographic") {
			this._camera = new THREE.OrthographicCamera(
				ww / -2,
				ww / 2,
				wh / 2,
				wh / -2,
				-10000,
				10000
			);
		} else {
			console.error(`unknown value for camera "${this._options.camera}"`);
		}

		this._scene = new THREE.Scene({
			background: this._options.background,
		});

		this._composer = new Composer(this._renderer);
		this._composer.setAutoupdate("auto");

		let sceneData = this._composer.renderPass(
			this._scene,
			this._camera,
			{
				msaa: true,
			}
		);

		this._composer.resize();

		this._scene = sceneData.scene;
		this._scene.name = this._options.name;

		await Promise.all(
			this._childDecorators.map((d) =>
				d.attach(this, {
					debug: this._options.debug,
					defines: this._options.defines,
				})
			)
		);

		this._loaded = false;
		this._active = false;

		await this.checkIfLoad();

		this.handleResize();
		this.checkIfActive();
		resizeStore.subscribe(this.handleResize);

		if (this._loopMode === "master") {
			window._masterLoop.push(this.loop);
		} else if (this._loopMode === "independent") {
			this._loop.start();
		}

		setTimeout(() => {
			this._parent.classList.add("ready");
		}, 0);

		this.oninit();
	}

	async destroy() {
		this._destruct = true;
		this._loop.kill();

		resizeStore.unsubscribe(this.handleResize);

		if (this.unload) {
			this.unload();
		}

		await Promise.all(
			Object.values(this._childDecorators).map((d) => d.detach(this, {}))
		);

		if (this._composer) {
			this._composer.dispose();
		}

		if (this._renderer) {
			this._renderer.forceContextLoss();
			this._renderer.domElement = null;
			this._renderer = null;
		}

		this.ondestroy();
	}

	handleResize() {
		let ww = this._parent.clientWidth;
		let wh = this._parent.clientHeight;

		this._ww = ww;
		this._wh = wh;

		if (this._renderer && this._composer) {
			this._renderer.setSize(ww, wh);

			this._composer.resize();

			let drawingBufferSize = this._renderer.getDrawingBufferSize(
				new THREE.Vector2()
			);

			this.updateUniforms({
				resolution: [drawingBufferSize.x, drawingBufferSize.y],
				screenResolution: [ww, wh],
			});
		}

		this._resizeArray.forEach((resizeFunc) => {
			resizeFunc();
		});

		this.onresize();
	}

	loop(multiplier) {
		if (this._destruct) return;

		this._loopArray.forEach(function (loopFunc) {
			loopFunc(multiplier);
		});

		this.onloop(multiplier);

		if (this._active) {
			this.beforeRender();
			this._composer.render();
		}
	}
}