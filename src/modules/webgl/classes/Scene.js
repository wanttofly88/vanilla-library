import dispatcher from "../../dispatcher.js";
import * as THREE from "/node_modules/three/build/three.module.js";
import { FunctionArray } from "../../utils/array.utils.js";
import {
	parseDefines,
	parseUniforms,
	uniformsToObject,
} from "../webgl.utils.js";
import Base from "./Base.js";
import Composer from "./Composer.js";
import { simpleVertexShader, simpleFragmentShader } from "../shaders.js";

const namePrefix = "scene";
const name = "undefined";

const defaultOptions = {
	autoload: false,
	shortName: name,
	name: namePrefix + "-" + name,

	sync: true,

	// camera
	camera: "orthographic",
	cameraDistance: 1000,

	// postprocessing
	uniforms: {},
	defines: {},
	fragmentShader: simpleFragmentShader(),
	msaa: false,
	background: 0x000000,

	decorators: [],
};


class Scene extends Base {
	constructor(constructorOptions) {
		if (!constructorOptions.parent) {
			console.error(`parent property is missing`);
		}
		if (!constructorOptions.options) {
			console.error(`options property is missing`);
		}
		if (!constructorOptions.decorators) {
			console.error(`decorators property is missing`);
		}

		super({
			parent: constructorOptions.parent,
			options: Object.assign(
				{},
				defaultOptions,
				constructorOptions.options
			),
			decorators: constructorOptions.decorators,
		});
	}

	async init() {
		let size = this._renderer.getSize(new THREE.Vector2());
		var drawingBufferSize = this._renderer.getDrawingBufferSize(
			new THREE.Vector2()
		);

		let ww = size.width;
		let wh = size.height;
		let camera, fov;

		let distance = this._options.cameraDistance;

		if (this._options.camera === "orthographic") {
			camera = new THREE.OrthographicCamera(
				ww / -2,
				ww / 2,
				wh / 2,
				wh / -2,
				-10000,
				10000
			);
		} else if (this._options.camera === "perspective") {
			camera = new THREE.PerspectiveCamera(60, ww / wh, 1, 10000);
			camera.userData.distance = distance;

			fov = 2 * (180 / Math.PI) * Math.atan(size.height / (2 * distance));
			camera.fov = fov;

			camera.position.set(0, 0, distance);
			camera.lookAt(0, 0, 0);
			camera.up = new THREE.Vector3(0, 1, 0);
			camera.updateProjectionMatrix();
		}

		this._scene = new THREE.Scene({
			background: this._options.background,
		});

		this._scene._inactive = false;
		this._scene._name = this._name;

		this._camera = camera;

		this._composer = new Composer(this._renderer);
		this._composer.setAutoupdate("auto");

		this._composer.renderPass(this._scene, this._camera, {
			msaa: this._options.msaa,
		});

		let fragmentShader = parseDefines(this._options.defines);

		fragmentShader += this._options.fragmentShader;

		let composerData = this._composer.shaderPass({
			uniforms: Object.assign(
				{
					resolution: {
						type: "v2",
						value: [
							drawingBufferSize.width,
							drawingBufferSize.height,
						],
					},
					screenResolution: {
						type: "v2",
						value: [size.width, size.height],
					},
				},
				parseUniforms(this._options.uniforms)
			),
			fragmentShader: fragmentShader,
			vertexShader: simpleVertexShader(),
		});

		this._resultMesh = composerData.mesh;
		this._parentObject.add(this._resultMesh);

		this.handleResize();

		dispatcher.dispatch({
			type: "scene:ready",
			name: this._name,
		});

		await Promise.all(
		    Object.values(this._childDecorators).map((d) => d.attach(this, {}))
		);

		this.oninit();
	}

	async destroy() {
		if (this.unload) {
		    this.unload();
		}

		await Promise.all(
		    Object.values(this._childDecorators).map((d) => d.detach(this, {}))
		);

		this._parentObject.remove(this._resultMesh);

		this._composer.dispose();
		this._scene.dispose();

		this.ondestroy();
	}

	activate() {
		this._scene._inactive = false;
	}

	deactivate() {
		this._scene._inactive = true;
	}

	handleResize() {
		let fov, distance;
		let size = this._renderer.getSize(new THREE.Vector2());
		let camera = this._camera;

		this._composer.resize();

		if (camera.type === "PerspectiveCamera") {
			distance = camera.userData.distance;
			fov = 2 * (180 / Math.PI) * Math.atan(size.height / (2 * distance));
			camera.fov = fov;
			camera.position.set(
				camera.position.x,
				camera.position.y,
				camera.userData.distance
			);
			camera.lookAt(0, 0, 0);
			camera.updateProjectionMatrix();
		} else {
			camera.left = size.width / -2;
			camera.right = size.width / 2;
			camera.top = size.height / 2;
			camera.bottom = size.height / -2;
			camera.updateProjectionMatrix();
		}

	    this._resizeArray.elements.forEach(function (resizeFunc) {
	        if (typeof resizeFunc === "function") {
	            resizeFunc();
	        }
	    });

	    this.onresize();
	}
}

export default Scene;