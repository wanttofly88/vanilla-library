export default function() {
	return `
		varying vec3 vU;
		varying vec3 vNormal;
		varying vec3 vViewPosition;

		void setMatcapVariables(vec4 viewModelPosition) {
			vNormal = normalMatrix * normal;
			vU = normalize(vec3(modelViewMatrix * vec4(position, 1.0)));
			vViewPosition = viewModelPosition.xyz;
		}
	`
};