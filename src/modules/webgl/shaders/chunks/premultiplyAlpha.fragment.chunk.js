export default function() {
	return `
		gl_FragColor.rgb *= gl_FragColor.a;
	`;
}