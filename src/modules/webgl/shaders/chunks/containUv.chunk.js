export default function() {
    return `
        vec2 containUv(vec2 p, vec2 resolution, vec2 textureSize) {
			float rs = resolution.x / resolution.y;
			float ri = textureSize.x / textureSize.y;

			vec2 new = rs > ri ? 
				vec2(textureSize.x * resolution.y / textureSize.y, resolution.y):
				vec2(resolution.x, textureSize.y * resolution.x / textureSize.x);

			vec2 offset = (rs > ri ? 
				vec2((new.x - resolution.x) / 2.0, 0.0):
			 	vec2(0.0, (new.y - resolution.y) / 2.0)) / new;

			return p * resolution / new + offset;
        }
    `
}