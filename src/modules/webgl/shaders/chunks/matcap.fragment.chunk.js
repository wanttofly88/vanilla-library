export default function (overrides) {
	if (!overrides) overrides = {};

	const matcap = `
		uniform sampler2D tMatcap;
	`;

	const getTexture = `
		vec4 getTexture(vec2 uv) {
			return texture2D(tMatcap, uv);
		}
	`;

	const getUv = `
		vec2 getUv(vec2 uv) {
			return uv;
		}
	`

	return `
		varying vec3 vU;
		uniform sampler2D tNormalMap;

		uniform float uNormalDepth;
		uniform vec2 uNormalRepeat;

		varying vec3 vNormal;
		varying vec3 vViewPosition;

		${overrides.matcap ? overrides.matcap : matcap}

		${overrides.getTexture ? overrides.getTexture : getTexture}

		${overrides.getUv ? overrides.getUv : getUv}

		vec3 perturbNormal2Arb(vec3 eyePos, vec3 surfNorm) {
			if (uNormalDepth == 0.0) {
				return surfNorm;
			} else {
				vec2 normalUv = vUv;

				vec2 normalScale = vec2(uNormalDepth, -uNormalDepth);

				vec2 normUvMapped = (mod(vUv.xy * uNormalRepeat, 1.0));
				vec4 nColor = texture2D(tNormalMap, normUvMapped) * 2.0 - 1.0;

				vec3 Q1 = dFdx(eyePos.xyz);
				vec3 Q2 = dFdy(eyePos.xyz);
				vec2 st1 = dFdx(normalUv.st);
				vec2 st2 = dFdy(normalUv.st);

				vec3 T = normalize(Q1 * st2.t - Q2 * st1.t);
				vec3 B = normalize(-Q1 * st2.s + Q2 * st1.s);		
				vec3 N = normalize(surfNorm);

				// invert S, T when the UV direction is backwards (from mirrored faces),
				// otherwise it will do the normal mapping backwards.
				vec3 NfromTB = cross(T, B);
				if (dot(NfromTB, N) < 0.0) {
					T *= -1.0;
					B *= -1.0;
				}

				vec3 mapN = nColor.rgb;
				mapN.xy = normalScale * mapN.xy;
				mat3 tsn = mat3(T, B, N);
				return normalize(tsn * mapN);
			}
		}

		vec4 matcap() {
			vec3 normal = normalize( vNormal );
			vec3 viewDir = normalize( vViewPosition );

			// normal = perturbNormal2Arb(viewDir, normal);

			vec3 x = normalize( vec3( viewDir.z, 0.0, - viewDir.x ) );
			vec3 y = cross( viewDir, x );
			vec2 uv = vec2( dot( x, normal ), dot( y, normal ) ) * 0.495 + 0.5; // 0.495 to remove artifacts caused by undersized matcap disks

			uv = getUv(uv);
			// return vec4(normal, 1.0);
			return getTexture(uv.xy);
		}`;
}