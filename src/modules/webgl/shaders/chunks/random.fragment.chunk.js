export default function() {
	return`
		highp float random() {
			const highp float a = 12.9898, b = 78.233, c = 43758.5453;
			highp float dt = dot( gl_FragCoord.xy, vec2( a,b ) ), sn = mod( dt, M_PI );
			return fract(sin(sn) * c);
		}
	`
};