const defaultOptions = {
	mask: (mixValueSting, amimationIndexString) => `${ mixValueSting }`,
	required: () => ``
}

export default function(options) {
	options = Object.assign({}, defaultOptions, options);

	return `
		uniform vec4 uMask;

		uniform sampler2D tDiffuse0;
		uniform sampler2D tDiffuse1;
		uniform sampler2D tDiffuse2;
		uniform sampler2D tDiffuse3;
		uniform sampler2D tDiffuse4;

		${ options.required() }

		vec4 slider4(vec2 uv) {
			vec4 color;

			if (uMask.w == 1.0) {
				return texture2D(tDiffuse4, uv);
			} else if (uMask.z == 1.0) {
				color = mix(texture2D(tDiffuse3, uv), texture2D(tDiffuse4, uv), ${ options.mask("uMask.w", "3") });
				return color;
			} else if (uMask.y == 1.0) {
				color = mix(texture2D(tDiffuse2, uv), texture2D(tDiffuse3, uv), ${ options.mask("uMask.z", "2") });
				color = mix(color, texture2D(tDiffuse4, uv), ${ options.mask("uMask.w", "3") });
				return color;
			} else if (uMask.x == 1.0) {
				color = mix(texture2D(tDiffuse1, uv), texture2D(tDiffuse2, uv), ${ options.mask("uMask.y", "1") });
				color = mix(color, texture2D(tDiffuse3, uv), ${ options.mask("uMask.z", "2") });
				color = mix(color, texture2D(tDiffuse4, uv), ${ options.mask("uMask.w", "3") });
				return color;
			} else {
				color = mix(texture2D(tDiffuse0, uv), texture2D(tDiffuse1, uv), ${ options.mask("uMask.x", "0") });
				color = mix(color, texture2D(tDiffuse2, uv), ${ options.mask("uMask.y", "1") });
				color = mix(color, texture2D(tDiffuse3, uv), ${ options.mask("uMask.z", "2") });
				color = mix(color, texture2D(tDiffuse4, uv), ${ options.mask("uMask.w", "3") });
				return color;
			}
		}

		vec4 slider3(vec2 uv) {
			vec4 color;

			if (uMask.z == 1.0) {
				return texture2D(tDiffuse3, uv);
			} else if (uMask.y == 1.0) {
				color = mix(texture2D(tDiffuse2, uv), texture2D(tDiffuse3, uv), ${ options.mask("uMask.z", "2") });
				return color;
			} else if (uMask.x == 1.0) {
				color = mix(texture2D(tDiffuse1, uv), texture2D(tDiffuse2, uv), ${ options.mask("uMask.y", "1") });
				color = mix(color, texture2D(tDiffuse3, uv), ${ options.mask("uMask.z", "2") });
				return color;
			} else {
				color = mix(texture2D(tDiffuse0, uv), texture2D(tDiffuse1, uv), ${ options.mask("uMask.x", "0") });
				color = mix(color, texture2D(tDiffuse2, uv), ${ options.mask("uMask.y", "1") });
				color = mix(color, texture2D(tDiffuse3, uv), ${ options.mask("uMask.z", "2") });
				return color;
			}
		}

		vec4 slider2(vec2 uv) {
			vec4 color;

			if (uMask.y == 1.0) {
				return texture2D(tDiffuse2, uv);
			} else if (uMask.x == 1.0) {
				color = mix(texture2D(tDiffuse1, uv), texture2D(tDiffuse2, uv), ${ options.mask("uMask.y", "1") });
				return color;
			} else {
				color = mix(texture2D(tDiffuse0, uv), texture2D(tDiffuse1, uv), ${ options.mask("uMask.x", "0") });
				color = mix(color, texture2D(tDiffuse2, uv), ${ options.mask("uMask.y", "1") });
				return color;
			}
		}
	`;
}