import {FunctionArray} from "../utils/array.utils.js";
import globalOptions from "../../options.js";

class MasterLoop {
	constructor () {
		this.init = this.init.bind(this);
		this.loop = this.loop.bind(this);
		this.push = this.push.bind(this);
		this.remove = this.remove.bind(this);

		this._loopArray = new FunctionArray();
		this._initialized = false;
	}

	init() {
		if (this._initialized) return;
		this._initialized = true;

		this.loop();
	}

	loop() {
		this._loopArray.forEach((f) => {
			if (globalOptions.asyncMasterLoop) {
				setTimeout(f, 0);
			} else {
				f();
			}
		});

		requestAnimationFrame(this.loop);
	}

	push(func) {
		return this._loopArray.push(func);
	}

	remove(func) {
		return this._loopArray.remove(func);
	}
}

let masterLoop = new MasterLoop();
window._masterLoop = masterLoop;

export default {
	init: masterLoop.init
};