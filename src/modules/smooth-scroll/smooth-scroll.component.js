import dispatcher from "../dispatcher.js";
import resizeStore from "../resize/resize.store.js";
import smoothScrollStore from "./smooth-scroll.store.js";
import scrollStore from "../scroll/scroll.store.js";
import touchStore from "../touch/touch.store.js";
import { offset } from "../utils/dom.utils.js";
import { FunctionArray } from "../utils/array.utils.js";
import { datasetToOptions } from "../utils/component.utils.js";

import defaultAnimation from "./animations/default-animation.decorator.js";
import sectionAnimation from "./animations/section-animation.decorator.js";

import popupControlsDecorator from "./controls/popup.decorator.js";
import focusControlsDecorator from "./controls/focus.decorator.js";

/*
element that will be scrolled
    element with overflow: auto || html
	.scroll-root || body
		.scroll-container
			smooth-scroll


example: 

<html>
    <body>
        <div class="page-wrapper scroll-container">
            <smooth-scroll data-id="smooth-scroll" data-animation="section">
                <!--if data-animation is set to section, additional element .scroll-section-container is needed-->
                <main class="scroll-section-container">
                    <section></section>
                    <section></section>
                </main>
            </smooth-scroll>

            <popup-component>
                <div class="scroll-container">
                    <smooth-scroll data-id="popup-scroll">
                        ...
                    </smooth-scroll>
                </div>
            </popup-component>
        </div>
    </body>
</html>
*/

const decorators = [popupControlsDecorator, focusControlsDecorator];

const animations = {
    default: defaultAnimation,
    section: sectionAnimation,
};

const defaultOptions = {
    id: "undefined",
    animation: "default",
    easing: 0.06,
};

class ElementClass extends HTMLElement {
    constructor(self) {
        self = super(self);
        self.init.call(self);
    }

    init() {
        this._options = datasetToOptions(this.dataset, defaultOptions);

        this.handleResize = this.handleResize.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.handleDispatcher = this.handleDispatcher.bind(this);
        this.preventInnerScroll = this.preventInnerScroll.bind(this);
        this.loop = this.loop.bind(this);

        this._enabledFlagsArray = new FunctionArray();
        this._enabledFlagsArray.push(() => true);

        this._active = false;
        this._targetScroll = 0;
        this._currentScroll = 0;

        if (window._masterLoop) {
            this._loopMode = "master";
        } else {
            this._loopMode = "independent";
        }
    }

    connectedCallback() {
        if (touchStore.getData().touch) return;

        this._id = this._options.id;

        this._innerContainer = this.closest(".scroll-container"); //fixed, overflow: hidden element
        if (!this._innerContainer) {
            console.error('"scroll-container" element is missing');
            return;
        }
        
        this.style.display = "block";

        this._root = this.getRootElement(this);
        this._outerContainer = this._root.parentNode; //element with native scroll to translate into smooth scroll

        this._outerContainer.classList.add("smooth-scroll-outer");

        animations[this._options.animation].attach(this, {
            easing: this._options.easing,
        });

        dispatcher.subscribe(this.handleDispatcher);

        this._innerContainer.style.position = "fixed";
        this._innerContainer.style.left = "0px";
        this._innerContainer.style.right = "0px";
        this._innerContainer.style.top = "0px";
        this._innerContainer.style.bottom = "0px";
        this._innerContainer.style.overflow = "hidden";
        this._innerContainer.style.transform = "translateZ(0px)";

        this._innerContainer.addEventListener(
            "scroll",
            this.preventInnerScroll
        );

        decorators.forEach((d) => d.attach(this, this._options));

        dispatcher.dispatch({
            type: "smooth-scroll:add",
            id: this._id,
            top: scrollStore.getData().top,
        });

        this.checkIfEnabled();
        scrollStore.subscribe(this.handleScroll);
    }

    disconnectedCallback() {
        this._destroy = true;
        this.disable();

        animations[this._options.animation].detach(this);

        dispatcher.unsubscribe(this.handleDispatcher);
        scrollStore.unsubscribe(this.handleScroll);

        decorators.forEach((d) => d.detach(this));

        dispatcher.dispatch({
            type: "smooth-scroll:remove",
            id: this._id,
        });
    }

    preventInnerScroll() {
        this._innerContainer.scrollTo(0, 0);
    }

    getRootElement(el) {
        if (
            el.classList.contains("scroll-root") ||
            el.tagName.toLowerCase() === "body"
        ) {
            return el;
        } else {
            return this.getRootElement(el.parentNode);
        }
    }

    loop(options) {
        if (this._destroy) return;
        if (!this._active) return;

        options = Object.assign(
            {
                targetScroll: this._targetScroll,
            },
            options
        );

        if (!this._popupId && scrollStore.getData().locked) {
            // for animated scrolling
            options.easing = 1;
        }

        if (
            !this._popupId &&
            scrollStore.getData().locked &&
            !scrollStore.getData().synth
        ) {
            this._locked = true;
        } else {
            this._locked = false;
        }

        this._currentScroll = this._decorators["animation"].animate(options);

        if (this._loopMode === "independent") {
            this._raf = requestAnimationFrame(this.loop);
        }
    }

    checkIfEnabled() {
        if (this._active && !this._enabledFlagsArray.boolReduce()) {
            this._active = false;

            // scrollStore.unsubscribe(this.handleScroll);
            if (this._loopMode === "independent") {
                cancelAnimationFrame(this._raf);
            } else if (this._loopMode === "master") {
                window._masterLoop.remove(this.loop);
            }

            resizeStore.unsubscribe(this.handleResize);

            dispatcher.dispatch({
                type: "smooth-scroll:disable",
                id: this._id,
            });
        } else if (!this._active && this._enabledFlagsArray.boolReduce()) {
            // let others disable first
            this._active = true;

            setTimeout(() => {
                this.handleResize();
                resizeStore.subscribe(this.handleResize);

                if (this._popupId) {
                    this._currentScroll = 0;
                    this._targetScroll = 0;
                    window.scrollTo(0, 0);
                } else {
                    window.scrollTo(0, scrollStore.getData().top);
                }

                this.handleScroll();

                dispatcher.dispatch({
                    type: "smooth-scroll:enable",
                    id: this._id,
                });

                if (this._loopMode === "independent") {
                    cancelAnimationFrame(this._raf);
                } else if (this._loopMode === "master") {
                    window._masterLoop.push(this.loop);
                }

                this.loop({
                    easing: 1,
                });

                this.handleScroll();
            }, 0);
        }
    }

    handleDispatcher(e) {
        if (e.type === "scroll:lock") {
            this.handleResize();
        }

        if (e.type === "scroll:unlock") {
            this.handleResize();
        }
    }

    handleResize() {
        if (!this._active) return;

        let height = this.clientHeight;

        if (!this._popupId && scrollStore.getData().locked) {
            height = resizeStore.getData().height;
        } else {
            height = this.clientHeight;
        }

        let pw = document.getElementsByClassName("page-wrapper")[0];
        let width = pw.clientWidth;

        this._root.style.height = height + "px";

        let diff = this._root.clientWidth - width;

        if (diff !== 0) {
            dispatcher.dispatch({
                type: "scroll:set-indents",
                diff: diff,
            });
        }
    }

    handleScroll() {
        if (!this._active) return;

        let scrolled;

        if (this._popupId) {
            scrolled = window.scrollY;
        } else {
            if (scrollStore.getData().locked) {
                scrolled = scrollStore.getData().top;
            } else {
                scrolled = window.scrollY;
            }
        }

        if (!this._active) {
            this._currentScroll = scrolled;
            this._targetScroll = scrolled;
        } else {
            this._targetScroll = scrolled;
        }
    }
}

customElements.define("smooth-scroll", ElementClass);

export default ElementClass;
