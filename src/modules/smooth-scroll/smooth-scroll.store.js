import dispatcher from '../dispatcher.js';
import EventEmitter from '../utils/EventEmitter.js';

var eventEmitter = new EventEmitter();
var active = null;

var _handleEvent = function(e) {
	if (e.type === 'smooth-scroll:enable') {
		if (active === e.id) return;
		active = e.id;
		window.__smoothScroll = e.id;
		eventEmitter.dispatch();

	} else if (e.type === 'smooth-scroll:disable') {
		if (active !== e.id) return;
		active = null;
		window.__smoothScroll = null;
		eventEmitter.dispatch();
	}
}

var getData = function() {
	return {
		active: active
	}
}

var _init = function() {
	dispatcher.subscribe(_handleEvent);
}

_init();

export default {
	subscribe: eventEmitter.subscribe.bind(eventEmitter),
	unsubscribe: eventEmitter.unsubscribe.bind(eventEmitter),
	getData: getData
}