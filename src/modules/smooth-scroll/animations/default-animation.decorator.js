import { attach, detach, update } from "../../utils/decorator.utils.js";
import dispatcher from "../../dispatcher.js";
import smoothScrollStore from "../smooth-scroll.store.js";

const name = "animation"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

// interpolation - функция инрерполяции
// easing - 3-е значение, которое будет передано в эту функцию
const defaultOptions = {
    name: name,
    interpolation: function (x0, x1, a) {
        return (1 - a) * x0 + a * x1;
    },
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleDispatcher = this.handleDispatcher.bind(this);

        this._overrideEasing = null;
        this._currentScroll = 0;
    }

    init() {
        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    animate(options) {
        let previousScroll = this._currentScroll;

        if (this._parent._locked) {
            return previousScroll;
        }

        options = Object.assign({}, this._options, options);
        let targetScroll = options.targetScroll;

        if (Math.abs(this._currentScroll - targetScroll) < 1) {
            this._currentScroll = targetScroll;
        } else {
            this._currentScroll = this._options.interpolation(
                this._currentScroll,
                targetScroll,
                options.easing
            );
        }

        if (Math.abs(this._currentScroll - previousScroll) > 0.1) {
            this._parent.style.transform =
                "translateY(" + -this._currentScroll + "px) translateZ(0px)";

            let event = new CustomEvent("smooth-scroll", {
                detail: {
                    top: this._currentScroll,
                    id: this._id,
                },
            });

            this._parent.dispatchEvent(event);
        }

        return this._currentScroll;
    }

    handleDispatcher(e) {
        if (e.type === "smooth-scroll:instant-scroll") {
            if (smoothScrollStore.getData().active !== this._parent._id) return;
            this._overrideEasing = 1;
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};