import { attach, detach, update } from "../../utils/decorator.utils.js";
import dispatcher from "../../dispatcher.js";
import smoothScrollStore from "../smooth-scroll.store.js";
import resizeStore from "../../resize/resize.store.js";
import { offset } from "../../utils/dom.utils.js";
import { positionDecorator } from "../../utils/dom.utils.js";

const name = "animation"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

// interpolation - функция инрерполяции
// easing - 3-е значение, которое будет передано в эту функцию
const defaultOptions = {
    name: name,
    interpolation: function (x0, x1, a) {
        return (1 - a) * x0 + a * x1;
    },
    layerOptimization: true,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleDispatcher = this.handleDispatcher.bind(this);
        this.handleResize = this.handleResize.bind(this);

        this._overrideEasing = null;
        this._currentScroll = 0;
    }

    init() {
        this.update();

        resizeStore.subscribe(this.handleResize);
        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        resizeStore.unsubscribe(this.handleResize);
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    update() {
        this._container = this._parent.getElementsByClassName(
            "scroll-section-container"
        )[0];
        if (!this._container) {
            console.error(
                "element with class 'scroll-section-container' is missing"
            );
            return;
        }

        this._sections = Array.prototype.map.call(
            this._container.children,
            (element) => {
                positionDecorator.attach(element);
                if (!this._options.layerOptimization) {
                    element.style.willChange = "transform";
                }

                element._position.push({
                    name: "smooth-scroll",
                    value: {
                        top: -this._currentScroll,
                        left: 0,
                        width: 0,
                        height: 0,
                    },
                    ignored: true,
                });

                element._position.setTransforms();

                return {
                    element: element,
                    offset: offset(element).top,
                    height: element.clientHeight,
                };
            }
        );
    }

    animate(options) {
        let previousScroll = this._currentScroll;

        if (this._parent._locked) {
            return previousScroll;
        }

        options = Object.assign({}, this._options, options);
        let targetScroll = options.targetScroll;

        if (Math.abs(this._currentScroll - targetScroll) < 1) {
            this._currentScroll = targetScroll;
        } else {
            this._currentScroll = this._options.interpolation(
                this._currentScroll,
                targetScroll,
                options.easing
            );
        }

        if (Math.abs(this._currentScroll - previousScroll) > 0.1) {
            let wh = resizeStore.getData().height;

            let topBoundry = Math.max(previousScroll, this._currentScroll);
            let bottomBoundry = Math.min(previousScroll, this._currentScroll);

            this._sections.forEach((s) => {
                if (!s.element.parentNode || !s.element._position) return;

                if (
                    topBoundry + wh > s.offset &&
                    bottomBoundry < s.offset + s.height
                ) {
                    if (this._options.layerOptimization && !s.layered) {
                        s.element.style.willChange = "transform";
                    }

                    s.layered = true;

                    s.element._position.set({
                        name: "smooth-scroll",
                        value: {
                            top: -this._currentScroll,
                            left: 0,
                            width: 0,
                            height: 0,
                        },
                    });
                } else {
                    if (s.layered) {
                        if (this._options.layerOptimization) {
                            s.element.style.willChange = "";
                        }

                        s.element._position.set({
                            name: "smooth-scroll",
                            value: {
                                top: -this._currentScroll,
                                left: 0,
                                width: 0,
                                height: 0,
                            },
                        });
                    }

                    s.layered = false;
                }
            });

            let event = new CustomEvent("smooth-scroll", {
                detail: {
                    top: this._currentScroll,
                    id: this._id,
                },
            });

            this._parent.dispatchEvent(event);
        }

        return this._currentScroll;
    }

    handleResize() {
        this._sections.forEach((s) => {
            if (!s.element.parentNode || !s.element._position) return;

            s.offset = offset(s.element).top;
            s.height = s.element.clientHeight;

            s.element._position.set({
                name: "smooth-scroll",
                value: {
                    top: -this._currentScroll,
                    left: 0,
                    width: 0,
                    height: 0,
                },
            });
        });
    }

    handleDispatcher(e) {
        if (e.type === "smooth-scroll:instant-scroll") {
            if (smoothScrollStore.getData().active !== this._parent._id) return;
            this._overrideEasing = 1;
        }

        if (e.type === "content:replaced") {
            this.update();
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};