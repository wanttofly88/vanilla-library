import dispatcher from "../../dispatcher.js";
import { attach, detach, update } from "../../utils/decorator.utils.js";
import { offset } from "../../utils/dom.utils.js";

import resizeStore from "../../resize/resize.store.js";
import keyboardEvents from "../../events/keyboard.view.js";
import smoothScrollStore from "../smooth-scroll.store.js";

const name = "focus"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleDispatcher = this.handleDispatcher.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
    }

    init() {
        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    handleDispatcher(e) {
        if (e.type === "keyboard:tab" || e.type === "keyboard:shift-tab") {
            if (smoothScrollStore.getData().active !== this._parent._id) return;
            this.handleFocus();
        }
    }

    handleFocus() {
        this._parent.preventInnerScroll();

        setTimeout(() => {
            this._parent.preventInnerScroll();

            let activeElement = document.activeElement;
            let rect = activeElement.getBoundingClientRect();

            let top = rect.top;
            let bottom = rect.bottom;
            let vh = resizeStore.getData().height;

            if (
                this._parent.contains(activeElement) &&
                (top < this._options.anchorOffset || bottom > vh)
            ) {
                let of = offset(activeElement).top;

                dispatcher.dispatch({
                    type: "smooth-scroll:instant-scroll",
                });
                window.scrollTo(0, of - this._options.anchorOffset);
            }
        }, 0);
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};