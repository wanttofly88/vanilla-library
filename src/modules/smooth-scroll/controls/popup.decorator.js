import { attach, detach, update } from "../../utils/decorator.utils.js";
import popupStore from "../../popup/popup.store.js";

const name = 'popup'; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handlePopup = this.handlePopup.bind(this);
        this.checkEnabled = this.checkEnabled.bind(this);
    }

    init() {
        let popup = this._parent.closest("popup-component");

        if (popup) {
            this._popupId = popup.getAttribute(
                "data-id"
            );
        } else {
            this._popupId = null;
        }

        this._parent._popupId = this._popupId;

        this._parent._enabledFlagsArray.push(this.checkEnabled);

        this.handlePopup();
        popupStore.subscribe(this.handlePopup);
    }

    destroy() {
        popupStore.unsubscribe(this.handlePopup);
    }

    checkEnabled() {
        return popupStore.getData().active === this._popupId;
    }

    handlePopup() {
        this._parent.checkIfEnabled();
    };
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};