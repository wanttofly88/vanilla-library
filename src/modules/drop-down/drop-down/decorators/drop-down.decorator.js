import { attach, detach, update } from "../../utils/decorator.utils.js";
import { transition } from "../../utils/animation.utils.js";
import { datasetToOptions } from "../../utils/component.utils.js";
import { getFocusable } from "../../utils/dom.utils.js";
import dispatcher from "../../dispatcher.js";
import defaultAnimation from "../animations/default-animation.decorator.js";

const name = "drop-down"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const animations = {
    default: defaultAnimation
}

const defaultOptions = {
    name: name,
    animation: "default", // имя анимации из объекта выше
    closeOutside: false, // закрытие при клике снаружи
    classButton: "button", // класс кнопки открытия
    classOuter: "outer", // класс внутреннего контейнера
    classInner: "inner",  // класс внешнего открытия
    classClose: "close", // класс кнопки закрытия
    openDuration: 0.3, // скорость открытия в секнудах
    closeDuration: 0.45, // скорость закрытия в секнудах
    maxHeight: Infinity // максимальная высота
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.handleDispatcher = this.handleDispatcher.bind(this);
        this.handleDocumentClick = this.handleDocumentClick.bind(this);

        this._active = undefined;
    }

    init() {
        this._options = datasetToOptions(this._parent.dataset, this._options);

        this._button = this._parent.getElementsByClassName(
            this._options.classButton
        )[0];

        this._outer = this._parent.getElementsByClassName(this._options.classOuter)[0];
        this._inner = this._parent.getElementsByClassName(this._options.classInner)[0];

        animations[this._options.animation].attach(this, {
            openDuration: this._options.openDuration,
            closeDuration: this._options.closeDuration
        });

        this._button.setAttribute("aria-haspopup", "true");
        this._button.setAttribute("aria-expanded", "false");

        this._button.addEventListener("focus", this.handleFocus);
        this._button.addEventListener("blur", this.handleBlur);

        this._button.addEventListener("click", (e) => {
            if (this._active) {
                this.close();
            } else {
                this.open();
            }
        });

        this._closeButton = this._parent.getElementsByClassName(this._options.classClose)[0];

        if (this._closeButton) {
            this._closeButton.addEventListener("click", (e) => {
                e.stopPropagation();
                e.preventDefault();
                if (this._active) {
                    this.close();
                }
            });
        }

        if (this._parent.classList.contains("active")) {
            this.open({
                duration: 0
            });
        } else {
            this.close({
                duration: 0
            });
        }

        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        document.removeEventListener("click", this.handleDocumentClick);
        dispatcher.unsubscribe(this.handleDispatcher);

        animations[this._options.animation].detach(this);
    }

    open(options) {
        if (this._active === true) return;
        this._active = true;

        options = Object.assign({
            duration: this._options.openDuration
        }, options);

        this._parent.classList.add("active");
        this._button.setAttribute("aria-expanded", "true");

        let focusable = getFocusable(this._inner);
        focusable.forEach((f) => {
            f.setAttribute("tabindex", 0);
        });

        document.addEventListener("click", this.handleDocumentClick);

        this._decorators.animation.open();

        transition(
            this._outer,
            options.duration,
            {
                height: Math.min(this._options.maxHeight, this._inner.clientHeight) + "px",
            },
            {
                ease: window._vars.ease.css.ease,
            }
        );

        clearTimeout(this._openTO);

        this._openTO = setTimeout(() => {
            if (this._options.maxHeight < this._inner.clientHeight) {
                this._outer.style.overflow = "auto";
                this._outer.style.height = Math.min(this._options.maxHeight, this._inner.clientHeight) + "px";
            } else {
                this._outer.style.height = "auto";
            }
        }, options.duration * 1000);

        this._parent.dispatchEvent(new Event("open"));
    };

    close(options) {
        if (this._active === false) return;
        this._active = false;

        options = Object.assign({
            duration: this._options.closeDuration
        }, options);

        this._parent.classList.remove("active");
        this._button.setAttribute("aria-expanded", "false");

        let focusable = getFocusable(this._inner);
        focusable.forEach((f) => {
            f.setAttribute("tabindex", -1);
        });

        this._decorators.animation.open();

        document.removeEventListener("click", this.handleDocumentClick);

        let height;
        if (this._options.maxHeight < this._inner.clientHeight) {
            height = this._options.maxHeight;
        } else {
            height = this._inner.clientHeight;
        }

        this._outer.style.overflow = "hidden";

        if (options.duration) {
            transition(this._outer, 0, {
                height: height + "px"
            });

            clearTimeout(this._openTO);

            this._openTO = setTimeout(() => {
                transition(
                    this._outer,
                    options.duration,
                    {
                        height: "0px"
                    }, {
                        ease: window._vars.ease.css.ease
                    }
                );  
            }, 20);
        } else {
            transition(this._outer, 0, {
                height: "0px"
            });
        }

        this._parent.dispatchEvent(new Event("close"));
    };

    handleFocus() {
        this._linkFocus = true;
    }

    handleBlur() {
        this._linkFocus = false;
    }

    handleDispatcher(e) {
        if (e.type === "keyboard:tab" || e.type === "keyboard:shift-tab") {
            if (!this._active) return;

            setTimeout(() => {
                let activeElement = document.activeElement;
                if (!this._parent.contains(activeElement)) {
                    this.close();
                }
            }, 0);
        }

        if (e.type === "keyboard:enter" && this._linkFocus) {
            e.event.preventDefault();

            if (!this._active) {
                this.open();
            } else {
                this.close();
            }
        }

        if (e.type === "drop-down:close") {
            if (e.element === this._parent) {
                this.close(e);
            }
        }

        if (e.type === "drop-down:open") {
            if (e.element === this._parent) {
                this.open(e);
            }
        }
    }

    handleDocumentClick(e) {
        if (!this._options.closeOutside) return;

        if (this._parent.contains(e.target) ||
            this._parent === e.target) return;

        this.close();
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};