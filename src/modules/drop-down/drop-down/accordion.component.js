import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";

const decorators = [];

const defaultOptions = {
	__namespace: "",
};

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		this._dropDowns = Array.prototype.slice.call(this.getElementsByTagName('drop-down'));
		this._dropDowns.forEach((dd) => {
			dd.addEventListener('open', () => {
				this._dropDowns.forEach((od) => {
					if (od !== dd) {
						dispatcher.dispatch({
							type: "drop-down:close",
							element: od,
							duration: 0.25
						});
					}
				});
			});
		});

		// code goes here
		decorators.forEach((d) => d.attach(this, this._options));
	}

	disconnectedCallback() {
		// code goes here
		decorators.forEach((d) => d.detach(this, this._options));
	}
}

customElements.define("accordion-component", ElementClass);

export default ElementClass;