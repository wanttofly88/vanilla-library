import { attach, detach, update } from "../../utils/decorator.utils.js";

const name = "animation"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
    openDuration: 0.3, // скорость открытия в секнудах
    closeDuration: 0.45, // скорость закрытия в секнудах
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;
    }

    init() {}

    destroy() {}

    open() {}

    close() {}
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};