import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import dropDownDecorator from "./decorators/drop-down.decorator.js";

// <drop-down>
// 		<button class="button">header</button>
//		<div class="outer">
//			<div class="inner">
//				.......
//				.......
//			</div>
//		</div>
// </drop-down>

// drop-down .outer {
//		overflow: hidden;
//		height: 0;
// }

// См декоратор для опций.
const defaultOptions = {};

const decorators = [dropDownDecorator];

let elementProto = Object.create(HTMLElement.prototype);

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
	}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		decorators.forEach((d) => {
			d.attach(this, this._options);
		});
	}

	disconnectedCallback() {
		decorators.forEach((d) => {
			d.detach(this);
		});
	}
}

customElements.define("drop-down", ElementClass);

export default ElementClass;