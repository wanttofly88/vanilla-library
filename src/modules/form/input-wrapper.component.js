import dispatcher from '../dispatcher.js';
import { datasetToOptions } from "../utils/component.utils.js";

import inputWrapperDecorator from "./inputs/input-wrapper.decorator.js";
import fileWrapperDecorator from "./inputs/file-wrapper.decorator.js";

const defaultOptions = {
	type: "input",
	validate: "submit", // "submit", "input", "blur", "none"  - проверка на ошибку при сабмите формы, любом вводе, снятие фокуса с инпута или никогда.
	displayError: "first_input", // "first_input" - 1я ошибка для 1-го инпута, "first_error" - 1я ошибка для каждого инпута, "all - все"
}

// для инпута см ./decorators/input-wrapper.decorator.js
// для файла см ./decorators/file-wrapper.decorator.js

class ElementClass extends HTMLLabelElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._options = datasetToOptions(this.dataset, defaultOptions);
	}

	connectedCallback() {
		if (this._options.type === "input") {
			inputWrapperDecorator.attach(this, {
				validate: this._options.validate,
				displayError: this._options.displayError
			});
		} else if (this._options.type === "file") {
			fileWrapperDecorator.attach(this, {
				validate: this._options.validate,
				displayError: this._options.displayError
			});
		}
	}

	disconnectedCallback() {
		if (this._options.type === "input") {
			inputWrapperDecorator.detach(this);
		} else if (this._options.type === "file") {
			fileWrapperDecorator.detach(this);
		}
	}
}

customElements.define('input-wrapper', ElementClass, {extends: 'label'});

export default ElementClass;