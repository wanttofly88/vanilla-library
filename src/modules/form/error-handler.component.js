import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";

const decorators = [];

const defaultOptions = {
    __namespace: "",
    name: "", // Not required. Name of the input to show error from.
    errorType: "", // Not required. Error type. For all types check out error-handlers folder, error types are defined in default options of each handler.
    classVisible: "visible", // Class that will be added on activaton
    formId: "", // Not required. Can take id from the parent form.
};

//    примеры
//    выведет ошибку типа "required" для поля "name"
//    <error-handler class="form-handler" data-error-type="required" data-name="name">
//        <span>Укажите ваше имя</span>
//    </error-handler>

//    выведет все ошибки (первую, если стоит дефолтная настройка у input-wrapper-а) для поля "name"
//    <error-handler class="form-handler" data-name="name">
//        <span>Неверно заполнено поле "имя"</span>
//    </error-handler>

//    выведет все ошибки (первую, если стоит дефолтная настройка у input-wrapper-а) для ошибки типа "required"
//    <error-handler class="form-handler" data-error-type="required">
//        <span>Неверно обязательное поле</span>
//    </error-handler>

//    выведет ошибку для группы полей, заданной через data-required-group
//    <error-handler class="form-handler" data-error-type="required-group" data-name="contacts">
//        <span>Укажите ваш телефон или e-mail, чтобы мы могли связаться с вами</span>
//    </error-handler>

//    выведет все ошибки (первую, если стоит дефолтная настройка у input-wrapper-а) при превышении размера фала
//    <error-handler class="form-handler"  data-error-type="file-size">
//        <span>Можно загрузить файл размером до 10 Мб, Большой файл можно отправить нам на почту hello@redcollar.ru</span>
//    </error-handler>

//    выведет ошибку при неверном заполнение email-a в поле "email"
//    <error-handler class="form-handler" data-error-type="email" data-name="email">
//        <span>Укажите корректный email адрес</span>
//    </error-handler>

//    выведет ошибки, полученные с back-end-а
//    <error-handler class="form-handler" data-error-type="backend">
//        <span></span>
//    </error-handler>

class ElementClass extends HTMLElement {
    constructor(self) {
        self = super(self);
        self.init.call(self);
    }

    init() {
        this.handleDispatcher = this.handleDispatcher.bind(this);

        this._visible = false;
    }

    connectedCallback() {
        this._options = datasetToOptions(this.dataset, defaultOptions);
        this._form = this.closest("form");
        if (this._form) {
            this._formId = this._form.getAttribute("data-id");
        } else {
            this._formId = this._options.formId;
        }

        this._originalText = this.innerHTML;

        decorators.forEach((d) => d.attach(this));
        dispatcher.subscribe(this.handleDispatcher);
    }

    disconnectedCallback() {
        decorators.forEach((d) => d.detach(this));
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    show(e) {
        if (this._visible) return;
        this._visible = true;

        if (e.text) {
            let text = "";
            if (Array.isArray(e.text)) {
                this.innerHTML = e.text.reduce((pr, cur) => {
                    return (pr += `<span>${cur}</span>`);
                }, "");
            } else {
                this.innerHTML = e.text;
            }
        } else {
            if (this.innerHTML !== this._originalText) {
                this._originalText = this.innerHTML;
            }
        }

        this.classList.add(this._options.classVisible);
    }

    hide(e) {
        if (!this._visible) return;
        this._visible = false;
        this.classList.remove(this._options.classVisible);
    }

    handleDispatcher(e) {
        if (e.type === "error-handler:show") {
            if (this._formId) {
                if (e.formId !== this._formId) return;
            }

            if (e.errorType && this._options.errorType) {
                if (this._options.errorType === "backend") {
                    if (this._options.name === e.name || !e.name && !this._options.name) {
                        this.show(e);
                    }
                } else {
                    if (this._options.name && e.name) {
                        if (this._options.name === e.name) {
                            this.show(e);
                        }
                    } else {
                        this.show(e);
                    }
                }
            } else {
                if (this._options.name && e.name) {
                    if (this._options.name === e.name) {
                        this.show(e);
                    }
                } else {
                    this.show(e);
                }
            }
        }

        if (e.type === "error-handler:hide-all") {
            if (this._formId) {
                if (e.formId !== this._formId) return;
            }

            this.hide(e);
        }

        if (e.type === "error-handler:hide") {
            if (this._formId) {
                if (e.formId !== this._formId) return;
            }

            if (e.errorType && this._options.errorType) {
                if (this._options.errorType === "backend") {
                    if (this._options.name === e.name || !e.name && !this._options.name) {
                        this.hide(e);
                    }
                } else {
                    if (this._options.name && e.name) {
                        if (this._options.name === e.name) {
                            this.hide(e);
                        }
                    } else {
                        this.hide(e);
                    }
                }
            } else {
                if (this._options.name && e.name) {
                    if (this._options.name === e.name) {
                        this.hide(e);
                    }
                } else {
                    this.hide();
                }
            }
        }
    }
}

customElements.define("error-handler", ElementClass);

export default ElementClass;