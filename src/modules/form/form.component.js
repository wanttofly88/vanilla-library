import dispatcher from "../dispatcher.js";
import formStore from "./form.store.js";
import http from "../utils/HTTP.js";

import { datasetToOptions } from "../utils/component.utils.js";

import defaultAnimation from "./animations/default-form.decorator.js";
import grecaptchaDecorator from "./decorators/grecaptcha.decorator.js";

const animations = {
	default: defaultAnimation,
};

const defaultOptions = {
	id: "",
	animation: "default",
	captcha: "", // "grecaptcha" to use grecapthca decorator. Don't forget to add data-sitekey attribute for grecaptcha.
	error: "error-handler" //"error-handler" || "form-response" which component to use to show the backend error.
};

let idName = "form-";
let idNum = 0;

class ElementClass extends HTMLFormElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._status = null;
		this._valid = true;

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleStore = this.handleStore.bind(this);
		this.handleDispatcher = this.handleDispatcher.bind(this);
	}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);
		this._method = this.getAttribute("method");

		if (!this._method) {
			this._method = "post";
		}

		if (!this._options.id) {
			idNum++;
			this._options.id = idName + idNum;
			this.setAttribute("data-id", this._options.id);
		}

		if (this._options.captcha === "grecaptcha") {
			grecaptchaDecorator.attach(this);
		}

		animations[this._options.animation].attach(this);

		dispatcher.dispatch({
			type: "form:add",
			id: this._options.id,
		});

		this.handleStore();

		this.addEventListener("submit", this.handleSubmit);
		formStore.subscribe(this.handleStore);
		dispatcher.subscribe(this.handleDispatcher);
	}

	disconnectedCallback() {
		animations[this._options.animation].detach(this);
		if (this._options.captcha === "grecaptcha") {
			grecaptchaDecorator.detach(this);
		}

		dispatcher.dispatch({
			type: "form:remove",
			id: this._options.id,
		});

		this.removeEventListener("submit", this.handleSubmit);
		formStore.unsubscribe(this.handleStore);
		dispatcher.unsubscribe(this.handleDispatcher);
	}

	handleStore() {
		let storeData = formStore.getData().items[this._options.id];

		if (!storeData) return;
		if (storeData.status === this._status) return;

		this._status = storeData.status;
		this.classList.remove("waiting");
		this.classList.remove("sending");
		this.classList.remove("submitted");
		this.classList.add(this._status);

		if (this._status === "submitted") {
			this._decorators.animation.hide();
		} else if (this._status === "waiting") {
			this._decorators.animation.show();
		}
	}

	checkCaptcha() {
		return Promise.resolve("");
	}

	handleSubmit(e) {
		let data;
		let action = this.action;
		let invalidInput;

		if (!FormData) return;

		e.preventDefault();

		this._valid = true;

		if (this._status !== "waiting") {
			return;
		}

		dispatcher.dispatch({
			type: "form:validate",
			id: this._options.id,
		});

		if (!this._valid) {
			dispatcher.dispatch({
				type: "form:validation-failed",
				id: this._options.id,
			});

			invalidInput = this.querySelector(
				'label[is="input-wrapper"].error'
			);

			if (invalidInput) {
				invalidInput = invalidInput.querySelector("input, textarea");
				invalidInput.focus();
			}

			return;
		}

		this.checkCaptcha().then((token) => {
			data = new FormData(this);
			data.append("ajax", true);
			if (token) {
				data.append("captcha", token);
			}

			dispatcher.dispatch({
				type: "form:send",
				id: this._options.id,
			});

			http(action)
				[this._method](data)
				.then(
					(response) => {
						let json = JSON.parse(response);

						if (json.status === "success") {
							dispatcher.dispatch({
								type: "form:submit",
								id: this._options.id,
								response: json,
							});

							dispatcher.dispatch({
								type: "form:response",
								response: json,
								id: this._options.id,
							});

							this.classList.add("hidden");
						} else if (json.status === "error") {
							if (this._options.error === "form-response") {
								dispatcher.dispatch({
									type: "form:submit",
									id: this._options.id,
									response: json,
								});

								dispatcher.dispatch({
									type: "form:response",
									response: json,
									id: this._options.id,
								});

								this.classList.add("hidden");

								setTimeout(() => {
									this.classList.remove("hidden");
									dispatcher.dispatch({
										type: "form:reset",
										id: this._options.id,
									});
								}, 3000);
							} else if (this._options.error === "error-handler") {
								dispatcher.dispatch({
									type: "form:soft-reset",
									id: this._options.id,
								});

								if (json.errors && typeof json.errors === "object") {
									Object.keys(json.errors).forEach((key) => {
										dispatcher.dispatch({
											type: "error-handler:show",
											text: json.errors[key],
											name: key,
											formId: this._options.id,
											errorType: "backend"
										});
									});
								}

								if (json.response) {
									dispatcher.dispatch({
										type: "error-handler:show",
										text: json.response,
										formId: this._options.id,
										errorType: "backend"
									});
								}
							}
						}
					},
					(reject) => {
						// reject silently
					}
				);
		});
	}

	handleDispatcher(e) {
		if (e.type === "form:set-valid" && e.id === this._options.id) {
			this._valid = true;
		}

		if (e.type === "form:set-invalid" && e.id === this._options.id) {
			this._valid = false;
		}
	}
}

customElements.define("form-component", ElementClass, { extends: "form" });

export default ElementClass;