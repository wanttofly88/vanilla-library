import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import defaultAnimation from "./animations/default-response.decorator.js";

// ответ формы. пример стилей и разметка в соответствующих файлах

const animations = {
	default: defaultAnimation,
};

const defaultOptions = {
	animation: "default"
};

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this.handleForm = this.handleForm.bind(this);
	}

	connectedCallback() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		this._id = this.getAttribute("data-id");
		this._inner = this.getElementsByClassName("response-inner");
		this._inner = Array.prototype.slice.call(this._inner);

		if (!this._inner) {
			this._inner = this;
		}

		if (!this._id) {
			console.warn("data-id attribute is missing on form-response");
			return;
		}
		dispatcher.subscribe(this.handleForm);

		animations[this._options.animation].attach(this);
	}

	disconnectedCallback() {
		dispatcher.unsubscribe(this.handleForm);

		animations[this._options.animation].detach(this);
	}

	handleForm(e) {
		if (e.type === "form:response") {
			if (e.id !== this._id) return;

			if (
				e.response.hasOwnProperty("response")
			) {
				if (e.response.status === "success") {
					this.classList.remove("status-error");
					this.classList.add("status-success");
					this.classList.add("active");
				} else if (e.response.status === "error") {
					this.classList.add("status-error");
					this.classList.remove("status-success");
					this.classList.add("active");
				}

				if (Array.isArray(e.response.response)) {
					e.response.response.forEach((tx, index) => {
						if (this._inner[index])
							this._inner[index].innerHTML = tx;
					});
				} else {
					this._inner[0].innerHTML = e.response.response;
				}

				this._decorators.animation.show();
			} else {
				this._inner.forEach((el) => (el.innerHTML = ""));
				this.classList.remove("active");

				this._decorators.animation.hide();
			}
		}

		if (e.type === "form:reset") {
			if (e.id !== this._id) return;
			this._inner.forEach((el) => (el.innerHTML = ""));
			this.classList.remove("active");

			this._decorators.animation.hide();
		}
	}
}

customElements.define("form-response", ElementClass);

export default ElementClass;