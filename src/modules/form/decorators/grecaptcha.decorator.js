import { attach, detach, update } from "../../utils/decorator.utils.js";
import { datasetToOptions } from "../../utils/component.utils.js";

const name = "racaptcha-decorator";

const defaultOptions = {
    name: name,
    sitekey: ""
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;
        
        this.callback = this.callback.bind(this);
        this.checkCaptcha = this.checkCaptcha.bind(this);
        this.handleDispatcher = this.handleDispatcher.bind(this); 
    }

    init() {
        this._options = datasetToOptions(this._parent.dataset, this._options);
        let sitekey = this._options.sitekey;

        if (!window._onloadGrecaptcha) {
            let placeholder = document.createElement("div");
            placeholder.id = "recaptcha";
            placeholder.style.display = "none";
    
            window._onloadGrecaptcha = function () {
                window._grecaptchaId = grecaptcha.render("recaptcha", {
                    sitekey: sitekey,
                    theme: "dark",
                    size: "invisible",
                    callback: "_recaptchaCallback",
                    badge: "bottomright",
                });
            };

            let tag = document.createElement("script");
            tag.src = "https://www.google.com/recaptcha/api.js?onload=_onloadGrecaptcha&amp;render=explicit";
            tag.async = true;
    
            window._recaptchaCallback = function (token) {
                window._recaptchaCallbacks[window._recaptchaActiveForm](token);
            };
    
            document.body.appendChild(placeholder);
            window._recaptchaCallbacks = {};

            let firstScriptTag = document.getElementsByTagName("script")[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        }

        window._recaptchaCallbacks[this._parent._id] = this.callback;
        this._parent.checkCaptcha = this.checkCaptcha;
    }

    destroy() {
        delete window._recaptchaCallbacks[this._parent._id];
    }

    callback(token) {
        if (this.resolve) {
            this.resolve(token);
        }
    }

    checkCaptcha() {
        window._recaptchaActiveForm = this._parent._id;
        grecaptcha.execute(window._grecaptchaId);
        return new Promise((resolve, reject) => {
            this.resolve = resolve;
        });
    }

    handleDispatcher(e) {
        if (e.type === "form:reset") {
            grecaptcha.reset(window._grecaptchaId);
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};
