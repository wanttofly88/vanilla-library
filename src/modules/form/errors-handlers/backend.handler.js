const defaultOptions = {
	errorType: "backend",
};

class RequiredHandler {
	constructor(options) {
		this._options = Object.assign({}, defaultOptions, options);
	}

	handle(input) {
		return null;
	}
}

let handler = new RequiredHandler();

export default handler;