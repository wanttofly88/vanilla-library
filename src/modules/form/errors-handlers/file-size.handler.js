const defaultOptions = {
    errorType: "file-size",
};

class FileSizeHandler {
    constructor(options) {
        this._options = Object.assign({}, defaultOptions, options);
    }

    handle(input) {
        if (input.type !== "file") return null;

        let maxSize = input.getAttribute('data-max-size');
        if (maxSize === null) return null;
        maxSize = parseInt(maxSize);

        if (input.files.length === 0) return null;

        let size = Array.prototype.reduce.call(input.files, (total, file) => {
            return total + file.size;
        }, 0);

        if (size > maxSize) {
            return {
                name: input.name,
                type: this._options.errorType,
            };
        } else {
            return null;
        }
    }
}

let handler = new FileSizeHandler();

export default handler;