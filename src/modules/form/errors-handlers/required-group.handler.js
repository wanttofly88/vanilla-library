const defaultOptions = {
	errorType: "required-group",
};

class RequiredGroupHandler {
	constructor(options) {
		this._options = Object.assign({}, defaultOptions, options);
	}

	handle(input) {
		let group = input.getAttribute("data-required-group");
		if (group === null) return;

		let root = input.closest("form") || document;
		let groupInputs = Array.prototype.slice.call(
			root.querySelectorAll(`input[data-required-group="${group}"]`)
		);

		let found = false;
		groupInputs.forEach((input) => {
			let name = input.name;
			let value;

			if (input.type === "checkbox") {
				found = found || input.checked;
			} else {
				found = found || !!input.value;
			}
		});

		if (!found) {
			return {
				name: group,
				type: this._options.errorType,
			};
		} else {
			return null;
		}
	}
}

let handler = new RequiredGroupHandler();

export default handler;