const defaultOptions = {
	errorType: "required",
};

class RequiredHandler {
	constructor(options) {
		this._options = Object.assign({}, defaultOptions, options);
	}

	handle(input) {
		if (input.getAttribute("required") === null) return;
		let name = input.name;
		let value;

		if (input.type === "checkbox") {
			value === input.checked;
		} if (input.type === "file") {
			value === input.files.length;
		} else {
			value = input.value;
		}

		if (!value) {
			return {
				name: name,
				type: this._options.errorType,
			};
		} else {
			return null;
		}
	}
}

let handler = new RequiredHandler();

export default handler;