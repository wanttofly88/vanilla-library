# Компоненты формы
___

## form.store
___
В данный момент служит только для хранения состояний формы. Поддержка событий извне не поддерживается, для ручной отправки формы используйте нативный метод form.submit();
### Данные
```javaScript
    import formStore from './form.store.js';
    let data = formStore.getData();
```
**items**: **Object** - объект всех форм на сайте.  
```javaScript
    let data = data.items['form-id'];
```
**id**: **String** - id формы.  
**status**: **String** - состояние формы.  
-'waiting' - дефолтное состояние, форма в ожидании.  
-'sending' - форма отправлена, ожидает ответа.  
-'submitted' - форма отправлена, получила ответ.  

## form.component
___
Базовая компонента формы

Синтаксис:
```html
<form is="form-component" action="#" method="post" data-id="form-id">
</form>
```
**data-id** обязательный аттрибут. Уникальный id формы для связи с остальными компонентами.  
**action** url для отправки данных.  
**method** метод отправки данный (дефолтный - POST).  
**novalidate** - отключит нативную валидацию и включит кастомную. Плохо с точки зрения ux-а, но может по дизайну нужно.  

Форме автоматически проставляется класс **hidden** когда форме пришел ответ. 

### Пример формы со стилями и разметкой
```html
<div class="form-wrapper">
    <form-response class="response" data-id="form-id">
        <div class="response-inner"></div>
    </form-response>
    <form action="response.json" data-id="form-id" is="form-component">
        <label is="input-wrapper" class="input-wrapper">
            <input type="text" class="text" name="email" required>
        </label>
        <button class="button wide">отправить заявку</button>
    </form>
</div>
```
```css
.form-wrapper {
    position: relative;
}
.form-wrapper .response {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    pointer-events: none;
    opacity: 0;
    transform: translateY(50px);
    transition-property: opacity 0.6s, transform 0.6s;
    transition-duration: 0.6s, 0.6s;
    transition-timing-function: var(--ease-in), ease;
    z-index: 1;
}
.form-wrapper .response.active {
    opacity: 1;
    transform: translateY(0px);
    transition-delay: 0.6s, 0.6s;
    transition-timing-function: var(--ease-out), ease;
    pointer-events: auto;
    z-index: 2;
}
.form-wrapper form {
    transition-property: opacity, transform;
    transition-duration: 0.6s, 0.6s;
    transition-delay: 0.6s, 0.6s;
    transition-timing-function: var(--ease-out), ease;
}
.form-wrapper form.hidden {
    opacity: 0;
    transform: translateY(20px);
    transition-delay: 0s, 0s;
    transition-timing-function: var(--ease-in), ease;
    pointer-events: none;
    z-index: 0;
}
```
### Пример ответа сервера
```json
{"status":"success","response":"Спасибо! Наши менеджеры свяжутся с вами в ближайшее время."}
```

### Пример ошибки сервера
Если подключена кастомная обработка ошибок, выведет так же их  
```json
{"status":"error","response":"Произошла ошибка. Попробуйте позже"}
```
Или для нескольких ошибок  
```json
{"status":"error","response": ["Поле 'имя' обязательно", "Поле 'email' обязательно"]}
```
Ошибки с указанием имен полей. Можно передавать как строку, так и массив
```json
{"status":"error","response": "Произошла ошибка. Попробуйте позже", "errors":{"name": "Поле обязательно к заполнено", "email": ["Поле обязательно к заполнено", "Неверно заполнено поле"]}}
```

**TODO**: Реализовать подсветку поля, фокус на поле и скролл к нему  

**status**: **String** - статус ответа (success || error)  
'success' - компоненте ответа будет присовон класс 'status-success' и 'active'. Форме - 'hidden'.  
'error' - Если для обработки ошибок используется компонент <form-response> (задается в настройках формы), компоненте ответа будет присовон класс 'status-error' и 'active'. Форме - 'hidden' на **3 секунды**, затем форма будет сброшена обратно. Иначе будут использованы кастомные обработчики ошибок <error-handler>  
**response**: **String** || **Array** - текст общей ошибки, не привязанной к конкретному полю.  
**errors**: **Object** - объект с ошибками конкретных полей.  
'key' - имя поля, в которм произошла ошибка.  
'value': **String** || **Array** - текст ошибки.  

## form-response.component
___
Компонента для вывода ответа формы

Синтаксис:
```html
<form-response class="response" data-id="form-id">
    <div class="response-inner"></div>
</form-response>
```
**data-id** обязательный аттрибут. Уникальный id формы для связи с остальными компонентами.  

Компоненте автоматически проставляется класс **visible** когда форме пришел ответ. 
Ответ сервера **(response)** будет записан в элемент **response-inner**.  

## input-wrapper.component
___
Обертки для инпутов

Синтаксис:
```html
<label is="input-wrapper">
    <input ....>
</label>
```

**data-type**: **String** - default **input**. Тип инпута.  
-'input' - текстовый input или textarea  
-'file' - input c type="file"    

**data-validate**: **String** - default **false**. способ валидации инпута.  
-'false' - Валидация произойдет при сабмите формы.  
-'blur' - Валидация произойдет при снятии фокуса с инпута. (рекомендуется)  
-'input' - Валидация произойдет при любом вводе данных в инпут.  

**data-display-error**: **String** - default **first_input**. режим вывода ошибок для кастомных ошибок.  
-'first_input' - Будет выведена 1-я встреченная ошибка для 1-го инпута с ошибкой.  
-'first_error' - Будет выведена 1-я встреченная ошибка для каждого инпута с ошибкой.  
-'all' - Будут выведены все ошибки.  

Для включения масок инпутов раскомментировать строку  
import IMask from '../../libs/IMask.js';  

Подключить маски в конце **connectedCallback**  
В коде присутствует закомментированный пример маски. 

### Файл
**TODO**: Добавить возможность перетаскивать несколько файлов  
**WARNING**: Должно работать с несколькими файлами, если добавлять их кликая на них, но оттестировано плохо. Требуется фидбэк  

Синтаксис:
```html
<label is="input-wrapper" data-type="file" >
    <input type="file" name="file">
    <span class="file-name">Прикрепить файл</span>
    <button type="button" class="remove" aria-label="remove file"></button>
</label>
```
**data-display-size**: **Boolean** - default **true**. выводить размер файла  
**data-class-file-name**: **String** - default **file-name**. класс элемента для вывлда информации по файлу. При удалении файла текст вернется к тому, который был изначально (в примере - 'Прикрепить файл')  
**data-class-remove**: **String** - default **remove**. класс кнопки удаления файла  
**data-class-drag**: **String** - default **drag-over**. ставит такой класс при перетягивание файла на инпут  
**data-class-not-empty**: **String** - default **not-empty**. ставит такой класс, когда файл добавлен  


### Классы, автоматически добавляемые input-wrapper-ом  

Добавляет класс **error**, если инпут заполнен неверно  
Добавляет класс **not-empty**, если инпут чем-то заполнен  
Добавляет класс **focus**, если курсор установлен в инпут  

### Кастомная обработка ошибок 
Подключить error-handler.component.js и добавить их в html  
Поставить фомре **novalidate**  
Элементу **input** указать что-то из следующего:  
-'required' - поле обязательно для заполнения  
-'data-required-group="group-name"' - хотя бы одно из полей с одинаковым "group-name" обязательно для заполнения  
-'data-max-size="10485760"' - для файлов. Максимальный размер файла в байтах
  
### Примеры:  
```html
<label is="input-wrapper" class="input-wrapper">
    <input class="text-input" required type="text" name="name">
</label>

<label is="input-wrapper" class="input-wrapper">
    <input class="text-input" data-required-group="contacts" type="text" name="phone">
</label>

<label is="input-wrapper" class="input-wrapper">
    <input class="text-input" data-required-group="contacts" type="text" name="email">
</label>

<label is="input-wrapper" data-type="file" class="file-wrapper">
    <input type="file" data-max-size="10485760" name="file">
    <span class="file-nfo">
        <span class="file-name">Прикрепить файл</span>
        <button type="button" class="remove" aria-label="remove file"></button>
    </span>
</label>

<label is="input-wrapper" class="input-wrapper">
    <textarea class="text-input" required type="text" name="name">
</label>
```

## error-handler.component
___
Компонента для вывода кастомных ошибок  
Для корректной работы форме требуется добавить аттрибут **novalidate**  

Синтаксис:
```html
<error-handler>
    <span>Текст ошибки</span>
</error-handler>
```
**data-form-id**: **String** - default **""**. Id формы. Если не добавлять, будет искать форму среди своих родителей  

**data-error-type**: **String** - default **""**. Тип ошибки. Для конкретных типов можно посмотреть папку **error-handlers**  
-'required' - поле обязательно для заполнения
-'required-group' - одно из полей группы обязательно для заполнения. В качестве data-name указать имя группы  
-'email-regex' - поле не соответствует regex-у для проверки на корректный email  
-'file-size' - поле с файлом привысило допустимый размер файла  
-'backend' - вывод ошибок с back-end-а  

**data-name**: **String** - default **""**. Имя поля, для которого будет выведена эта ошибка  

**data-class-visible**: **String** - default **visible**. Класс, который будет добавлен при активации ошибки  

### Примеры:  

```html
<!-- выведет ошибку типа "required" для поля "name"-->
<error-handler class="form-handler" data-error-type="required" data-name="name">
    <span>Укажите ваше имя</span>
</error-handler>

<!-- выведет все ошибки (первую, если стоит дефолтная настройка у input-wrapper-а) для поля "name"-->
<error-handler class="form-handler" data-name="name">
    <span>Неверно заполнено поле "имя"</span>
</error-handler>

<!-- выведет все ошибки (первую, если стоит дефолтная настройка у input-wrapper-а) для ошибки типа "required"-->
<error-handler class="form-handler" data-error-type="required">
    <span>Неверно обязательное поле</span>
</error-handler>

<!-- выведет ошибку для группы полей, заданной через data-required-group-->
<error-handler class="form-handler" data-error-type="required-group" data-name="contacts">
    <span>Укажите ваш телефон или e-mail, чтобы мы могли связаться с вами</span>
</error-handler>

<!-- выведет все ошибки (первую, если стоит дефолтная настройка у input-wrapper-а) при превышении размера фала-->
<error-handler class="form-handler"  data-error-type="file-size">
    <span>Можно загрузить файл размером до 10 Мб, Большой файл можно отправить нам на почту hello@redcollar.ru</span>
</error-handler>

<!-- выведет ошибку при неверном заполнение email-a в поле "email"-->
<error-handler class="form-handler" data-error-type="email" data-name="email">
    <span>Укажите корректный email адрес</span>
</error-handler>

<!-- выведет ошибки, полученные с back-end-а-->
<error-handler class="form-handler" data-error-type="backend">
    <span></span>
</error-handler>
```
