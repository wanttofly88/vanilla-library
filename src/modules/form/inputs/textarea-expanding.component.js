var _handleInput = function () {
	var rows;
	var extra;
	this._rows--;
	if (this._rows < 2) {
		this._rows = 1;
	}
	this.setAttribute("rows", this._rows);

	while (this.clientHeight < this.scrollHeight) {
		rows = parseInt(this.getAttribute("rows"));
		this._rows++;
		this.setAttribute("rows", this._rows);
	}
};

class ElementClass extends HTMLTextAreaElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this.handleInput = _handleInput.bind(this);
	}
	connectedCallback() {
		this.style.overflow = "hidden";
		this._rows = parseInt(this.getAttribute("rows"));
		this._lineHeight = parseInt(getComputedStyle(this).lineHeight);

		this.addEventListener("keyup", this.handleInput);
		this.addEventListener("input", this.handleInput);
	}
	disconnectedCallback() {}
}

customElements.define("textarea-expanding", ElementClass, {
	extends: "textarea",
});

export default ElementClass;