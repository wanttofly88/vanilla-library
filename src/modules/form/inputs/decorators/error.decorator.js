import { attach, detach, update } from "../../../utils/decorator.utils.js";
import dispatcher from "../../../dispatcher.js";
import requiredHandler from "../../errors-handlers/required.handler.js";
import requiredGroupHandler from "../../errors-handlers/required-group.handler.js";
import emailRegexHandler from "../../errors-handlers/email-regex.handler.js";
import fileSizeHandler from "../../errors-handlers/file-size.handler.js";

const name = "error"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
};

const errorHandlers = [
    requiredHandler,
    requiredGroupHandler,
    emailRegexHandler,
    fileSizeHandler
];

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.handleDispatcher = this.handleDispatcher.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    init() {
        this._form = this._parent.closest("form");
        if (this._form) {
            this._formId = this._form.getAttribute("data-id");
        } else {
            this._formId = null;
        }

        this._input = this._parent.getElementsByTagName("input")[0];
        if (!this._input) {
            this._input = this._parent.getElementsByTagName("textarea")[0];
        }
        if (!this._input) {
            this._input = this._parent.parentNode.getElementsByTagName(
                "input"
            )[0];
        }
        if (!this._input) {
            // just give up already
            return;
        }

        if (this._input.type === "checkbox") {
            this._input.addEventListener("change", this.handleChange);
        } if (this._input.type === "file") {
            this._input.addEventListener("change", this.handleChange);
        } else {
            this._input.addEventListener("input", this.handleInput);
            this._input.addEventListener("focus", this.handleFocus);
            this._input.addEventListener("blur", this.handleBlur);
        }

        dispatcher.subscribe(this.handleDispatcher);
    }

    destroy() {
        dispatcher.unsubscribe(this.handleDispatcher);
    }

    validate(options) {
        options = Object.assign(
            {
                submitted: false,
            },
            options
        );

        if (this._options.validate === "none") return;

        let errorFound = null;
        errorHandlers.forEach((handler) => {
            if (
                this._options.displayError === "first_input" &&
                this._errorFoundOnDifferentInput
            ) {
                return;
            }

            if (
                (this._options.displayError === "first_error" ||
                    this._options.displayError === "first_input") &&
                errorFound
            ) {
                return;
            }

            let error = handler.handle(this._input);
            if (error) {
                errorFound = error;

                if (this._options.displayError === "all") {
                    this.showError(error);
                }
            }
        });

        if (errorFound) {
            if (
                this._options.displayError === "first_error" ||
                this._options.displayError === "first_input"
            ) {
                this.showError(errorFound);
            }

            if (options.submitted) {
                dispatcher.dispatch({
                    type: "form:set-invalid",
                    id: this._formId,
                });
            }
        } else {
            this.removeError();
        }
    }

    showError(error) {
        this._parent.classList.add("error");
        this._input.setAttribute("aria-invalid", true);

        if (
            this._options.displayError === "first_input"
        ) {
            dispatcher.dispatch({
                type: "error-handler:hide",
                formId: this._formId
            });
        }

        dispatcher.dispatch({
            type: "error-handler:show",
            name: error.name,
            errorType: error.type,
            formId: this._formId
        });
    }

    removeError() {
        dispatcher.dispatch({
            type: "error-handler:hide",
            name: this._input.name,
            formId: this._formId
        });

        this._parent.classList.remove("error");
        this._input.removeAttribute("aria-invalid");
    }

    handleInput() {
        if (this._options.validate === "input") {
            this._errorFoundOnDifferentInput = false;
            this.validate();
        } else {
            this._parent.classList.remove("error");
            this._input.removeAttribute("aria-invalid");
        }
    }

    handleChange() {
        if (this._options.validate !== "input") {
            this._parent.classList.remove("error");
            this._errorFoundOnDifferentInput = false;
        }
    }

    handleFocus() {}

    handleBlur() {
        if (
            this._options.validate === "blur" ||
            this._options.validate === "input"
        ) {
            this._errorFoundOnDifferentInput = false;
            this.validate();
        }
    }

    handleDispatcher(e) {
        if (e.type === "form:validate") {
            if (e.id !== this._formId) return;
            dispatcher.dispatch({
                type: "error-handler:hide",
                name: this._input.name,
                formId: this._formId
            });

            this.validate({
                submitted: true,
            });
        }

        if (e.type === "form:validation-failed") {
            if (e.id !== this._formId) return;
            this._errorFoundOnDifferentInput = false;
        }

        if (e.type === "form:set-invalid") {
            if (e.id !== this._formId) return;
            this._errorFoundOnDifferentInput = true;
        }

        if (e.type === "form:reset") {
            this._errorFoundOnDifferentInput = false;
            if (this._input.type === "checkbox") {
                this._input.checked = false;
            } else {
                this._input.value = "";
            }
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, Object.assign({}, defaultOptions, options));
    },
};
