import { attach, detach, update } from "../../utils/decorator.utils.js";
import dispatcher from "../../dispatcher.js";
import { datasetToOptions } from "../../utils/component.utils.js";
// import IMask from '../../libs/IMask.js';
import errorDecorator from "./decorators/error.decorator.js";

const name = "input-wrapper"; // указать дефолтное имя декоратора

if (name === null) {
	console.error("decorator name is missing");
}

// <label is="input-wrapper" class="input-wrapper">
//     <input class="text-input" required type="text" name="name">
// </label>

// <label is="input-wrapper" class="input-wrapper">
//     <input class="text-input" data-required-group="contacts" type="text" name="phone">
// </label>

// <label is="input-wrapper" class="input-wrapper">
//     <input class="text-input" data-required-group="contacts" type="text" name="email">
// </label>

// <label is="input-wrapper" class="input-wrapper">
//     <textarea class="text-input" required type="text" name="name">
// </label>

const defaultOptions = {
	id: "",
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = options;

		this.handleInput = this.handleInput.bind(this);
		this.handleFocus = this.handleFocus.bind(this);
		this.handleBlur = this.handleBlur.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	init() {
		this._options = datasetToOptions(this._parent.dataset, this._options);

		this._input = this._parent.getElementsByTagName("input")[0];
		if (!this._input) {
			this._input = this._parent.getElementsByTagName("textarea")[0];
		}
		if (!this._input) {
			this._input = this._parent.parentNode.getElementsByTagName(
				"input"
			)[0];
		}
		if (!this._input) {
			// just give up already
			return;
		}

		errorDecorator.attach(this._parent, {
			validate: this._options.validate,
			displayError: this._options.displayError
		});

		if (this._input.type === "checkbox") {
			this._input.addEventListener("change", this.handleChange);
		} else {
			this._input.addEventListener("input", this.handleInput);
			this._input.addEventListener("focus", this.handleFocus);
			this._input.addEventListener("blur", this.handleBlur);
		}

		this.handleInput();

		// if (this._input.type === 'tel') {
		// 	let mask = IMask(this._input, {
		// 		mask: '+{1} (000) 000-0000'
		// 	});
		// }
	}

	destroy() {
		errorDecorator.detach(this._parent);
	}

	handleInput() {
		if (this._input.value !== "") {
			this._parent.classList.add("not-empty");
		} else {
			this._parent.classList.remove("not-empty");
		}
	}

	handleChange() {}

	handleFocus() {
		this._parent.classList.add("focus");
	}

	handleBlur() {
		this._parent.classList.remove("focus");
	}
}

export default {
	attach: (parent, options) => {
		return attach(
			Decorator,
			parent,
			Object.assign({}, defaultOptions, options)
		);
	},
	detach: (parent, options) => {
		return detach(parent, Object.assign({}, defaultOptions, options));
	},
	update: (parent, options) => {
		return update(parent, Object.assign({}, defaultOptions, options));
	},
};