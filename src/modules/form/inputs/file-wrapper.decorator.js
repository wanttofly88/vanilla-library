import { attach, detach, update } from "../../utils/decorator.utils.js";
import { datasetToOptions } from "../../utils/component.utils.js";
import errorDecorator from "./decorators/error.decorator.js";

const name = "file-wrapper"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

// data-max-size - для ограничения размера файлов. В байтах.
// сейчас отсутствует нативное сообщение об ошибке для этого. Используйте error-handler.component.js

// <label is="input-wrapper" data-type="file" class="file-wrapper">
//     <input type="file" data-max-size="10485760" name="file">
//     <span class="file-nfo">
//         <span class="file-name">Прикрепить файл</span>
//         <button type="button" class="remove" aria-label="remove file"></button>
//     </span>
// </label>

// Не работает добавленик нескольких файлов перетаскиванием

const defaultOptions = {
    name: name,
    displaySize: true, // отображать размер файла или нет
    classFileName: "file-name", // класс элемента для вывлда информации по файлу
    classRemove: "remove", // класс кнопки удаления файла
    classDrag: "drag-over", // ставит такой класс при перетягивание файла на инпут
    classNotEmpty: "not-empty", // ставит такой класс, когда файл добавлен
};

function formatFileSize(bytes) {
    let i = -1;
    let byteUnits;
    let lang = document.documentElement.getAttribute("lang") || "en";

    if (lang === "ru") {
        byteUnits = ["Кб", "Мб", "Гб", "Тб", "Пб"];
    } else {
        byteUnits = ["KB", "MB", "GB", "TB", "PB"];
    }

    do {
        bytes = bytes / 1024;
        i++;
    } while (bytes > 1024 && i < byteUnits.length);

    return Math.max(bytes, 0.1).toFixed(1) + byteUnits[i];
}

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;

        this.handleDrop = this.handleDrop.bind(this);
        this.handleOver = this.handleOver.bind(this);
        this.handleOut = this.handleOut.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    init() {
        this._options = datasetToOptions(this._parent.dataset, this._options);

        this._fileName = this._parent.getElementsByClassName(
            this._options.classFileName
        )[0];
        this._originalFileName = this._fileName.innerHTML;

        this._input = this._parent.getElementsByTagName("input")[0];
        this._removeElement = this._parent.getElementsByClassName(
            this._options.classRemove
        )[0];

        errorDecorator.attach(this._parent, {
            validate: this._options.validate,
            displayError: this._options.displayError
        });

        this._input.addEventListener("change", this.handleChange);

        this._parent.addEventListener("drop", this.handleDrop);
        this._parent.addEventListener("dragover", this.handleOver);
        this._parent.addEventListener("mouseleave", this.handleOut);
        this._removeElement.addEventListener("click", this.handleRemove);
    }

    destroy() {
        errorDecorator.detach(this._parent);
    }

    handleDrop(e) {
        let files = [];
        e.preventDefault();

        if (!e.dataTransfer) return;

        // if (e.dataTransfer.items) {
        //  for (let i = 0; i < e.dataTransfer.items.length; i++) {
        //      if (e.dataTransfer.items[i].kind === 'file') {
        //          files.push(e.dataTransfer.items[i].getAsFile());
        //      }
        //  }
        // } else {
        //  files = e.dataTransfer.files;
        // }

        this._input.files = e.dataTransfer.files;

        if (e.dataTransfer.items) {
            e.dataTransfer.items.clear();
        } else {
            e.dataTransfer.clear();
        }

        this._parent.classList.remove(this._options.classDrag);
        this._input.focus();

        this.handleChange();
    }

    handleOver(e) {
        e.preventDefault();
        if (!this._parent.classList.contains(this._options.classDrag)) {
            this._parent.classList.add(this._options.classDrag);
        }
    }

    handleOut() {
        this._parent.classList.remove(this._options.classDrag);
    }

    handleChange() {
        let files = this._input.files;

        if (files && files.length) {
            this._parent.classList.add(this._options.classNotEmpty);
            let name = files[0].name;

            if (this._options.displaySize) {
                name += " " + formatFileSize(files[0].size);
            }

            this._fileName.innerHTML = name;
        } else {
            this._parent.classList.remove(this._options.classNotEmpty);
            this._fileName.innerHTML = this._originalFileName;
        }
    }

    handleRemove(e) {
        let event = new Event("change");
        e.stopPropagation();
        e.preventDefault();

        this._input.type = "";
        this._input.type = "file";
        this._input.dispatchEvent(event);
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, Object.assign({}, defaultOptions, options));
    },
};