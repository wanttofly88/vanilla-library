import dispatcher from '../../dispatcher.js';
import {attach, detach} from '../../utils/decorator.utils.js';

let name = 'animation';

const defaultOptions = {
	name: name
}

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({
		}, defaultOptions, options);

		this.show = this.show.bind(this);
		this.hide = this.hide.bind(this);
	}

	init() {
	}

	destroy() {
	}

	show() {
		this._parent.classList.remove('hidden');
	}

	hide() {
		this._parent.classList.add('hidden');
	}
}

export default {
    attach: (parent, options) => {
        attach(Decorator, parent, Object.assign({}, defaultOptions, options));
    },
    detach: (parent, options) => {
        detach(parent, Object.assign({}, defaultOptions, options));
    },
};