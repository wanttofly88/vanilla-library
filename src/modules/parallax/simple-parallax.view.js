import dispatcher from '../dispatcher.js';
import scrollStore from '../scroll/scroll.store.js';
import {offset} from '../utils/dom.utils.js';
import {datasetToOptions} from '../utils/component.utils.js';
import resolveProperty from '../utils/resolve-property/resolve-property.js'
import resizeStore from '../resize/resize.store.js';

const defaultOptions = {
	from: 100,
	to: -100,
	__namespace: 'parallax'
}

let elements;

let _handleScroll = function(options) {
	options = options || {};

	let wh = resizeStore.getData().height;
	let scrolled = scrollStore.getData().top;
	
	let checkElement = function(element) {
		let from = element._parallaxOptions.from;
		let to = element._parallaxOptions.to;

		let tp = element._parallaxOptions.offset - wh;
		let bt = element._parallaxOptions.offset + element._parallaxOptions.height;

		let c = (to - from) / (bt - tp);
		let p = c * (scrolled - (tp + (bt - tp) / 2));
		
		if (scrolled > tp + element._parallaxOptions.from && scrolled < bt - element._parallaxOptions.to) {
			element._resolveProperty.set({
				property: 'y',
				namespace: element._parallaxOptions.__namespace,
				value: p
			});
		}

		if (options.forced) { // initial
			if (scrolled < tp + element._parallaxOptions.from && scrolled) {
				element._resolveProperty.set({
					property: 'y',
					namespace: element._parallaxOptions.__namespace,
					value: element._parallaxOptions.from
				});
			} else if (scrolled <= bt - element._parallaxOptions.to) {
				element._resolveProperty.set({
					property: 'y',
					namespace: element._parallaxOptions.__namespace,
					value: p
				});
			} else {
				element._resolveProperty.set({
					property: 'y',
					namespace: element._parallaxOptions.__namespace,
					value: element._parallaxOptions.to
				});
			}
		}
	}

	Array.prototype.forEach.call(elements, function(element) {
		checkElement(element, false);
	});
}

let _handleResize = function() {
	Array.prototype.forEach.call(elements, function(element) {
		element._parallaxOptions.offset = offset(element).top;
		element._parallaxOptions.height = element.clientHeight;
		console.log(element._parallaxOptions.offset);
		console.log(element.clientHeight)
	});
}

let _handleDispatcher = function(e) {
	if (e.type === 'content:replaced') {
		_getElements();
		_handleResize();
		_handleScroll({forced: true});
	}
}

let _getElements = function() {
	elements = document.getElementsByClassName('simple-parallax');

	Array.prototype.forEach.call(elements, function(element) {
		element._parallaxOptions = datasetToOptions(element.dataset, defaultOptions);

		resolveProperty.attach(element, {
			property: 'y',
			namespace: element._parallaxOptions.__namespace,
			setStyles: true
		});
	});
}


let init = function() {
	_getElements();
	_handleResize();
	_handleScroll({forced: true});
	resizeStore.subscribe(_handleResize);
	resizeStore.subscribe(_handleScroll);
	scrollStore.subscribe(_handleScroll);
	dispatcher.subscribe(_handleDispatcher);
}

export default {
	init: init
}