import dispatcher from '../dispatcher.js';
import * as THREE from '/node_modules/three/build/three.module.js';
import resizeStore from '../resize/resize.store.js';
import simpleVertexShader from './glsl/simple.vertex.js';
import simpleFragmentShader from './glsl/simple.fragment.js';

import addMesh from './utils/mesh.utils/addMeshOBJ.js';
import {FunctionArray} from '../utils/array.utils.js';

let loadingManager = new THREE.LoadingManager(function() {});
let texloader = new THREE.TextureLoader(loadingManager);

// mesh data: {
// 	srcs: [{{source name}}], // can pass multiple textures for whatever reson. frame animations for example.
//	options: {
//		position: [0, 0, 0],
//		size: [1440, 900],
//		type: 'cover' || 'contain'
//	}
//}

// possible hooks:
// onload
// onunload
// onloop
// onshow
// onhide
// onresize
// onrebuild

// props:
// _screenMode: 'mobile' || 'desktop' - if width is greater then height
// _touch: true || false - if touch screen

const defaultOptions = {
	meshData: [],
	uniforms: {},
	vertexShader: simpleVertexShader,
	fragmentShader: simpleFragmentShader
}

class MeshDecorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);
		this._meshes = [];
		this._touch = this._parent._touch;
		this._loopArray = new FunctionArray();
		this._resizeArray = new FunctionArray();
		this._activePromises = [];

		this.resize = this.resize.bind(this);
		this.loop = this.loop.bind(this);
	}

	// hooks
	oninit() {};
	onload() {};
	onloop() {};
	onunload() {};
	onresize() {};
	onshow() {};
	onhide() {};

	init() {
		let self = this;

		this._group = new THREE.Group();
		this._group.position.set(0, 0, 0);
		this._group.rotation.set(0, 0, 0);

		this._parent._showArray.push(this.show);
		this._parent._hideArray.push(this.hide);

		this._parent._scene.add(this._group);

		this.oninit();
	}

	destroy() {
		this._parent._showArray.remove(this.show);
		this._parent._hideArray.remove(this.hide);

		this._parent._scene.remove(this._group);
	}
	
	updateUniforms(u) {
		this._meshes.forEach(function(m) {
			for (let key in u) {
				if (m.material.uniforms.hasOwnProperty(key)) {
					m.material.uniforms[key].value = u[key];
				}
			}
		});
	}

	getMeshByName(name) {
		return this._meshes.find(function(m) {
			return m.name === name
		});
	}

	resize(mesh) {
		let size = this._parent._renderer.getSize(new THREE.Vector2());
		let screenSize = resizeStore.getData();

		function resizeMesh(m) {
			m.material.uniforms.screenResolution.value = [size.width, size.height];
		}

		if (mesh) {
			resizeMesh(mesh);
		} else {
			this._meshes.forEach(resizeMesh);
		}

		if (this.onresize) {
			this.onresize();
		}
	}

	load() {
		let size = this._parent._renderer.getSize(new THREE.Vector2());
		let self = this;

		if (this._loaded) return;
		this._loaded = true;
		
		if (this._options.background) {
			this._background = new THREE.Mesh(
				new THREE.PlaneBufferGeometry(1, 1),
				new THREE.MeshBasicMaterial({color: this._options.background})
			);
		} else {
			this._background = null;
		}

		if (size.width > size.height) {
			this._screenMode = 'desktop';
		} else {
			this._screenMode = 'mobile';
		}
		
		let promises = [];
		let meshData;

		if (Array.isArray(this._options.meshData)) {
			meshData = this._options.meshData;
		} else {
			if (!this._options.meshData['mobile'] ||
				!this._options.meshData['desktop']) {
				console.warn('meshData has to include "mobile" and "desktop" arrays or to be an array');
				return;
			}
			if (size.width > size.height) {
				meshData = this._options.meshData['desktop'];
			} else {
				meshData = this._options.meshData['mobile'];
			}
		}
			
		meshData.forEach(function(m) {
			let src;
			let fragmentShader;
			let addMeshPromise;
			let uniforms = Object.assign({}, self._options.uniforms);
			uniforms = Object.assign(uniforms, m.uniforms);


			fragmentShader = m.fragmentShader || self._options.fragmentShader;
			if (self._touch) {
				fragmentShader = 
				'#define TOUCH 1\n' + fragmentShader;
			}

			addMeshPromise = addMesh(m.src, Object.assign({
				fragmentShader: fragmentShader,
				vertexShader: self._options.vertexShader,
				uniforms: uniforms
			}, m));

			self._activePromises.push(addMeshPromise);

			promises.push(addMeshPromise.then(function(data) {
				self._activePromises = self._activePromises.filter(p => p !== addMeshPromise);
				if (addMeshPromise._canceled) {
					return Promise.reject('Promise canceled');
				};

				if (data.group.type === 'Group') {
					data.group.traverse(function(m) {
						if (m.type === 'Mesh') {
							self._group.add(m);
							self._meshes.push(m);
						}
					});
				} else if (data.group.type === 'Mesh') {
					self._group.add(data.group);
					self._meshes.push(data.group);
				}

				console.dir(self._group);
				
				return Promise.resolve();
			}));
		});

		return new Promise(function(resolve, reject) {
			Promise.all(promises).then(function(results) {
				if (self._background) {
					self._group.add(self._background);
				}

				self.resize();
				self._parent._loopArray.push(self.loop);
				self._parent._resizeArray.push(self.resize);
				self.onload();

				dispatcher.dispatch({
					type: 'stage-group:ready'
				});
				resolve();
			}).catch(function(r) {
				reject(r);
			});
		});
	}

	unload() {
		let self = this;
		let group = this._group;

		if (!this._loaded) return;

		this._loaded = false;

		for (let i = group.children.length - 1; i >= 0; i--) {
		    group.remove(group.children[i]);
		}

		this._meshes = [];
		this._parent._loopArray.remove(this.loop);
		this._parent._resizeArray.remove(this.resize);

		this.onunload();

		return Promise.resolve();
	}

	cancelLoad() {
		this._activePromises.forEach(function(p) {
			p._canceled = true;
		});
	}

	show() {
		this.onshow();
	}

	hide() {
		this.onhide();
	}

	rebuild() {
		this.onrebuild();
	}

	loop() {
		if (this._destruct) return;

		this._loopArray.elements.forEach(function(loopFunc) {
			if (typeof loopFunc === 'function') {
				loopFunc();
			}
		});

		this.onloop();
	}
}

export default MeshDecorator;