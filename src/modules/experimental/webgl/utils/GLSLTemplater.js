var Templater = function(template, names) {
	this.names = names;
	this.template = template;
	this.chunks = [];

	this.addChunk = function(chunk) {
		this.chunks.push(chunk);
	}

	this.parse = function() {
		var self = this;
		var template = this.template;
		this.chunks.forEach(function(chunk) {
			self.names.forEach(function(name) {
				var posStart = chunk.indexOf('\/\/*' + name + '*');
				var posEnd = chunk.indexOf('\/\/*\/' + name + '*');
				var str;
				var tmPos;

				if (posStart === -1 || posEnd === -1) {
					// console.log('GLSLTemplater: no chunk position for name ' + name);
					return;
				}

				posStart += name.length + 5;

				str = chunk.substring(posStart + 1, posEnd);

				tmPos = template.indexOf('\/\/*\/' + name + '*');
				if (tmPos === -1) {
					console.log('GLSLTemplater: no template position for name ' + name);
					return;
				}

				template = template.slice(0, tmPos) 
					+ ' \n' + str + template.slice(tmPos);
			});
		});

		return template;
	}
}

export default Templater;