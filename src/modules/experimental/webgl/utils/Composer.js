import * as THREE from '/node_modules/three/build/three.module.js';
import simpleVertexShader from '../glsl/simple.vertex.js';
import simpleFragmentShader from '../glsl/simple.fragment.js';

// autoupdate
// auto - default. renders every time 'render' is called
// manual - renders only if 'needsUpdate' flag is set
// semi-auto - renders only if 'needsUpdate' flag is set or any uniform was changed

var Composer = function(renderer) {
	this.passes = [];
	this.renderer = renderer;
	this.autoupdate = 'auto';
	this.needsUpdate = false;
	this.previousRenderedUniforms = {}

	this.createBufferTexture = function(options) {
		var drawingBufferSize = this.renderer.getDrawingBufferSize(new THREE.Vector2());
		var bufferTexture;

		options = Object.assign({
			minFilter: THREE.LinearFilter,
			magFilter: THREE.LinearFilter,
			stencilBuffer: true,
			depthBuffer: true
		}, options);
		
		bufferTexture = new THREE.WebGLRenderTarget(
			drawingBufferSize.width,
			drawingBufferSize.height, {
				minFilter: options.minFilter, 
				magFilter: options.magFilter,
				format: THREE.RGBAFormat,
				stencilBuffer: options.stencilBuffer,
				depthBuffer: options.depthBuffer
			}
		);

		bufferTexture.texture.generateMipmaps = false;

		return bufferTexture;
	}

	this.renderPass = function(scene, camera) {
		var bufferTexture = this.createBufferTexture();

		this.passes.push({
			scene: scene,
			camera: camera,
			target: bufferTexture
		});

		return {
			texture: bufferTexture,
			scene: scene,
			camera: camera
		}
	}

	this.setAutoupdate = function(val) {
		if (['auto', 'manual', 'semi-auto'].indexOf(val) === -1) {
			console.warn('wrong autoupdate value');
			return;
		}
		this.autoupdate = val;
	}

	this.shaderPass = function(shader, data) {
		// can accept texture if first pass;
		var size = this.renderer.getSize(new THREE.Vector2());
		var drawingBufferSize = this.renderer.getDrawingBufferSize(new THREE.Vector2());
		var dpr = this.renderer.getPixelRatio();

		var material = new THREE.ShaderMaterial(shader);
		var geometry = new THREE.PlaneBufferGeometry(1, 1);
		var mesh = new THREE.Mesh(geometry, material);

		var scene, camera;
		var cameraDistance = 1000;
		var fov;
		var bufferTexture;

		var bufferTexture = this.createBufferTexture({
			depthBuffer: false,
			stencilBuffer: false
		});

		material.transparent = true;
		material.alpha = true;

		data = data || {};

		if (data.camera === 'perspective') {
			fov = 2 * (180 / Math.PI) * Math.atan(size.height / (2 * cameraDistance));
			camera = new THREE.PerspectiveCamera(fov, size.width / size.height, 100, 1500);
			camera.userData.distance = cameraDistance;
			camera.position.set(0, 0, cameraDistance);
			camera.up = new THREE.Vector3(0, 1, 0);
			camera.lookAt(0, 0, 0);
		} else {
			camera = new THREE.OrthographicCamera(
				size.width / -2,
				size.width / 2,
				size.height / 2,
				size.height / -2,
			-100, 100);
		}

		mesh.scale.set(size.width, size.height, 1);

		if (data.scene) {
			scene = scene;
		} else {
			scene = new THREE.Scene();
			scene._name = 'composer-scene';
		}

		scene.add(mesh);
		scene._inactive = false;

		if (!data.texture) {
			material.uniforms.tDiffuse = {
				type: 't',
				value: this.passes[this.passes.length - 1].target.texture
			}
		} else {
			material.uniforms.tDiffuse = {
				type: 't',
				value: data.texture
			}
		}

		this.passes.push({
			scene: scene,
			camera: camera,
			mesh: mesh,
			target: bufferTexture
		});

		return {
			mesh: mesh,
			scene: scene,
			camera: camera,
			texture: bufferTexture
		}
	}

	this.copyPass = function(data) {
		// just empty pass. for ping-ponging for example

		return this.shaderPass({
			uniforms: {},
			vertexShader: simpleVertexShader,
			fragmentShader: simpleFragmentShader
		}, data);
	}

	this.render = function(target) {
		var self = this;
		var renderFlag = false;

		if (this.autoupdate === 'auto') {
			renderFlag = true;
		} else if (this.autoupdate === 'manual' && this.needsUpdate) {
			renderFlag = true;
		} else if (this.autoupdate === 'semi-auto') {
			if (this.needsUpdate || this.checkUniformUpdates()) {
				renderFlag = true;
			}
		}

		this.passes.forEach(function(pass) {
			if (pass.scene._inactive) {
				renderFlag = false;
			}
		});

		if (!renderFlag) return;

		this.needsUpdate = false;

		this.passes.forEach(function(pass, index) {
			if (index === self.passes.length - 1) {
				if (target) {
					self.renderer.setRenderTarget(target);
					self.renderer.render(pass.scene, pass.camera);
				} else {
					if (pass.target) {
						pass.target.dispose();
						pass.target = null;
					}
					self.renderer.setRenderTarget(null);
					self.renderer.render(pass.scene, pass.camera);
				}
			} else {
				self.renderer.setRenderTarget(pass.target);
				self.renderer.render(pass.scene, pass.camera);
			}
			// if (pass.scene._name) {
			// 	console.log(pass);
			// }	
			self.renderer.setRenderTarget(null);
		});
	}

	this.updateUniforms = function(uniforms) {
		this.passes.forEach(function(pass, index) {
			if (!pass.mesh) return;

			for (var key in uniforms) {
				if (pass.mesh.material.uniforms.hasOwnProperty(key)) {
					pass.mesh.material.uniforms[key].value = uniforms[key];
				}
			}
		});
	}

	this.checkUniformUpdates = function() {
		var flag = false;
		var self = this;

		this.passes.forEach(function(pass, index) {
			if (!pass.mesh) return;
			for (var key in pass.mesh.material.uniforms) {
				if (pass.mesh.material.uniforms.hasOwnProperty(key)) {
					if (!self.previousRenderedUniforms.hasOwnProperty(key) ||
						self.previousRenderedUniforms[key] !== pass.mesh.material.uniforms[key].value) {
						self.previousRenderedUniforms[key] = pass.mesh.material.uniforms[key].value;
						flag = true;
					};
				}
			}
		});

		return flag;
	}

	this.resize = function() {
		var drawingBufferSize = this.renderer.getDrawingBufferSize(new THREE.Vector2());
		var size = this.renderer.getSize(new THREE.Vector2());
		var dpr = this.renderer.getPixelRatio();
		var screenDpr = window.devicePixelRatio;

		if (size.x === 0 || size.y === 0) {
			console.error('renderer size is 0. check CSS canvas size');
		}

		this.passes.forEach(function(pass) {
			if (pass.camera.type === 'OrthographicCamera') {
				pass.camera.left = size.width / -2;
				pass.camera.right = size.width / 2;
				pass.camera.top = size.height / 2;
				pass.camera.bottom = size.height / -2;
				pass.camera.updateProjectionMatrix();
			} else if (pass.camera.type === 'PerspectiveCamera') {
				pass.camera.fov = 2 * (180 / Math.PI) * Math.atan(size.height / (2 * pass.camera.userData.distance));
				pass.camera.aspect = size.width / size.height;				
				pass.camera.updateProjectionMatrix();
			}
			
			if (pass.target) {
				pass.target.setSize(drawingBufferSize.width, drawingBufferSize.height);
			}

			if (pass.mesh) {
				if (pass.mesh.material.uniforms.resolution) {
					pass.mesh.material.uniforms.resolution.value = [size.width, size.height];
				}
				if (pass.mesh.material.uniforms.screenResolution) {
					pass.mesh.material.uniforms.screenResolution.value = [drawingBufferSize.width, drawingBufferSize.height];
				}
				pass.mesh.scale.set(size.width, size.height, 1);
			}
		});
	}
}

export default Composer;