import * as THREE from '/node_modules/three/build/three.module.js';
import simpleFragmentShader from '../../glsl/simple.fragment.js';
import simpleVertexShader from '../../glsl/simple.vertex.js';

import GLTFLoader from '../../../../libs/GLTFLoader.js';

var loadingManager = new THREE.LoadingManager(function() {});
var texloader = new THREE.TextureLoader(loadingManager);

var meshLoader = new GLTFLoader(loadingManager);

// DOESN'T WORK!!!

function isTouchDevice() {
	var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
	var mq = function(query) {
		return window.matchMedia(query).matches;
	}

	if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
		return true;
	}

	var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
	return mq(query);
}

var addMesh = function(src, options) {
	var options = Object.assign({
		position: {x: 0, y: 0, z: 0},
		size: {w: 1, h: 1},
		vertexShader: simpleVertexShader,
		fragmentShader: simpleFragmentShader,
		uniforms: {}
	}, options);

	var self = this;
	var promises = [];
	var geometry, material, mesh;

	var type = options.type;
	var x, y, z, w, h;

	x = options.position[0];
	y = options.position[1];
	z = options.position[2];
	w = options.size[0];
	h = options.size[1];

	return new Promise(function(resolve, reject) {
		var texture;

		if (!src.url) {
			console.error('url property is missing');
			resolve(null);
			return;
		}

		meshLoader.load(src.url, function(data) {
			var scene = data.scene;
			var animations = data.animations;
			var group = new THREE.Group();

			scene.children.forEach(function(ch) {
				console.log(ch);
				if (ch.children && ch.children.length) {
					ch.children.forEach(function(c) {
						group.add(c);
					})
				} else {
					group.add(ch);
				}
			});

			// group.traverse(function(ch) {
			// 	if (ch.type === 'Mesh') {
			// 		ch.material = new THREE.MeshBasicMaterial({color: 0xFF0000})
			// 	}
			// });


			group.scale.set(1000, 1000, 1000);
			group.position.set(0, 0, 0);

			resolve({
				group: group,
				animations: animations
			});
		});
	});
}

export default addMesh;