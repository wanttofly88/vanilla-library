import * as THREE from '/node_modules/three/build/three.module.js';
import simpleFragmentShader from '../../glsl/simple.fragment.js';
import simpleVertexShader from '../../glsl/simple.vertex.js';

import GLTFLoader from '../../../../libs/OBJLoader.js';

let loadingManager = new THREE.LoadingManager(function() {});
let texloader = new THREE.TextureLoader(loadingManager);

var meshLoader = new GLTFLoader(loadingManager);

function isTouchDevice() {
	let prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
	let mq = function(query) {
		return window.matchMedia(query).matches;
	}

	if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
		return true;
	}

	let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
	return mq(query);
}

let addMesh = function(src, owerriteOptions) {
	let options = Object.assign({
		position: {x: 0, y: 0, z: 0},
		size: [1, 1, 1],
		vertexShader: simpleVertexShader,
		fragmentShader: simpleFragmentShader,
		uniforms: {}
	}, owerriteOptions);

	let self = this;
	let promises = [];
	let geometry, material, mesh;

	let x, y, z, w, h;

	x = options.position[0];
	y = options.position[1];
	z = options.position[2];

	return new Promise(function(resolve, reject) {
		let fragmentShader = options.fragmentShader;
		let optionalUniforms = Object.assign({}, options.uniforms);
		let dpi = window.devicePixelRatio;
		let size;

		if (!src) {
			console.warn('url property is missing');
			resolve(null);
			return;
		}

		fragmentShader = '#define DPI ' + (dpi % 1 === 0 ? dpi + '.0' : dpi) + ' \n' + options.fragmentShader;

		if (isTouchDevice()) {
			fragmentShader = '#define MOBILE 1.0\n' + fragmentShader;
		}

		material = new THREE.ShaderMaterial({
			uniforms: Object.assign({
				resolution: {type: 'v2', value: [w, h]},
				screenResolution: {type: 'v2', value: [1, 1]},
				uCursorPosition: {type: 'v2', value: [0, 0]},
				textureSize: {type: 'v2', value: size}
			}, optionalUniforms),
			vertexShader: options.vertexShader,
			fragmentShader: fragmentShader,
			transparent: true
		});

		meshLoader.load(src, function(group) {
			group.traverse(function(child) {
				if (child instanceof THREE.Mesh) {
					child.material = material;
				}
			});
			
			if (options.size.length < 3) {
				console.error('size option array needs 3 elements');
			}
			group.scale.set(options.size[0], options.size[1], options.size[2]);
			group.position.set(options.position.x, options.position.y, options.position.z);

			resolve({
				group: group
			});
		});
	});
}

export default addMesh;