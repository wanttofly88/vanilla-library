import * as THREE from '/node_modules/three/build/three.module.js';
import simpleFragmentShader from '../../glsl/simple.fragment.js';
import simpleVertexShader from '../../glsl/simple.vertex.js';

let loadingManager = new THREE.LoadingManager(function() {});
let texloader = new THREE.TextureLoader(loadingManager);

function isTouchDevice() {
	let prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
	let mq = function(query) {
		return window.matchMedia(query).matches;
	}

	if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
		return true;
	}

	let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
	return mq(query);
}

let addMesh = function(srcs, owerriteOptions) {
	let options = Object.assign({
		srcType: 'image',
		position: {x: 0, y: 0, z: 0},
		size: {w: 1, h: 1},
		sizeType: 'cover', // 'cover' || 'contain'
		vertexShader: simpleVertexShader,
		fragmentShader: simpleFragmentShader,
		uniforms: {}
	}, owerriteOptions);

	let self = this;
	let promises = [];
	let geometry, material, mesh;

	let sizeType = options.sizeType;
	let x, y, z, w, h;

	x = options.position[0];
	y = options.position[1];
	z = options.position[2];
	w = options.size[0];
	h = options.size[1];

	srcs.forEach(function(src, index) {
		promises.push(new Promise(function(resolve, reject) {
			let texture;

			if (options.srcType === 'video') {
				if (!src.url) {
					console.error('url property on mesh properties is undefined');
					return;
				}

				let videoElement = document.createElement('video');
				texture = new THREE.VideoTexture(videoElement);
				texture.needsUpdate = true;
				texture.magFilter = THREE.LinearFilter;
				texture.minFilter = THREE.LinearFilter;
				texture.name = src.name || '';
				texture._videoElement = videoElement;
				texture._type = 'video';

				videoElement.oncanplaythrough = function() {
					texture._size = [videoElement.videoWidth, videoElement.videoHeight];
					resolve(texture);
				}
				videoElement.src = src.url;
				videoElement.loop = true;
			} else if (options.srcType === 'canvas') {
				if (!options.canvas) {
					console.error('canvas property on mesh properties is undefined');
					return;
				}
				texture = new THREE.CanvasTexture(options.canvas);
				texture.needsUpdate = true;
				texture.magFilter = THREE.LinearFilter;
				texture.minFilter = THREE.LinearFilter;
				texture.name = options.name || canvas.name || 'undefined';
				texture._size = [options.canvas.width, options.canvas.height];
				texture._type = 'canvas';
				resolve(texture);
			} else if (options.srcType === 'image') {
				texture = texloader.load(src.url, function(e) {
					texture.needsUpdate = true;
					texture.magFilter = THREE.LinearFilter;
					texture.minFilter = THREE.LinearFilter;
					texture.name = src.name || '';
					texture._size = [texture.image.naturalWidth, texture.image.naturalHeight];
					resolve(texture);
				});
			} else {
				if (!options.srcType) {
					console.error('srcType property on mesh properties is undefined');
				} else {
					console.error('unrecognized srcType property on mesh properties');
				}
				
				return;
			}
		}));
	});

	return Promise.all(promises).then(function(textures) {
		let fragmentShader = options.fragmentShader;
		let optionalUniforms = Object.assign({}, options.uniforms);
		let dpi = window.devicePixelRatio;
		let size;

		for (let key in optionalUniforms) {
			optionalUniforms[key] = Object.assign({}, optionalUniforms[key])
			if (options[key]) {
				optionalUniforms[key].value =  options[key]
			}
		}

		fragmentShader = '#define DPI ' + (dpi % 1 === 0 ? dpi + '.0' : dpi) + ' \n' + options.fragmentShader;

		if (isTouchDevice()) {
			fragmentShader = '#define MOBILE 1.0\n' + fragmentShader;
		}

		if (sizeType === 'cover') {
			fragmentShader = 
				'#define COVER 1.0\n' +
				fragmentShader;
		} else if (sizeType === 'contain') {
			fragmentShader = 
				'#define CONTAIN 1.0\n' +
				fragmentShader;
		}

		if (textures[0]._type = 'canvas') {
			size = [textures[0].image.width, textures[0].image.height];
		} else {
			size = [textures[0]._size[0], textures[0]._size[1]];
		}

		material = new THREE.ShaderMaterial({
			uniforms: Object.assign({
				tDiffuse: {type: 't', value: textures[0]},
				resolution: {type: 'v2', value: [w, h]},
				screenResolution: {type: 'v2', value: [1, 1]},
				uCursorPosition: {type: 'v2', value: [0, 0]},
				textureSize: {type: 'v2', value: size}
			}, optionalUniforms),
			vertexShader: options.vertexShader,
			fragmentShader: fragmentShader,
			transparent: true
		});

		geometry = new THREE.PlaneBufferGeometry(1, 1);
		mesh = new THREE.Mesh(geometry, material);
		mesh.scale.set(w, h, 1);
		mesh.position.set(x, y, z);

		mesh.name = options.name || srcs[0];

		mesh._originalMetrics = {
			x: x,
			y: y,
			z: z,
			w: w,
			h: h,
			type: sizeType
		}

		mesh._textures = textures;

		return Promise.resolve(mesh);
	});
}

export default addMesh;