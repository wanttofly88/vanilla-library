var FunctionArray = function() {
	this.elements = [];
	this.push = function(func) {
		this.elements.push(func);
	}
	this.remove = function(func) {
		this.elements = this.elements.filter(function(el) {
			return el !== func;
		});
	}
}

export default FunctionArray;