import dispatcher from '../dispatcher.js';
import * as THREE from '/node_modules/three/build/three.module.js';
import resizeStore from '../resize/resize.store.js';
import stylesStore from '../styles/styles.store.js';
import cursorStore from '../events/cursor.store.js';

import gyroscopeDecorator from './decorators/gyroscope.decorator.js';
import Composer from './utils/Composer.js';
import {FunctionArray} from '../utils/array.utils.js';
import {PowerEase} from '../utils/ease.utils.js';

import simpleVertexShader from './glsl/simple.vertex.js';
import simpleFragmentShader from './glsl/simple.fragment.js';

var dpr = window.devicePixelRatio;
var texloader = new THREE.TextureLoader();

var name = 'undefined';

var defaultOptions = {
	shortName: name,
	name: 'scene-' + name,

	// camera
	cameraType: 'orthographic',	
	cameraDistance: 1000,
	
	// postprocessing
	uniforms: {},
	defines: {},
	fragmentShader: simpleFragmentShader,

	decorators: []
}

// this._smoothCursor = {  -smoothed cursor position in range of [-1, 1]
// 	x: 0,
// 	y: 0
// }

// this._cursor = {  -mouse position
// 	relative: {x: 0, y: 0},   -mouse position in range of [-1, 1]
// 	absolute: {x: 0, y: 0}    -mouse position in screen coordinates
// }

// this._pointer = {	-touch position
// 	down: false,	-if touchstart was called
// 	relative: {x: 0, y: 0},		-last touch position in range of [-1, 1]
// 	absolute: {x: 0, y: 0}		-last touch position in screen coordinates
// }

// this._gyroQuaternion = new THREE.Quaternion();	-parsed gyroscope quaternion

class SceneDecorator {
	constructor(parent, options) {
		this._active = false;
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this._shortName = this._options.shortName;
		this._name = this._options.name;

		this._touch = this._parent._touch;
		this._renderer = parent._renderer;

		this._loopArray = new FunctionArray();
		this._resizeArray = new FunctionArray();
		this._showArray = new FunctionArray();
		this._hideArray = new FunctionArray();

		this._activeFlags = new FunctionArray();

		this._smoothCursor = {
			x: 0,
			y: 0
		}
		this._cursor = {
			relative: {x: 0, y: 0},
			absolute: {x: 0, y: 0}
		}
		this._pointer = {
			down: false,
			relative: {x: 0, y: 0},
			absolute: {x: 0, y: 0}
		}
		this._gyroQuaternion = new THREE.Quaternion();
		this._gyroEuler = new THREE.Euler();

		this.touchHandler = {
			touchstart: function(e) {
				var ww = resizeStore.getData().width;
				var wh = resizeStore.getData().height;

				if (e.touches.length !== 1) return;
				this._pointer.down = true;
				var x = e.touches[0].clientX;
				var y = e.touches[0].clientY;

				this._pointer.absolute = {
					x: x,
					y: y,
				}
				this._pointer.relative = {
					x: (2 * x - ww) / ww,
					y: (2 * y - wh) / wh
				}
			}.bind(this),

			touchmove: function(e) {
				var ww = resizeStore.getData().width;
				var wh = resizeStore.getData().height;

				if (e.touches.length !== 1) return;
				var x = e.touches[0].clientX;
				var y = e.touches[0].clientY;

				this._pointer.absolute = {
					x: x,
					y: y,
				}
				this._pointer.relative = {
					x: (2 * x - ww) / ww,
					y: (2 * y - wh) / wh
				}
			}.bind(this),

			touchend: function() {
				this._pointer.down = false;
			}.bind(this),
		}

		this.resize = this.resize.bind(this);
		this.loop = this.loop.bind(this);
		this.handleDispatcher = this.handleDispatcher.bind(this);
	}

	// callbacks
	oninit() {}
	ondestroy() {}
	onshow() {}
	onhide() {}
	onload() {}
	onunload() {}
	onresize() {}
	onloop() {}
	//

	init() {
		var sceneData;
		var img, style, src;
		var self = this;

		var size = this._renderer.getSize(new THREE.Vector2());

		var ww = size.width;
		var wh = size.height;

		var promise = new Promise(function(resolve, reject) {
			self.buildScene({cameraType: self._options.cameraType})
			.then(function(sceneData) {
				var composerData;

				self._scene = sceneData.scene;
				self._scene._inactive = true;
				self._scene._name = self._name;

				self._camera = sceneData.camera;

				self._composer = new Composer(self._renderer);
				self._composer.setAutoupdate('auto');

				self._composer.renderPass(self._scene, self._camera);

				composerData = self._composer.shaderPass({
					uniforms: Object.assign({
						cursorPosition: {type: 'v2', value: [0, 0]},
						visibility: {type: 'f', value: 0},
						resolution: {type: 'v2', value: [size.width, size.height]}
					}, self._options.uniforms),
					fragmentShader: self.getPostprocessingShader(),
					vertexShader: simpleVertexShader
				});

				self._resultMesh = composerData.mesh;

				// decorators
				self._options.decorators.forEach(function(d) {
					d.attach(self);
				});

				self.resize();

				gyroscopeDecorator.attach(self);
				dispatcher.subscribe(self.handleDispatcher);

				self.oninit();

				resolve();

				dispatcher.dispatch({
					type: 'scene:ready',
					name: self._name
				});
			})
		});

		this._activeFlags.push(function() {
			return this._loaded;
		}.bind(this));

		// this._dataGroup = document.querySelector('.gl-data[data-name="' + this._shortName + '"]');

		// if (this._dataGroup) {
		// 	this._srcElements = Array.prototype.slice.call(this._dataGroup.getElementsByClassName('src'));
		// 	this._sources = {}

		// 	// sound data
		// 	this._soundSrc = this._dataGroup.getAttribute('data-sound');
		// 	this._soundMapSrc = this._dataGroup.getAttribute('data-soundMap');
		// 	this._musicSrc = this._dataGroup.getAttribute('data-music');

		// 	this._srcElements.forEach(function(layer) {
		// 		var layerName = layer.getAttribute('data-name');
		// 		var s = layer.getAttribute('data-src');

		// 		self._sources[layerName] = s;
		// 	});
		// }

		return promise;
	}

	destroy() {
		var self = this;

		this._options.decorators.forEach(function(d) {
			d.detach(self);
		});

		gyroscopeDecorator.detach(this);
		dispatcher.unsubscribe(this.handleDispatcher);

		this._parent._loopArray.remove(this.loop);

		this.ondestroy();
	}

	checkIfActive() {
		if (this._activeFlags.reduce(function(prev, flag) {
			if (typeof prev === 'function') {
				return prev() && flag();
			} else {
				return prev && flag();
			}
		})) {
			if (!this._active) {
				this._active = true;
				this._scene._inactive = false;
				console.log('scene ' + this._name + ' activeted');
				this._parent._scene.add(this._resultMesh);
				this._parent._loopArray.push(this.loop);
				this._parent._resizeArray.push(this.resize);

				if (this._touch) {
					document.addEventListener('touchstart', this.touchHandler.touchstart);
					document.addEventListener('touchmove', this.touchHandler.touchmove);
					document.addEventListener('touchend', this.touchHandler.touchend);
				}
			}
		} else {
			if (this._active) {
				this._active = false;
				this._scene._inactive = true;
				console.log('scene ' + this._name + ' deactiveted');
				this._parent._scene.remove(this._resultMesh);
				this._parent._resizeArray.remove(this.resize);
				this._parent._loopArray.remove(this.loop);

				if (this._touch) {
					document.addEventListener('touchstart', this.touchHandler.touchstart);
					document.addEventListener('touchmove', this.touchHandler.touchmove);
					document.addEventListener('touchend', this.touchHandler.touchend);
				}
			}
		}
	}

	getPostprocessingShader() {
		var dpi = this._renderer.getPixelRatio();
		var fragmentShader = '';
		var key, type, value;

		for (key in this._options.defines) {
			type = this._options.defines[key].type;
			value = this._options.defines[key].value;

			if (Array.isArray(value)) {
				value = value.map(function(v) {
					return (v % 1 === 0 ? v + '.0' : v.toString())
				}).join(',');
			} else {
				value = value % 1 === 0 ? value + '.0' : value.toString()
			}

			if (type === 'f') {
				fragmentShader += '#define ' + key + ' ' + value + '\n';
			} else if (type === 'v2') {
				fragmentShader += '#define ' + key + ' vec2(' + value + ')\n';
			} else if (type === 'v3') {
				fragmentShader += '#define ' + key + ' vec3(' + value + ')\n';
			} else if (type === 'v4') {
				fragmentShader += '#define ' + key + ' vec4(' + value + ')\n';
			}
		}

		if (this._touch) {
			fragmentShader += 
			'#define TOUCH 1\n';
		}

		fragmentShader += 
			'#define M_PI 3.1415926535897932384626433832795\n' +
			'#define DPI ' + (dpi % 1 === 0 ? dpi + '.0' : dpi) + ' \n' +
			this._options.fragmentShader;

		return fragmentShader;
	}

	updateUniforms(uniforms) {
		for (var decoratorId in this._decorators) {
			if (this._decorators[decoratorId].updateUniforms) {
				this._decorators[decoratorId].updateUniforms(uniforms);
			}
		}
		this._composer.updateUniforms(uniforms);
	}

	buildScene(options) {
		var camera, scene, renderer;
		var size = this._renderer.getSize(new THREE.Vector2());
		var ww = size.width;
		var wh = size.height;
		var fov, distance;

		distance = this._options.cameraDistance;

		if (options.cameraType === 'orthographic') {
			camera = new THREE.OrthographicCamera(ww / -2, ww / 2,  wh / 2, wh / -2, -10000, 10000);
			scene = new THREE.Scene();
		} else if (options.cameraType === 'perspective') {
			camera = new THREE.PerspectiveCamera(60, ww / wh, 1, 100000);
			camera.userData.distance = distance;
			fov = 2 * (180 / Math.PI) * Math.atan(size.height / (2 * distance));
			camera.fov = fov;
			camera.up = new THREE.Vector3(0, 1, 0);
			camera.updateProjectionMatrix();

			camera.position.set(0, 0, distance);
			camera.lookAt(0, 0, 0);
		}

		scene = new THREE.Scene();

		return Promise.resolve({
			scene: scene,
			camera: camera
		});
	}

	render() {
		var composer = this._composer;

		if (this._loaded) {
			composer.render(null);
		}
	}

	resize() {
		var self = this;
		var fov, distance;
		var wh = this._renderer.getSize(new THREE.Vector2()).height;
		var ww = this._renderer.getSize(new THREE.Vector2()).width;
		var camera = this._camera;

		this._composer.resize();

		if (camera.type === 'PerspectiveCamera') {
			distance = camera.userData.distance;
			fov = 2 * (180 / Math.PI) * Math.atan(wh / (2 * distance));
			camera.fov = fov;
			camera.position.set(
				camera.position.x,
				camera.position.y,
				camera.userData.distance
			);
			camera.lookAt(0, 0, 0);
			camera.updateProjectionMatrix();
			
		}

		this._resizeArray.forEach(function(resizeFunc) {
			resizeFunc();
		});

		this.onresize();
	}

	load(options) {
		var self = this;

		if (this._loaded) return;
		this._loaded = true;

		if (this._soundSrc && this._soundMapSrc) {
			dispatcher.dispatch({
				type: 'sound:load',
				src: this._soundSrc,
				mapSrc: this._soundMapSrc
			});
		}

		return Promise.all(
		Object.values(self._decorators)
			.map(d => d.load())
			.concat([])
		).then(function() {
			self.checkIfActive();

			self.onload();
			return Promise.resolve();
		}).catch(function(reject) {
			console.error(reject);
		});
	}

	unload(options) {
		var self = this;

		if (!this._loaded) return;
		this._loaded = false;

		this.checkIfActive();

		return Promise.all(Object.values(self._decorators).map(d => d.unload())).then(function() {
			if (self.onunload) self.onunload()
			return Promise.resolve();
		}).catch(function(reject) {
			console.error(reject);
		});
	}

	cancelLoad() {
		var self = this;

		return Promise.all(Object.values(self._decorators).map(d => d.cancelLoad())).then(function() {
			self._loaded = false;
			self.checkIfActive();
			return Promise.resolve();
		}).catch(function(reject) {
			self._loaded = false;
			self.checkIfActive();
			console.error(reject);
		});
	}

	show() {
		if (this._musicSrc) {
			dispatcher.dispatch({
				type: 'audio:stop'
			});
			dispatcher.dispatch({
				type: 'audio:set-playlist',
				id: this._shortName,
				playlist: [{
					src: this._musicSrc,
					name: 0,
					index: 0
				}]
			});
		}

		this._showArray.elements.forEach(function(loopFunc) {
			if (typeof loopFunc === 'function') {
				loopFunc();
			}
		});
		this.onshow();
	}

	hide(e) {
		this._hideArray.elements.forEach(function(loopFunc) {
			if (typeof loopFunc === 'function') {
				loopFunc();
			}
		});
		this.onhide();
	}

	handleDispatcher(e) {
	}

	loop() {
		var cursorData = cursorStore.getData();
		var ww = resizeStore.getData().width;
		var wh = resizeStore.getData().height;
		var x, y;

		this.checkIfActive();

		x = (2 * cursorData.screenX - ww) / ww;
		y = (2 * cursorData.screenY - wh) / wh;

		this._cursor = {
			absolute: {
				x: cursorData.screenX,
				y: cursorData.screenY
			},
			relative: {
				x: x,
				y: y
			}
		}
		
		let lerpFactor = stylesStore.getData()[this._shortName] ? stylesStore.getData()[this._shortName].lerpFactor : stylesStore.getData().lerpFactor;
		this._smoothCursor = {
			x: this._smoothCursor.x + (this._cursor.relative.x - this._smoothCursor.x) / lerpFactor,
			y: this._smoothCursor.y + (this._cursor.relative.y - this._smoothCursor.y) / lerpFactor,
			r: cursorStore.getData().cursorClick + 1
		}

		this._loopArray.elements.forEach(function(loopFunc) {
			if (typeof loopFunc === 'function') {
				loopFunc();
			}
		});

		this.onloop();

		this.render();
	}
}

export default SceneDecorator;