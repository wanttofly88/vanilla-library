/**
 * @author richt / http://richt.me
 * @author WestLangley / http://github.com/WestLangley
 *
 * W3C Device Orientation control (http://w3c.github.io/deviceorientation/spec-source-orientation.html)
 */

import {Euler, Math as _Math, Quaternion, Vector3} from '/node_modules/three/build/three.module.js';

var name = 'gyroscope-decorator';

var Decorator = function(component, options) {
	// this.object.rotation.reorder( 'YXZ' );

	this._quaternion = new Quaternion();

	this._deviceOrientation = {};
	this._screenOrientation = 0;

	this._alphaOffset = 0; // radians
	this._multiplyer = 1;

	this._component = component;
	this._options = {}

	this.init = function() {
		// if (navigator.permissions) {
		// 	navigator.permissions.query({name: 'gyroscope'}).then(function(result) {
		// 		if (result.state === 'granted') {
		// 			self._gyroscopePermissionGranted = true;
		// 			self._gyroscope = new Gyroscope({frequency: 60});
		// 			self._gyroscope.start();
		// 			// console.dir(self._gyroscope);
		// 		}
		// 	});
		// }
	}.bind(this);

	this.onDeviceOrientationChange = function (e) {
		if (!this._initial) {
			this._initial = e;
		}

		this._deviceOrientation = {
			alpha: this._initial.alpha - e.alpha,
			beta: this._initial.beta - e.beta + 90,
			gamma: this._initial.gamma - e.gamma
		};
	}.bind(this);

	this.onGyroscope = function(e) {
		this._deviceOrientation = e;
	}.bind(this);

	this.onScreenOrientationChange = function () {
		this._screenOrientation = window.orientation || 0;
	}.bind(this);

	this.setQuaternion = function () {
		var zee = new Vector3(0, 0, 1);
		var euler = new Euler();
		var q0 = new Quaternion();
		var q1 = new Quaternion(-Math.sqrt(0.5), 0, 0, Math.sqrt(0.5)); // - PI/2 around the x-axis

		return function (quaternion, alpha, beta, gamma, orient) {
			euler.set(beta, alpha, - gamma, 'YXZ'); // 'ZXY' for the device, but 'YXZ' for us
			quaternion.setFromEuler(euler); // orient the device
			quaternion.multiply(q1); // camera looks out the back of the device, not the top
			quaternion.multiply(q0.setFromAxisAngle(zee, - orient)); // adjust for screen orientation
		};
	}.bind(this)();

	this.load = function () {
		this._initial = null;

		this.onScreenOrientationChange(); // run once on load
		window.addEventListener('orientationchange', this.onScreenOrientationChange);

		// if (this._gyroscope) {
		// 	this._gyroscope.addEventListener('reading', this.onGyroscope);
		// } else if (window.DeviceOrientationEvent) {
			//this._multiplyer = 1;
			window.addEventListener('deviceorientation', this.onDeviceOrientationChange);
		//}

		this._enabled = true;
		this._component._loopArray.push(this.loop);
	}.bind(this);

	this.unload = function () {
		window.removeEventListener('orientationchange', this.onScreenOrientationChange);
		window.removeEventListener('deviceorientation', this.onDeviceOrientationChange);
		this._enabled = false;
		this._component._loopArray.remove(this.loop);
	}.bind(this);

	this.cancelLoad = function() {}.bind(this);

	this.loop = function () {
		if (this._enabled === false) return;
		var device = this._deviceOrientation;

		if (device) {
			var alpha = device.alpha ? _Math.degToRad(device.alpha) + this._alphaOffset : 0; // Z
			var beta = device.beta ? _Math.degToRad(device.beta) : Math.PI / 2; // X'
			var gamma = device.gamma ? _Math.degToRad(device.gamma) : 0; // Y''
			var orient = this._screenOrientation ? _Math.degToRad(this._screenOrientation) : 0; // O
			this.setQuaternion(this._quaternion, alpha, beta, gamma, orient);

			this._component._gyroQuaternion.copy(this._quaternion);
			this._component._gyroEuler.setFromQuaternion(this._quaternion);
		}
	}.bind(this);
}

var attach = function(component, options) {
	var decorator;

	if (!component._decorators) {
		component._decorators = {};
	}
	if (!component._decorators[name]) {
		decorator = new Decorator(component, options);
		component._decorators[name] = decorator;
		decorator.init();
	} else {
		decorator = component._decorators[name];
	}

	return decorator;
}

var detach = function(component, options) {
	
}

export default {
	attach: attach,
	detach: detach
}
