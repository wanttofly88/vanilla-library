import dispatcher from '../dispatcher.js';
import * as THREE from '/node_modules/three/build/three.module.js';
import {offset} from '../utils/dom.utils.js';

import resizeStore from '../resize/resize.store.js';

import Composer from '../webgl/utils/Composer.js';
import {FunctionArray} from '../utils/array.utils.js';

// import fxaaFragmentShader from './glsl/fxaa.fragment.js';
// import mainPostFragmentShader from './glsl/main-post.fragment.js';

import simpleFragmentShader from '../webgl/glsl/simple.fragment.js';
import simpleVertexShader from '../webgl/glsl/simple.vertex.js';

import exampleSceneDecorator from './example/example.scene.js';

// import stylesSceneDecorator from './scenes/styles/styles.scene.js';
import {datasetToOptions} from '../utils/component.utils.js';

let loadingManager = new THREE.LoadingManager(function() {});
let texloader = new THREE.TextureLoader(loadingManager);

let decorators = [exampleSceneDecorator];

THREE.Cache.enabled = true;

const defaultOptions = {
	__namespace: 'stage'
}

function isTouchDevice() {
	let prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
	let mq = function(query) {
		return window.matchMedia(query).matches;
	}

	if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
		return true;
	}

	let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
	return mq(query);
}

function isWebGLEnabled() {
	let canvas = document.createElement('canvas');
	let gl = canvas.getContext('webgl')
	|| canvas.getContext('experimental-webgl');
	return (gl && gl instanceof WebGLRenderingContext)
}

let _buildStage = function(options) {
	let camera, scene, renderer;
	let ww = this.clientWidth;
	let wh = this.clientHeight;

	options = options || {};

	camera = new THREE.OrthographicCamera(ww / -2, ww / 2,  wh / 2, wh / -2, -10000, 10000);

	renderer = new THREE.WebGLRenderer({
		antialias: true,
		alpha: true,
		premultipliedAlpha: true
	});

	// renderer.setPixelRatio(Math.min(dpr, window._lets.MAXDPR || 1));
	renderer.setPixelRatio(1);
	renderer.setSize(ww, wh);

	renderer.domElement.style.width  = '100%';
	renderer.domElement.style.height = '100%';
	renderer.setClearColor(0x1E1E1E, 0);

	this.appendChild(renderer.domElement);
	this._renderer = renderer;

	scene = new THREE.Scene();
	scene._name = 'main-scene';

	return Promise.resolve({
		scene: scene,
		camera: camera,
		renderer: renderer
	});
}

let _loop = function() {
	if (this._destruct) return;

	this._loopArray.forEach(function(loopFunc) {
		loopFunc();
	});

	if (this._onScreen) {
		this.render();
	}

	requestAnimationFrame(this.loop);
}

let _render = function() {
	let composer = this._composer;
	composer.render();
}

let _handleResize = function() {
	let self = this;

	let ww = this.clientWidth;
	let wh = this.clientHeight;

	if (!this._renderer) return;
	if (this._ww === ww && this._wh === wh) return;

	this._ww = ww;
	this._wh = wh;
	
	this._renderer.setSize(ww, wh);
	this._composer.resize();

	this._resizeArray.forEach(function(resizeFunc) {
		resizeFunc();
	});
	
	let margin = 200;
	let topElement = document.getElementById('top-sections');
	this._offset = topElement.clientHeight + (wh + margin) * 2;
}

let _handleScroll = function() {
	console.error('this is wrong!');
	let scrolled = scrollStore.getData().top;
	let wh = resizeStore.getData().height;
	let margin = 200;

	if (scrolled > this._offset
		&& scrolled < this._offset + wh + margin * 2) {
		this._scrollDisabled = false;
	} else {
		this._scrollDisabled = true;
	}

	console.log(this._scrollDisabled);
}

let _updateUniforms = function(uniforms) {
	this._composer.updateUniforms(uniforms);

	for (let key in this._decorators) {
		if (this._decorators[key].updateUniforms) {
			this._decorators[key].updateUniforms(uniforms);
		}
	}
}

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._options = datasetToOptions(this.dataset, defaultOptions);

		this._destruct = false;
		this._onScreen = true;

		this.buildStage = _buildStage.bind(this);
		this.handleResize = _handleResize.bind(this);
		this.handleScroll = _handleScroll.bind(this);
		this.updateUniforms = _updateUniforms.bind(this);

		this._loopArray = new FunctionArray();
		this._resizeArray = new FunctionArray();

		this.loop = _loop.bind(this);
		this.render = _render.bind(this);
	}

	connectedCallback() {
		let stageData;
		let img, style, src;
		let self = this;

		let ww = this.clisentWidth;
		let wh = this.clisentHeight;

		// if (navigator.permissions) {
		// 	navigator.permissions.query({name: 'gyroscope'}).then(function(result) {
		// 		self._gyroscopePermissionGranted = true;
		// 	})
		// }

		this._id = this.getAttribute('data-id');
		this._touch = isTouchDevice() ? 1 : 0;

		if (!isWebGLEnabled()) return;

		this.buildStage({
			renderer: true
		})
		.then(function(stageData) {
			let screenSize = self._renderer.getSize(new THREE.Vector2());
			let dpi = self._renderer.getPixelRatio();

			self._scene = stageData.scene;
			self._camera = stageData.camera;

			// let cube = new THREE.Mesh(
			// 	new THREE.BoxGeometry(100, 100, 100),
			// 	new THREE.MeshBasicMaterial({color: 0x00ff00})
			// );
			// self._scene.add(cube);

			window._mainScene = self._scene;
			window._mainLoop = self._loopArray;

			self._composer = new Composer(self._renderer);
			self._composer.setAutoupdate('auto');
			self._resultMesh = self._composer.renderPass(self._scene, self._camera);

			// self._resultMesh = self._composer.shaderPass({
			// 	uniforms: {
			// 		uDarkMask: {type: 'v2', value: [0, 0]},
			// 		resolution: {type: 'v2', value: [screenSize.width, screenSize.height]},
			// 		dpi: {type: 'f', value: dpi} 
			// 	},
			// 	fragmentShader: mainPostFragmentShader,
			// 	vertexShader: simpleVertexShader
			// }).mesh;

			Promise.all(decorators.map(d => d.attach(self)))
			.then(function() {
				if (self._decorators) {
					return Promise.all(Object.values(self._decorators).map(d => d.load()))
				} else {
					return Promise.resolve();
				}
			}).then(function() {
				let pw = document.getElementsByClassName('page-wrapper')[0];
				pw.classList.add(self._options.__namespace + '-active');

				self.handleResize();
				self.handleScroll();
				resizeStore.subscribe(self.handleResize);
				scrollStore.subscribe(self.handleScroll);
				self.loop();
			}).catch(function(reject) {
				console.error(reject);
			});
		});
	}

	disconnectedCallback() {
		let self = this;
		let pw = document.getElementsByClassName('page-wrapper')[0];
		pw.classList.remove(this._options.__namespace + '-active');

		this._destruct = true;

		decorators.map(d => d.detach(self));

		resizeStore.unsubscribe(this.handleResize);

		if (this._renderer) {
			this._renderer.forceContextLoss();
			this._renderer.context = null;
			this._renderer.domElement = null;
			this._renderer = null;
		}
	}
}

customElements.define('main-stage', ElementClass);

export default ElementClass;