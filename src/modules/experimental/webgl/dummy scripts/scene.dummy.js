import dispatcher from '../../../dispatcher.js';
import * as THREE from '/node_modules/three/build/three.module.js';
import postFragmentShader from './styles-post.fragment.js';
import SceneDecorator from '../../../webgl/Scene.decorator.js';

import meshDecorator from './styles.mesh.js';

const shortName = null;
if (shortName === null) {
	console.error('shortName is null');
}

const name = 'scene-' + shortName;
const cameraType = 'orthographic';
const dpr = window.devicePixelRatio;
const decorators = [meshDecorator];

const defaultOptions = {
	name: name,
	shortName: shortName,
	cameraType: cameraType,
	decorators: decorators,
	uniforms: {
	},
	fragmentShader: postFragmentShader,
}

const texloader = new THREE.TextureLoader();

class Decorator extends SceneDecorator {
	constructor(parent, options) {
		super(parent, options);

		this._parent = parent;
		this._options = Object.assign({
		}, defaultOptions, options);
	}

	oninit() {}

	ondestroy() {}

	onload() {}

	onshow() {}

	onloop() {}

	onunload() {}

	onshide() {}

	onresize() {}
}

let attach = function(parent, options) {
	let decorator;

	let promise = new Promise(function(resolve, reject) {
		options = Object.assign(defaultOptions, options);

		if (!parent._decorators) {
			parent._decorators = {};
		}

		if (!parent._decorators[name]) {
			decorator = new Decorator(parent, options);
			parent._decorators[name] = decorator;
			decorator.init()
				.then(function() {
					resolve(decorator)
				});
		} else {
			decorator = parent._decorators[name];
			resolve(decorator)
		}	
	});

	return promise;
}

let detach = function(parent, options) {
	let decorator = parent._decorators[name];
	decorator.destroy();
}

export default {
	attach: attach,
	detach: detach
}