import dispatcher from '../../../dispatcher.js';
import * as THREE from '/node_modules/three/build/three.module.js';
import resizeStore from '../../../resize/resize.store.js';

import fragmentShader from '../../glsl/simple.fragment.js';
import vertexShader from '../../glsl/simple.vertex.js';

import MeshDecorator from '../../../webgl/Mesh-2d-group.decorator.js';

let shortName = null;

if (shortName === null) {
	console.error('shortName is null');
}
let name = 'mesh-' + shortName;

const defaultOptions = {
	name: name,
	shortName: shortName,
	fragmentShader: fragmentShader,
	vertexShader: vertexShader,
	uniforms: {},
	meshData: [{
		srcType: 'image',
		position: [0, 0, 0],
		size: [size.x, size.y],
		type: 'cover'
	}]
}

class Decorator extends MeshDecorator {
	constructor(parent, options) {
		super(parent, options);
		this._parent = parent;

		this._options = Object.assign({
		}, defaultOptions, options);
	}

	onloop() {
	}

	onshow() {
	}

	onhide() {
	}

	onload() {
	}

	onunload() {
	}
}

let attach = function(parent, options) {
	let decorator;
	let size = parent._renderer.getSize(new THREE.Vector2());

	options = Object.assign(defaultOptions, options);

	if (!parent._decorators) {
		parent._decorators = {};
	}
	if (!parent._decorators[name]) {
		decorator = new Decorator(parent, options);
		parent._decorators[name] = decorator;
		decorator.init();

		decorator.resize();
		resizeStore.subscribe(decorator.resize);
	} else {
		decorator = parent._decorators[name];
	}

	return decorator;
}

let detach = function(parent, options) {
	resizeStore.unsubscribe(decorator.resize);
}

export default {
	attach: attach,
	detach: detach
}