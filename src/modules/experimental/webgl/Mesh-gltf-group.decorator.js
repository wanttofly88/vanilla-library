import dispatcher from '../dispatcher.js';
import * as THREE from '/node_modules/three/build/three.module.js';
import resizeStore from '../resize/resize.store.js';
import simpleVertexShader from './glsl/simple.vertex.js';
import simpleFragmentShader from './glsl/simple.fragment.js';

import addMesh from './utils/mesh.utils/addMesh3d.js';
import {FunctionArray} from '../utils/array.utils.js';

import GLTFloader from '../../libs/GLTFLoader.js';

var loadingManager = new THREE.LoadingManager(function() {});
var texloader = new THREE.TextureLoader(loadingManager);
var sceneLoader = new GLTFloader(loadingManager);

var defaultOptions = {
	meshData: [],
	uniforms: {},
	vertexShader: simpleVertexShader,
	fragmentShader: simpleFragmentShader
}

// mesh data: {
// 	srcs: [{{source name}}], // can pass multiple textures for whatever reson. frame animations for example.
//	options: {
//		position: [0, 0, 0],
//		size: [1440, 900],
//		type: 'cover' || 'contain'
//	}
//}

// possible callbacks:
// onload
// onunload
// onloop
// onshow
// onhide
// onresize
// onrebuild

// props:
// _screenMode: 'mobile' || 'desktop' - if width is greater then height
// _touch: true || false - if touch screen

var MeshDecorator = function(parent, options) {
	this._parent = parent;
	this._options = Object.assign({}, defaultOptions, options);

	this._meshes = [];
	this._touch = this._parent._touch;
	this._textures = [];

	// hooks
	this.oninit = function() {};
	this.ondestroy = function() {};
	this.onload = function() {};
	this.onloop = function() {};
	this.onunload = function() {};
	this.onresize = function() {};
	this.onshow = function() {};
	this.onhide = function() {};

	this._loopArray = new FunctionArray();
	this._resizeArray = new FunctionArray();

	this._activePromises = [];

	this.init = function() {
		var self = this;
		var toRemove = [];

		var optionalUniforms = this._options.uniforms;

		this._group = new THREE.Group();
		this._parent._scene.add(this._group);

		this.oninit();
	}.bind(this);

	this.destroy = function() {
		this._parent._loopArray.remove(this.loop);
		this._parent._resizeArray.remove(this.resize);
		this._parent._scene.remove(this._group);
		this.ondestroy();
	}.bind(this);

	this.loadTextures = function(src) {
		return new Promise(function(resolve, reject) {
			var texture = texloader.load(src, function(e) {
				texture.needsUpdate = true;
				texture.magFilter = THREE.LinearFilter;
				texture.minFilter = THREE.LinearFilter;
				texture.name = src.name || '';
				texture._size = [texture.image.naturalWidth, texture.image.naturalHeight];
				resolve(texture);
			});
		});
	}.bind(this);

	this.loadGLTF = function(src) {
		return new Promise(function(resolve, reject) {
			sceneLoader.load(src, function(gltf) {
				resolve(gltf);
			});
		});
	}.bind(this);

	this.updateUniforms = function(u) {
		this._meshes.forEach(function(m) {
			if (!m.material.uniforms) return;
			for (var key in u) {
				if (m.material.uniforms.hasOwnProperty(key)) {
					m.material.uniforms[key].value = u[key];
				}
			}
		});
	}.bind(this);

	this.getMeshByName = function(name) {
		return this._meshes.find(function(m) {
			return m.name === name
		});
	}.bind(this);

	this.resize = function(mesh) {
		var size = this._parent._renderer.getSize(new THREE.Vector2());
		var screenSize = resizeStore.getData();

		function resizeMesh(m) {
			if (m.material.uniforms &&
				m.material.uniforms.screenResolution) {
				m.material.uniforms.screenResolution.value = [size.width, size.height];
			}
		}

		if (mesh) {
			resizeMesh(mesh);
		} else {
			this._meshes.forEach(resizeMesh);
		}

		this._resizeArray.forEach(function(resizeFunc) {
			resizeFunc();
		});

		if (this.onresize) {
			this.onresize();
		}
	}.bind(this);

	this.load = function() {
		var promise;
		var promises = [];
		var size = this._parent._renderer.getSize(new THREE.Vector2());
		var self = this;
		var meshData;
		var self = this;

		if (this._loaded) return;
		this._loaded = true;

		var loadGLTFPromise = this.loadGLTF(self._parent._sources.gltf);
		promises.push(this.loadGLTF(self._parent._sources.gltf).then(function(gltf) {
			var toRemove = [];
			var toAdd = [];

			self._animations = gltf.animations;
			gltf.scene.traverse(function(el) {
				if (el.type === 'Mesh') {
					self._meshes.push(el);
					toAdd.push(el);
				} else if (el.type === 'Object3D') {
					toRemove.push(el);
				}
			});
			toRemove.forEach(function(el) {
				el.parent.remove(el);
			});
			toAdd.forEach(function(el) {
				self._group.add(el);
			});
		}));

		if (this._options.uniforms.tMatCapUnmasked) {
			var loadMatcapUnmaskedPromise = this.loadTextures(self._parent._sources.matcapUnmasked);
			promises.push(loadMatcapUnmaskedPromise.then(function(texture) {
				self._activePromises = self._activePromises.filter(p => p !== loadMatcapUnmaskedPromise);
				if (loadMatcapUnmaskedPromise._canceled) {
					return Promise.reject('Promise canceled');
				};

				self.updateUniforms({
					tMatCapUnmasked: texture
				});

				self._textures.tMatCapUnmasked = texture;

				return Promise.resolve();
			}));
		}

		if (this._options.uniforms.tMatCapMasked) {
			var loadMatcapMaskedPromise = this.loadTextures(self._parent._sources.matcapMasked);
			promises.push(loadMatcapMaskedPromise.then(function(texture) {
				self._activePromises = self._activePromises.filter(p => p !== loadMatcapMaskedPromise);
				if (loadMatcapMaskedPromise._canceled) {
					return Promise.reject('Promise canceled');
				};

				self.updateUniforms({
					tMatCapMasked: texture
				});

				self._textures.tMatCapMasked = texture;

				return Promise.resolve();
			}));
		}

		if (this._options.uniforms.uNormalMap) {
			var loadNormalPromise = this.loadTextures(self._parent._sources.normal);
			promises.push(loadNormalPromise.then(function(texture) {
				self._activePromises = self._activePromises.filter(p => p !== loadNormalPromise);
				if (loadNormalPromise._canceled) {
					return Promise.reject('Promise canceled');
				};

				self.updateUniforms({
					uNormalMap: texture
				});

				self._textures.uNormalMap = texture;

				return Promise.resolve();
			}));
		}

		return new Promise(function(resolve, reject) {
			Promise.all(promises).then(function(results) {
				self.resize();
				// var cube = new THREE.Mesh(
				// 	new THREE.BoxGeometry(100, 100, 100),
				// 	new THREE.MeshBasicMaterial({color: 0x00ff00})
				// );
				// self._group.add(cube);

				self._meshes.forEach(function(m) {
					var size = self._parent._renderer.getSize(new THREE.Vector2);
					var material = new THREE.ShaderMaterial({
						uniforms: Object.assign({
						}, self._options.uniforms),
						vertexShader: self._options.vertexShader || simpleVertexShader,
						fragmentShader: self._options.fragmentShader || simpleFragmentShader,
						transparent: true
					});

					if (material.uniforms.tMatCapUnmasked) {
						material.uniforms.tMatCapUnmasked.value = self._textures.tMatCapUnmasked;
					}

					if (material.uniforms.tMatCapMasked) {
						material.uniforms.tMatCapMasked.value = self._textures.tMatCapMasked;
					}

					if (material.uniforms.uNormalMap) {
						material.uniforms.uNormalMap.value = self._textures.uNormalMap;
					}
					

					material.extensions.derivatives = true;
					m.material = material;
				});

				self._parent._loopArray.push(self.loop);
				self._parent._resizeArray.push(self.resize);

				self.onload();

				dispatcher.dispatch({
					type: 'stage-group:ready'
				});

				resolve();
			}).catch(function(r) {
				reject(r);
			});
		});
	}.bind(this);

	this.unload = function() {
		var self = this;
		var group = this._group;

		if (!this._loaded) return;

		this._loaded = false;

		this._parent._loopArray.remove(this.loop);
		this._parent._resizeArray.remove(this.resize);
		this.onunload();

		return Promise.resolve();
	}.bind(this);

	this.cancelLoad = function() {
		this._activePromises.forEach(function(p) {
			p._canceled = true;
		});
	}.bind(this);

	this.show = function() {
		this.onshow();
	}.bind(this);

	this.hide = function() {
		this.onhide();
	}.bind(this);

	this.rebuild = function() {
		this.onrebuild();
	}.bind(this);

	this.loop = function() {
		if (this._destruct) return;

		this._loopArray.forEach(function(loopFunc) {
			loopFunc();
		});

		this.onloop();
	}.bind(this);
}

export default MeshDecorator;