import resizeStore from '../../resize/resize.store.js';
import { transition } from '../../utils/animation.utils.js';

const name = 'animation'; // указать дефолтное имя декоратора

if (name === null) {
	console.error('decorator name is missing');
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this.onSlideScroll = this.onSlideScroll.bind(this);
		this._currentType = 'normal';
	}

	onSlideScroll(data) {
		let self = this;
		let newIndex = data.newIndex;

		let duration = data.duration;

		let ease = window._vars.ease.css.ease;
		let easeIn = window._vars.ease.css.easeIn;
		let easeOut = window._vars.ease.css.easeOut;

		let id = this._parent._options.id;

		this._slides = this._parent._slides;

		let newSlideType = 'normal';

		if (newIndex !== undefined && this._slides[newIndex]) {
			let newSlide = this._slides[newIndex];
			let bgImgBlocks = newSlide.getElementsByClassName('bg-img-block');
			let bgColorBlocks = newSlide.getElementsByClassName(
				'bg-color-block'
			);

			if (bgImgBlocks.length) {
				newSlideType = 'img';
			} else if (bgColorBlocks.length) {
				newSlideType = 'dark';
			}
		}

		this._slides.forEach(function (slide, index) {
			slide.classList.remove('slide-previous');
			slide.classList.remove('slide-current');
			slide.classList.remove('slide-next');

			let textBlocks = slide.getElementsByClassName('text-block');
			let imgBlock = slide.getElementsByClassName('img-block');
			let bgImgBlocks = slide.getElementsByClassName('bg-img-block');
			let innerBlocks = slide.getElementsByClassName('bg-inner');
			let bgColorBlocks = slide.getElementsByClassName('bg-color-block');

			if (index > newIndex) {
				slide.classList.add('slide-next');

				transition(
					textBlocks,
					duration * 0.6,
					{
						opacity: 0,
						transform: 'translateY(200px)',
					},
					{
						ease: ['ease', easeIn],
					}
				);

				transition(
					imgBlock,
					duration * 0.6,
					{
						opacity: 0,
						transform: 'translateY(400px)',
					},
					{
						ease: ['ease', easeIn],
					}
				);

				transition(
					bgImgBlocks,
					duration * 0.75,
					{
						transform: 'translateY(100vh)',
					},
					{
						ease: easeIn,
					}
				);

				transition(
					innerBlocks,
					duration * 0.75,
					{
						transform: 'translateY(-80vh)',
					},
					{
						ease: easeIn,
					}
				);

				if (newSlideType === 'img') {
					transition(
						bgColorBlocks,
						duration * 0.75,
						{
							opacity: 0,
						},
						{
							ease: ease,
							delay: 1 * duration,
						}
					);
				} else {
					transition(
						bgColorBlocks,
						duration * 0.75,
						{
							opacity: 0,
						},
						{
							ease: ease,
						}
					);
				}
			} else if (index < newIndex) {
				slide.classList.add('slide-previous');

				transition(
					textBlocks,
					duration * 0.6,
					{
						opacity: 0,
						transform: 'translateY(-200px)',
					},
					{
						ease: ['ease', easeIn],
					}
				);

				transition(
					imgBlock,
					duration * 0.6,
					{
						opacity: 0,
						transform: 'translateY(-400px)',
					},
					{
						ease: ['ease', easeIn],
					}
				);

				transition(
					bgImgBlocks,
					duration * 0.75,
					{
						transform: 'translateY(-100vh)',
					},
					{
						ease: easeIn,
					}
				);

				transition(
					innerBlocks,
					duration * 0.75,
					{
						transform: 'translateY(80vh)',
					},
					{
						ease: easeIn,
					}
				);

				if (newSlideType === 'img') {
					transition(
						bgColorBlocks,
						duration * 0.75,
						{
							opacity: 0,
						},
						{
							ease: ease,
							delay: 1 * duration,
						}
					);
				} else {
					transition(
						bgColorBlocks,
						duration * 0.75,
						{
							opacity: 0,
						},
						{
							ease: ease,
						}
					);
				}

				transition(
					bgColorBlocks,
					duration * 0.6,
					{
						opacity: 0,
					},
					{
						ease: ease,
					}
				);
			} else {
				slide.classList.add('slide-current');

				transition(
					textBlocks,
					duration,
					{
						opacity: 1,
						transform: 'translateY(0px)',
					},
					{
						ease: ['ease', easeOut],
						delay: 0.4,
					}
				);

				transition(
					imgBlock,
					duration,
					{
						opacity: 1,
						transform: 'translateY(0px)',
					},
					{
						ease: ['ease', easeOut],
						delay: 0.4,
					}
				);

				transition(
					innerBlocks,
					duration * 0.9,
					{
						transform: 'translateY(0px)',
					},
					{
						ease: easeOut,
					}
				);

				transition(
					bgImgBlocks,
					duration * 0.9,
					{
						transform: 'translateY(0px)',
					},
					{
						ease: easeOut,
					}
				);

				let bgDuration =
					self._currentType === 'img' ? 0 : duration * 0.9;
				transition(
					bgColorBlocks,
					bgDuration,
					{
						opacity: 1,
					},
					{
						ease: ease,
					}
				);
			}
		});

		self._currentType = newSlideType;

		if (isFinite(this._parent._options.shift)) {
			transition(
				this._parent._wrapper,
				duration,
				{
					transform: `translateY(${
						-this._parent._options.shift * newIndex
					}px)`,
				},
				{
					ease: ease,
				}
			);
		} else {
			let wh = resizeStore.getData().height;
			transition(
				this._parent._wrapper,
				duration,
				{
					transform: `translateY(${-wh * newIndex}px)`,
				},
				{
					ease: ease,
				}
			);
		}
	}

	init() {
		this._id = this._parent._options.id;
		this.onSlideScroll({
			duration: 0,
			appearDelay: 0,
			newIndex: this._parent._index,
		});
		this._parent._slideScrollArray.push(this.onSlideScroll);
	}

	destroy() {
		this._parent._slideScrollArray.remove(this.onSlideScroll);
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};
