import resizeStore from '../../resize/resize.store.js';
import { transition } from '../../utils/animation.utils.js';

const name = 'animation'; // указать дефолтное имя декоратора

if (name === null) {
	console.error('decorator name is missing');
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this.onSlideScroll = this.onSlideScroll.bind(this);
	}

	onSlideScroll(data) {
		let self = this;
		let newIndex = data.newIndex;

		let duration = data.duration;
		let opacityDuration = duration * 0.6;

		let easeing = window._vars.ease.css.ease;
		let easeIn = window._vars.ease.css.easeIn;
		let easeOut = window._vars.ease.css.easeOut;

		this._slides = this._parent._slides;

		this._slides.forEach(function (slide, index) {
			slide.classList.remove('slide-previous');
			slide.classList.remove('slide-current');
			slide.classList.remove('slide-next');

			if (index > newIndex) {
				slide.classList.add('slide-next');
				transition(
					slide,
					[opacityDuration, duration],
					{
						opacity: 0,
						transform: 'translateY(200px)',
					},
					{
						ease: ['ease', easeIn],
					}
				);
			} else if (index < newIndex) {
				slide.classList.add('slide-previous');
				transition(
					slide,
					[opacityDuration, duration],
					{
						opacity: 0,
						transform: 'translateY(-200px)',
					},
					{
						ease: ['ease', easeIn],
					}
				);
			} else {
				slide.classList.add('slide-current');

				transition(
					slide,
					[opacityDuration, duration],
					{
						opacity: 1,
						transform: 'translateY(0px)',
					},
					{
						ease: ['ease', easeOut],
						delay: appearDelay || 0,
					}
				);
			}
		});

		if (isFinite(this._parent._options.shift)) {
			transition(
				this._parent._wrapper,
				duration,
				{
					transform: `translateY(${
						-this._parent._options.shift * newIndex
					}px)`,
				},
				{
					ease: easeing,
				}
			);
		} else {
			let wh = resizeStore.getData().height;
			transition(
				this._parent._wrapper,
				duration,
				{
					transform: `translateY(${-wh * newIndex}px)`,
				},
				{
					ease: easeing,
				}
			);
		}
	}

	init() {
		this._id = this._parent._options.id;
		this.onSlideScroll({
			duration: 0,
			appearDelay: 0,
			newIndex: this._parent._index,
		});
		this._parent._slideScrollArray.push(this.onSlideScroll);
	}

	destroy() {
		this._parent._slideScrollArray.remove(this.onSlideScroll);
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};
