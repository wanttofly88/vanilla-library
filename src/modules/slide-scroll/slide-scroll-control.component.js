import dispatcher from '../dispatcher.js';
import store from './slide-scroll.store.js';

var elementProto = Object.create(HTMLButtonElement.prototype);

// <slide-scroll-control data-id="id" data-to="next"></slide-scroll-control>
// компонент для управления слайдером
// data-to возможные значения: next, prev, номер слайда


var _handleStore = function() {
	var storeData = store.getData().items[this._id];
	var self = this;
	var timeForBlocking;

	if (!storeData || storeData.index === this._index) return;

	timeForBlocking = storeData.timeForBlocking || 800;

	this._index = storeData.index;
	this._animating = true;

	setTimeout(function() {
		self._animating = false;
	}, timeForBlocking);

	if (this._to === 'prev') {
		if (this._index === 0) {
			this.classList.add('inactive');
		} else {
			this.classList.remove('inactive');
		}
	} else if (this._to === 'next') {
		if (this._index === storeData.total - 1) {
			this.classList.add('inactive');
		} else {
			this.classList.remove('inactive');
		}
	}
}
var _handleClick = function() {
	var setFocus;
	var target;

	if (this._animating) return;

	target = this.getAttribute('data-target');
	if (target) {
		target = this.querySelector(target);
	}
	if (!target) {
		target = this;
	}

	dispatcher.dispatch({
		type: 'slide-scroll:click',
		element: target,
		originalTarget: this
	});

	setFocus = this.getAttribute('data-set-focus') || 'true';
	if (setFocus === 'false') setFocus = false;

	if (this._to === 'next') {
		dispatcher.dispatch({
			type: 'slide-scroll:direction',
			direction: 'down',
			id: this._id,
			setFocus: setFocus
		});
	} else if (this._to === 'prev') {
		dispatcher.dispatch({
			type: 'slide-scroll:direction',
			direction: 'up',
			id: this._id,
			setFocus: setFocus
		});
	} else {
		dispatcher.dispatch({
			type: 'slide-scroll:to',
			index: parseInt(this._to),
			id: this._id,
			setFocus: setFocus
		});
	}
}

class ElementClass extends HTMLButtonElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._animating = false;
		this._index = null;

		this.handleStore = _handleStore.bind(this);
		this.handleClick = _handleClick.bind(this);
	}
	connectedCallback() {
		this._id = this.getAttribute('data-id');
		if (!this._id) {
			console.warn('data-id attribute is missing on slide-scroll-control');
		}
		this._to = this.getAttribute('data-to');
		if (!this._to) {
			console.warn('data-to attribute is missing on slide-scroll-control');
		}

		this.addEventListener('click', this.handleClick);
		store.subscribe(this.handleStore);
		this.handleStore();
	}
	disconnectedCallback() {
		this.removeEventListener('click', this.handleClick);
		store.unsubscribe(this.handleStore);
	}
}

customElements.define('slide-scroll-control', ElementClass, {extends: 'button'});

export default ElementClass;