import dispatcher from '../dispatcher.js';
import store from './slide-scroll.store.js';

// <slide-scroll-total data-id="id"></slide-scroll-index>
// выводит общее количество слайдов

elementProto.handleStore = function() {
	var storeData = store.getData().items[this._id];
	if (!storeData) return;

	this.innerHTML = storeData.total;
}

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this.handleStore = this.handleStore.bind(this);
	}
	connectedCallback() {
		this._id = this.getAttribute('data-id');
		if (!this._id) {
			console.warn('data-id attribute is missing on slide-scroll-total');
		}
		store.subscribe(this.handleStore);
		this.handleStore();
	}
	disconnectedCallback() {
		store.unsubscribe(this.handleStore);
	}
}

customElements.define('slide-scroll-total', ElementClass);

export default ElementClass;