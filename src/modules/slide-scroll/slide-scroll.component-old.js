import dispatcher from '../dispatcher.js';
import slideStore from '../slide-scroll/slide-scroll.store.js';
import resizeStore from '../resize/resize.store.js';
import scrollStore from '../scroll/scroll.store.js';
import popupStore from '../popup/popup.store.js';
import pageLoadStore from '../page-load/page-load.store.js';
import WheelHandler from '../utils/WheelHandler.js';
import VerticalSwipe from '../utils/VerticalSwipe.js';
import addressStore from '../addressbar/addressbar.store.js';

var pageWrapper = document.getElementsByClassName('page-wrapper')[0];

// <slide-scroll 
//		data-id="id" 
//		data-min-heights="320:600;640:600;1000:1000;" 
//		data-popup-id="popup-1"
//		data-shift="150">
//		<div class="s-wrapper">
// 			<div class="s-slide">...</div>
//			<div class="s-slide">...</div>
//			...
//		</div>
// </slide-scroll>

//	slide-scroll {
// 		display: block;
// 		position: relative;
//	}
//	slide-scroll.slide-mode {
//		height: 100vh;
//		overflow: hidden;
//	}
//	slide-scroll.slide-mode .s-wrapper {
//		height: 100vh;
//	}

// в store-е задается timeForBlocking и animationSpeed (время анимации и время блокировки событий)

// data-id id элемента который будет сохранен в store

// data-min-heights минимальные высоты экрана в формате 320:500;640:100; итд 
// где 1-е число в группе - ширина экрана, 2-е - минимальная высота

// data-popup-id если указан, то id попапа будет означать что скролл работает только если открыт попап с этим id
// если указан 'none' то скролл будет работать только если все попапы закрыты

// data-shift - вертекальное смещение слайдов при скролле. если не выставлен, то будет смещаться на высоту экрана

// внизу в attachedCallback и detachedCallback подписка и отписка на store-ы. если хочется оменять или у store-ов другое апи
// не забывать отписываться от сторов только, иначе будет потенциальная утечка памяти

var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
	navigator.userAgent && !navigator.userAgent.match('CriOS');

var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

var idName = 'slide-scroll-';
var idNum  = 1;

var fireResize = function() {
	var evt = document.createEvent("HTMLEvents");
	evt.initEvent('resize', true, false);
	window.dispatchEvent(evt);
}

var translate = function(element, position, speed) {
	element.style.transitionDuration = speed + 'ms';
	element.style.transform = 'translateY(' + position + 'px) translateZ(0)';
}

var KeyboardHandler = function(component) {
	this.component = component;
	this.focusable = [];

	this.onKeyDown = function(e) {
		var keyCode = e.which;
		var self = this;
		var activeElement;
		var activeIndex;
		var activeSlide;

		if (keyCode === 9) { // Tab
			activeElement = document.activeElement;

			if (!activeElement) {
				activeIndex = 0;
			} else {
				activeIndex = Array.prototype.indexOf.call(this.focusable, activeElement);
				if (e.shiftKey) {
					activeIndex--;
				} else {
					activeIndex++;
				}

				if (activeIndex < 0) {
					activeIndex = this.focusable.length - 1;
				} else if (activeIndex > this.focusable.length - 1) {
					activeIndex = 0;
				}

				activeElement = this.focusable[activeIndex];

				if (!activeElement) return;
				activeSlide = activeElement.closest('.s-slide');

				if (!activeSlide) return;
				activeSlide = Array.prototype.indexOf.call(this.component._slides, activeSlide);

				dispatcher.dispatch({
					type: 'slide-scroll:to',
					id: this.component._id,
					index: activeSlide,
					setFocus: this._setFocus
				});
			}

			setTimeout(function() {
				if (pageWrapper.scrollTo) {
					pageWrapper.scrollTo(0, 0);
				}
				if (self.component.scrollTo) {
					self.component.scrollTo(0, 0);
				}				
			}, 0);
		}

		if (keyCode === 17) { // Ctrl
			this.component._ctrl = true;
		}

		if (keyCode === 38 || keyCode === 33) {
			dispatcher.dispatch({
				type: 'slide-scroll:direction',
				id: this.component._id,
				direction: 'up'
			});
		} else if (keyCode === 40 || keyCode === 34) {
			dispatcher.dispatch({
				type: 'slide-scroll:direction',
				id: this.component._id,
				direction: 'down'
			});
		}
	}.bind(this);

	this.onKeyUp = function() {
		this.component._ctrl = false;
	}.bind(this);

	this.update = function() {
		// if (isSafari || isIOS) { // safari is bad news....
			this.focusable = document.querySelectorAll('button, a, input, select, textarea');
		// } else {
		// 	this.focusable = document.querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"]');
		// }
	}.bind(this);

	this.attach = function() {
		document.addEventListener('keydown', this.onKeyDown);
		document.addEventListener('keyup', this.onKeyUp);

		this.update();
	}

	this.detach = function() {
		document.removeEventListener('keydown', this.onKeyDown);
		document.removeEventListener('keyup', this.onKeyUp);
	}
}

var _handleStore = function() {
	var storeData = slideStore.getData().items[this._id];
	var nativeScroll = slideStore.getData().nativeScroll;
	var wh = window.innerHeight;
	var self = this;
	var animationSpeed;
	var appearDelay;
	var focusTarget;
	var activeSlide;
	var prevIndex = this._index;

	this._index = storeData.index;
	this._isScrolling = true;

	if (this.onSlide) { // хук для оберток
		this.onSlide();
	}

	activeSlide = this._slides[this._index];

	if (prevIndex > storeData.index) {
		pageWrapper.classList.add('slide-direction-up');
		pageWrapper.classList.remove('slide-direction-down');
		this._direction = 'up';
	} else {
		pageWrapper.classList.remove('slide-direction-up');
		pageWrapper.classList.add('slide-direction-down');
		this._direction = 'down';
	}

	animationSpeed = slideStore.getData().animationSpeed;
	appearDelay = slideStore.getData().appearDelay;

	setTimeout(function() {
		self._isScrolling = false;
	}, slideStore.getData().timeForBlocking);

	if (this._active) {
		this._slides.forEach(function(slide, index) {
			if (index > storeData.index) {
				slide.classList.remove('slide-delayed');
				slide.classList.remove('slide-previous');
				slide.classList.remove('slide-current');
				slide.classList.add('slide-next');
			} else if (index < storeData.index) {
				slide.classList.remove('slide-delayed');
				slide.classList.remove('slide-next');
				slide.classList.remove('slide-current');
				slide.classList.add('slide-previous');
			} else {
				if (slide.getAttribute('id') && self._type === 'main') {
					dispatcher.dispatch({
						type: 'hash:set',
						hash: slide.getAttribute('id')
					});
				}
				if (appearDelay) {
					clearTimeout(self._appearDelayTo);
					if (appearDelay < 0) appearDelay = 0;

					self._appearDelayTo = setTimeout(function() {
						slide.classList.add('slide-delayed');
						slide.classList.remove('slide-next');
						slide.classList.remove('slide-previous');
						slide.classList.add('slide-current');
					}, appearDelay);
				} else {
					slide.classList.remove('slide-delayed');
					slide.classList.remove('slide-next');
					slide.classList.remove('slide-previous');
					slide.classList.add('slide-current');
				}
			}
		});

		if (this._shift) {
			translate(this._wrapper, -this._shift * this._index, animationSpeed);
		} else if (this._shift === undefined || this._shift === null) {
			translate(this._wrapper, -wh * this._index, animationSpeed);
		}

		if (storeData.setFocus) {
			focusTarget = activeSlide.querySelector('.focus-target');
			if (!focusTarget) {
				//if (isSafari || isIOS) {
					focusTarget = activeSlide.querySelector('button, input, select, textarea');
				//} else {
				//	focusTarget = activeSlide.querySelector('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"]');
				//}
			}
			if (focusTarget) {
				focusTarget.focus();
				if (pageWrapper.scrollTo) {
					pageWrapper.scrollTo(0, 0);
					requestAnimationFrame(function() {
						pageWrapper.scrollTo(0, 0);
					});
				}
				if (this.scrollTo) {
					requestAnimationFrame(function() {
						self.scrollTo(0, 0);
					});
				}
			}
		}
	} else {
		if (nativeScroll) return;

		dispatcher.dispatch({
			type: 'scroll:to',
			element: activeSlide,
			speed: 0.45
		});
	}
}

var _handleScroll = function(e) {
	var scrolled = window.scrollY;
	var maxIndex = 0;

	if (this._active === true) return;
	// if (self._isScrolling) return;

	this._slides.forEach(function(slide, index) {
		if (scrolled + 50 >= slide.offsetTop) {
			maxIndex = index;
		}
	});

	if (maxIndex === this.index) return;
	this._index = maxIndex;

	dispatcher.dispatch({
		type: 'slide-scroll:to',
		id: this._id,
		index: maxIndex,
		native: true
	});
}

var _handleResize = function() {
	var self = this;
	var storeData = slideStore.getData().items[this._id];
	var wh = window.innerHeight;
	var ww = window.innerWidth;

	var r = document.getElementsByTagName('html')[0];
	var minHeight;

	if (!storeData) {
		console.warn('slide-scroll store error');
		return;
	}

	if (this._minHeights) {
		minHeight = this._minHeights.reduce(function(prev, cur) {
			return cur.bp < ww ? cur: prev;
		});

		this._minHeight = minHeight;
		if (wh < minHeight.h && this._active !== false) {
			this.deactivate();
		} else if (wh >= minHeight.h && this._active !== true) {
			this.activate();
		}
	} else if (!this._active) {
		this.activate();
	}

	if (this._active) {
		this._slides.forEach(function(slide, index) {
			slide.style.position = 'absolute';
			slide.style.height = '100%';
			slide.style.width = '100%';
			if (self._shift !== undefined) {
				slide.style.top = self._shift*index + 'px';
			} else {
				slide.style.top = wh*index + 'px';
			}
		});
		if (this._shift !== undefined) {
			translate(this._wrapper, -this._shift*storeData.index, 0);
		} else {
			translate(this._wrapper, -wh*storeData.index, 0);
		}
	} else {
		this._slides.forEach(function(slide, index) {
			slide.style.position = null;
			slide.style.height = null;
			slide.style.width = null;
			slide.style.top = null;
		});
	}
}

var _handlePopup = function() {
	var active = popupStore.getData().active;

	if (!this._popupId) return;
	if (!this._active) return;

	if (this._popupId === 'none') {
		if (!active) {
			this.setInputHandlers();
		} else {
			this.removeInputHandlers();
		}
	} else {
		if (active === this._popupId) {
			this.setInputHandlers();
		} else {
			this.removeInputHandlers();
		}
	}
}

var _handleDispatcher = function(e) {
	var foundIndex;
	var found = false;

	if (e.type === 'scroll:to' && e.hasOwnProperty('hash')) {
		this._slides.forEach(function(slide, index) {
			if (slide.id === e.hash) {
				found = true;
				foundIndex = index;
			}
		})

		if (found) {
			dispatcher.dispatch({
				type: 'slide-scroll:to',
				id: this._id,
				index: foundIndex,
				setFocus: false
			});
		}
	}
}

var _activate = function() {
	var self = this;
	var wh = window.innerHeight;

	if (this._active === true) return;
	this._active = true;

	this.setInputHandlers();
	this.classList.add('slide-mode');
	this.classList.remove('scroll-mode');
	pageWrapper.classList.add('slide-scroll-active');

	this.scrollTop = 0;
	if (this.scrollTo) {
		this.scrollTo(0, 0);
	}

	this.handleResize();
	this.handleStore();
	setTimeout(fireResize, 20);

	dispatcher.dispatch({
		type: 'slide-scroll:enable',
		id: this._id
	});
}

var _deactivate = function() {
	var self = this;

	if (this._active === false) return;
	this._active = false; 

	this.removeInputHandlers();
	this.classList.remove('slide-mode');
	this.classList.add('scroll-mode');
	pageWrapper.classList.remove('slide-scroll-active');

	this._slides.forEach(function(slide, index) {
		slide.style.position = 'relative';
		if (self._minHeight) {
			slide.style.height = self._minHeight + 'px';
		} else {
			slide.style.height = '';
		}
		slide.style.top = '0px';
		slide.classList.remove('slide-next');
		slide.classList.remove('slide-previous');
	});

	translate(this._wrapper, 0, 0);
	setTimeout(fireResize, 20);

	dispatcher.dispatch({
		type: 'slide-scroll:disable',
		id: this._id
	});
}

var _setInputHandlers = function() {
	var self = this;

	this._hadlersSet = true;
	this.touchHandler.attach();
	this.wheelHandler.attach();
	this.keyboardHandler.attach();

	this.wheelHandler.handle = function(direction) {
		var blocked = slideStore.getData().isBlocked;
		var interactive = pageLoadStore.getData().interactive;
		if (blocked || !interactive) return;

		dispatcher.dispatch({
			type: 'slide-scroll:direction',
			id: self._id,
			direction: direction,
			setFocus: this._setFocus
		});
	}

	this.touchHandler.move = function(move) {
		var wh = self.clientHeight;
		var blocked = slideStore.getData().isBlocked;
		var interactive = pageLoadStore.getData().interactive;

		if (blocked || !interactive) return;

		if (self._shift !== undefined) {
			translate(self._wrapper, -self._shift * self._index + move, 0);
		} else {
			translate(self._wrapper, -wh * self._index + move, 0);
		}
	}

	this.touchHandler.cancel = function(returnSpeed) {
		var wh = self.clientHeight;

		if (self._shift !== undefined) {
			translate(self._wrapper, -self._shift * self._index, returnSpeed);
		} else {
			translate(self._wrapper, -wh * self._index, returnSpeed);
		}
	}

	this.touchHandler.handle = function(direction) {
		var blocked = slideStore.getData().isBlocked;
		var interactive = pageLoadStore.getData().interactive;

		if (blocked || !interactive) {
			self.touchHandler.cancel();
			return;
		};

		dispatcher.dispatch({
			type: 'slide-scroll:direction',
			id: self._id,
			direction: direction,
			setFocus: self._setFocus
		});
	}

}

var _removeInputHandlers = function() {
	this._hadlersSet = false;
	this.touchHandler.detach();
	this.wheelHandler.detach();
	this.keyboardHandler.detach();
}

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._ctrl = false;
		this._active = null;
		this.handleStore  = _handleStore.bind(this);
		this.handleResize = _handleResize.bind(this);
		this.handleScroll = _handleScroll.bind(this);
		this.handlePopup = _handlePopup.bind(this);
		this.handleDispatcher = _handleDispatcher.bind(this);
		this.touchHandler  = new VerticalSwipe(this, true);
		this.wheelHandler  = new WheelHandler();
		this.keyboardHandler  = new KeyboardHandler(this);
		this.activate   = _activate.bind(this);
		this.deactivate = _deactivate.bind(this);
		this.setInputHandlers = _setInputHandlers.bind(this);
		this.removeInputHandlers = _removeInputHandlers.bind(this);
		this._index = 0;
		this._hadlersSet = false;
	}
	connectedCallback() {
		var self = this;
		this._slides = Array.prototype.slice.call(this.getElementsByClassName('s-slide'));
		this._active = undefined;
		this._wrapper = this.getElementsByClassName('s-wrapper')[0];
		this._total = this._slides.length;
		this._type = this.getAttribute('data-type');

		this._setFocus = this.getAttribute('data-set-focus') || true;
		if (this._setFocus === 'false') {
			this._setFocus = false;
		}

		this._id = this.getAttribute('data-id') || (idName + (idNum++));
		this.setAttribute('data-id', this._id);

		this._popupId = this.getAttribute('data-popup-id') || 'none';
		this._shift = this.getAttribute('data-shift');

		if (this._shift) {
			this._shift = parseInt(this._shift);
		} else {
			this._shift = undefined;
		}

		// if (Modernizr.touchevents) {
		// 	this._shift = 200;
		// }

		this._minHeights = this.getAttribute('data-min-heights');
		this._minHeights = this._minHeights ? this._minHeights.split(';').map(function(item) {
			var vals = item.split(':');
			return {
				bp: parseInt(vals[0]),
				h: vals[1] === 'inf' ? Infinity : parseInt(vals[1])
			}
		}): null;

		dispatcher.dispatch({
			type: 'slide-scroll:add',
			id: this._id,
			index: 0,
			total: this._total
		});

		this._slides.forEach(function(slide, index) {
			if (index > self._index) {
				slide.classList.add('slide-next');
			} else if (index < self._index) {
				slide.classList.add('slide-previous');
			} else {
				slide.classList.add('slide-current');
				if (slide.getAttribute('id') && self._type === 'main') {
					dispatcher.dispatch({
						type: 'hash:set',
						hash: slide.getAttribute('id')
					});
				}
			}
		});

		this.handleResize();
		this.handleScroll();
		this.handlePopup();

		scrollStore.subscribe(this.handleScroll);
		popupStore.subscribe(this.handlePopup);
		slideStore.subscribe(this.handleStore);
		resizeStore.subscribe(this.handleResize);
		dispatcher.subscribe(this.handleDispatcher);

		setTimeout(function() {
			self.classList.add('slide-scroll-ready');
		}, 20);
	}
	disconnectedCallback() {
		scrollStore.unsubscribe(this.handleScroll);
		popupStore.unsubscribe(this.handlePopup);
		slideStore.unsubscribe(this.handleStore);
		resizeStore.unsubscribe(this.handleResize);
		dispatcher.unsubscribe(this.handleDispatcher);

		this.deactivate();

		dispatcher.dispatch({
			type: 'slide-scroll:remove',
			id: this._id
		});
	}
}

customElements.define('slide-scroll', ElementClass);

export default ElementClass;