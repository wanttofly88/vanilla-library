import dispatcher from '../../dispatcher.js';
import { getFocusable } from '../../utils/dom.utils.js';

const name = 'keyboard'; // указать дефолтное имя декоратора

if (name === null) {
	console.error('decorator name is missing');
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this.focusable = [];

		this.onKeyDown = this.onKeyDown.bind(this);
		this.onKeyUp = this.onKeyUp.bind(this);
		this.update = this.update.bind(this);
		this.handleDispatcher = this.handleDispatcher.bind(this);
		this.setSlidesMode = this.setSlidesMode.bind(this);
		this.setScrollMode = this.setScrollMode.bind(this);
		this.enableSlidesControls = this.enableSlidesControls.bind(this);
		this.disableSlidesControls = this.disableSlidesControls.bind(this);
	}

	onKeyDown(e) {
		let keyCode = e.which;
		let self = this;
		let activeElement;
		let activeIndex;
		let activeSlide;

		let pageWrapper = document.getElementsByClassName('page-wrapper')[0];

		if (keyCode === 9) {
			// Tab
			activeElement = document.activeElement;

			if (!activeElement) {
				activeIndex = 0;
			} else {
				activeIndex = Array.prototype.indexOf.call(
					this.focusable,
					activeElement
				);
				if (e.shiftKey) {
					activeIndex--;
				} else {
					activeIndex++;
				}

				if (activeIndex < 0) {
					activeIndex = this.focusable.length - 1;
				} else if (activeIndex > this.focusable.length - 1) {
					activeIndex = 0;
				}

				activeElement = this.focusable[activeIndex];

				if (!activeElement) return;
				activeSlide = activeElement.closest('.s-slide');

				if (!activeSlide) return;
				activeSlide = Array.prototype.indexOf.call(
					this._parent._slides,
					activeSlide
				);
				if (activeSlide === -1) return;

				dispatcher.dispatch({
					type: 'slide-scroll:to',
					id: this._parent._options.id,
					index: activeSlide,
					setFocus: this._setFocus,
				});
			}

			setTimeout(function () {
				if (pageWrapper && pageWrapper.scrollTo) {
					pageWrapper.scrollTo(0, 0);
				}
				if (self._parent.scrollTo) {
					self._parent.scrollTo(0, 0);
				}
			}, 0);
		}

		if (keyCode === 17) {
			// Ctrl
			this._parent._ctrl = true;
		}

		if (keyCode === 38 || keyCode === 33) {
			if (this._parent._scrollUp === 'disabled') return;
			dispatcher.dispatch({
				type: 'slide-scroll:direction',
				id: this._parent._options.id,
				direction: 'up',
			});
		} else if (keyCode === 40 || keyCode === 34) {
			if (this._parent._scrollDown === 'disabled') return;
			dispatcher.dispatch({
				type: 'slide-scroll:direction',
				id: this._parent._options.id,
				direction: 'down',
			});
		}
	}

	onKeyUp() {
		this._parent._ctrl = false;
	}

	update() {
		this.focusable = getFocusable();
	}

	handleDispatcher(e) {
		if (
			e.type === 'content:replaced' ||
			e.type === 'focusable:change' ||
			e.type === 'focusable:add'
		) {
			this.update();
		}
	}

	enableSlidesControls() {
		document.addEventListener('keydown', this.onKeyDown);
		document.addEventListener('keyup', this.onKeyUp);
	}

	disableSlidesControls() {
		document.removeEventListener('keydown', this.onKeyDown);
		document.removeEventListener('keyup', this.onKeyUp);
	}

	setSlidesMode() {}

	setScrollMode() {}

	init() {
		this.update();
		dispatcher.subscribe(this.handleDispatcher);
	}

	destroy() {
		this.disableSlidesControls();
		dispatcher.unsubscribe(this.handleDispatcher);
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};
