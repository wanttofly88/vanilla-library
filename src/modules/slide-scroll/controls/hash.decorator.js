import dispatcher from '../../dispatcher.js';
import addressbarStore from '../../addressbar/addressbar.store.js';

const name = 'hash'; // указать дефолтное имя декоратора

if (name === null) {
	console.error('decorator name is missing');
}

const defaultOptions = {
	name: name,
}

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({
		}, defaultOptions, options);

		this.onSlideScroll = this.onSlideScroll.bind(this);
		this.handleDispatcher = this.handleDispatcher.bind(this);

		this.setSlidesMode = this.setSlidesMode.bind(this);
		this.setScrollMode = this.setScrollMode.bind(this);
		this.enableSlidesControls = this.enableSlidesControls.bind(this);
		this.disableSlidesControls = this.disableSlidesControls.bind(this);
	}

	onSlideScroll(data) {
		let activeSlide = this._parent._slides[data.newIndex];

		let hash = addressbarStore.hash;
		if (activeSlide.id && activeSlide.id !== hash) {
			dispatcher.dispatch({
				type: 'hash:set',
				hash: activeSlide.id
			});
		}
	}

	handleDispatcher(e) {
		let foundIndex;
		let found = false;

		if (e.type === 'scroll:to' && e.hasOwnProperty('hash')) {
			this._parent._slides.forEach(function(slide, index) {
				if (slide.id === e.hash) {
					found = true;
					foundIndex = index;
				}
			});

			if (found) {
				dispatcher.dispatch({
					type: 'slide-scroll:to',
					id: this._parent._options.id,
					index: foundIndex,
					setFocus: false
				});
			}
		}
	}

	enableSlidesControls() {
		if (!this._parent._options.hash) return;
		if (this._hashSubscribed) return;
		this._hashSubscribed = true;
		this._parent._slideScrollArray.push(this.onSlideScroll);
		dispatcher.subscribe(this.handleDispatcher);
	}

	disableSlidesControls() {
		if (!this._parent._options.hash) return;
		if (!this._hashSubscribed) return;
		this._hashSubscribed = false;
		this._parent._slideScrollArray.remove(this.onSlideScroll);
		dispatcher.unsubscribe(this.handleDispatcher);
	}

	setSlidesMode() {}

	setScrollMode() {}

	init() {
		let self= this;
		if (!this._parent._options.hash) return;

		let hash = addressbarStore.getData().hash;
		this._parent._slides.forEach((s, i) => {

			if (hash && s.id && s.id === hash) {
				// sets index before it adds into store
				self._parent._index = i;
			}
		});	
	}

	destroy() {
		this.disableSlidesControls();
		if (!this._parent._options.hash) return;
	}
}

let attach = function(parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {
	}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
}

let detach = function(parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
}

export default {
	attach: attach,
	detach: detach
}