import dispatcher from '../../dispatcher.js';
import slideStore from '../slide-scroll.store.js';
import { transition } from '../../utils/animation.utils.js';

const name = 'touch'; // указать дефолтное имя декоратора

if (name === null) {
	console.error('decorator name is missing');
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this._start = {};
		this._delta = {};
		this._horizontal = undefined;
		this._edge = false;

		this.setSlidesMode = this.setSlidesMode.bind(this);
		this.setScrollMode = this.setScrollMode.bind(this);
		this.ontouchstart = this.ontouchstart.bind(this);
		this.ontouchmove = this.ontouchmove.bind(this);
		this.ontouchend = this.ontouchend.bind(this);
		this.enableSlidesControls = this.enableSlidesControls.bind(this);
		this.disableSlidesControls = this.disableSlidesControls.bind(this);
	}

	ontouchstart(e) {
		var touches = e.touches[0];

		this.wh = this._parent.clientHeight;
		this.index = this._parent._index || 0;
		this.total = this._parent._total || 0;

		this._start = {
			x: touches.pageX,
			y: touches.pageY,
			time: +new Date(),
		};
		this._delta = {};
		this._horizontal = undefined;

		this._parent.addEventListener('touchmove', this.ontouchmove);
		this._parent.addEventListener('touchend', this.ontouchend);
	}

	ontouchmove(e) {
		var touches;
		var move = 0;
		var touchMoveEvent;

		if (e.touches.length > 1 || (e.scale && e.scale !== 1)) return;
		touches = event.touches[0];

		this.touches = event.touches[0];

		this._delta = {
			x: this.touches.pageX - this._start.x,
			y: this.touches.pageY - this._start.y,
		};

		if (typeof this._horizontal === undefined) {
			this._horizontal = !!(
				this._horizontal ||
				Math.abs(this._delta.y) < Math.abs(this._delta.x)
			);
		}

		if (this._horizontal) return;

		move = this._delta.y / 3;

		if (this.index || this.total) {
			if (this._delta.y > 0 && this.index <= 0) {
				this._edge = true;
			} else if (this._delta.y < 0 && this.index >= this.total - 1) {
				this._edge = true;
			} else {
				this._edge = false;
			}
		} else {
			this._edge = false;
		}

		if (this._edge) {
			move = move / 4;
		}

		if (move > 0) {
			if (this._parent._scrollDown === 'disabled') {
				move = 0;
			} else {
				move = Math.min(move, this._parent._options.shift);
			}
		} else {
			if (this._parent._scrollUp === 'disabled') {
				move = 0;
			} else {
				move = Math.max(move, -this._parent._options.shift);
			}
		}

		touchMoveEvent = new CustomEvent('touchshift', {
			detail: move,
		});
		this._parent.dispatchEvent(touchMoveEvent);

		if (this.move) {
			this.move(move);
		}
	}

	ontouchend(e) {
		var duration = +new Date() - this._start.time;
		var check =
			(parseInt(duration) < 250 && Math.abs(this._delta.y) > 20) ||
			Math.abs(this._delta.y) > 100;
		var returnSpeed = 0.25;
		var touchEndEvent;

		this._parent.removeEventListener('touchmove', this.ontouchmove, false);
		this._parent.removeEventListener('touchend', this.ontouchend, false);

		if (this._horizontal) return;

		if (check && !this._edge) {
			this.handle(this._delta.y > 0 ? 'up' : 'down');
		} else {
			if (this._edge) returnSpeed = 0.15;

			if (this.cancel) {
				this.cancel(returnSpeed);
			}

			touchEndEvent = new CustomEvent('touchcancel');
			this._parent.dispatchEvent(touchEndEvent);
		}
	}

	handle(direction) {
		let storeData = slideStore.getData().items[this._parent._options.id];
		let blocked = storeData.isBlocked;
		if (direction === 'up' && this._parent._scrollUp === 'disabled') return;
		if (direction === 'down' && this._parent._scrollDown === 'disabled')
			return;

		if (blocked) {
			this.cancel();
			return;
		}

		dispatcher.dispatch({
			type: 'slide-scroll:direction',
			id: this._parent._options.id,
			direction: direction,
			setFocus: this._parent._options.setFocus,
		});
	}

	move(move) {
		let wh = this._parent.clientHeight;
		let storeData = slideStore.getData().items[this._parent._options.id];
		let blocked = storeData.isBlocked;

		if (blocked) return;

		if (isFinite(this._parent._options.shift)) {
			transition(this._parent._wrapper, 0, {
				transform: `translateY(${
					-this._parent._options.shift * this._parent._index + move
				}px)`,
			});
		} else {
			transition(this._parent._wrapper, 0, {
				transform: `translateY(${-wh * this._parent._index + move}px)`,
			});
		}
	}

	cancel(returnSpeed) {
		let wh = this._parent.clientHeight;

		if (isFinite(this._parent._options.shift)) {
			transition(this._parent._wrapper, returnSpeed, {
				transform: `translateY(${
					-this._parent._options.shift * this._parent._index
				}px)`,
			});
		} else {
			transition(this._parent._wrapper, returnSpeed, {
				transform: `translateY(${-wh * this._parent._index}px)`,
			});
		}
	}

	enableSlidesControls() {
		this._parent.addEventListener('touchstart', this.ontouchstart);
	}

	disableSlidesControls() {
		this._parent.removeEventListener('touchstart', this.ontouchstart);
		this._parent.removeEventListener('touchmove', this.ontouchmove);
		this._parent.removeEventListener('touchend', this.ontouchend);
	}

	setSlidesMode() {}

	setScrollMode() {}

	init() {}

	destroy() {
		this.disableSlidesControls();
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};
