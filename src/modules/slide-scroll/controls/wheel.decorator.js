import dispatcher from "../../dispatcher.js";
import slideStore from "../slide-scroll.store.js";

const name = "wheel"; // указать дефолтное имя декоратора

if (name === null) {
	console.error("decorator name is missing");
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this._time = null;

		this._ctrl = false;
		this._isScrolling = false;

		this.resetBuffer = this.resetBuffer.bind(this);
		this.onWheel = this.onWheel.bind(this);
		this.handle = this.handle.bind(this);
		this.setSlidesMode = this.setSlidesMode.bind(this);
		this.setScrollMode = this.setScrollMode.bind(this);
		this.enableSlidesControls = this.enableSlidesControls.bind(this);
		this.disableSlidesControls = this.disableSlidesControls.bind(this);
	}

	resetBuffer() {
		this._scrollBuffer = [];
		for (var i = 0; i < 40; i++) {
			this._scrollBuffer.push(0);
		}
	}

	onWheel(e) {
		var value = e.wheelDelta || -e.deltaY || -e.detail;
		var direction = value > 0 ? "up" : "down";
		var previousTime;
		var summ1, summ2;
		var bufferOld, bufferNew;

		if (this._ctrl) return;
		if (
			Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDelta) ||
			Math.abs(e.deltaX) > Math.abs(e.deltaY)
		)
			return;

		previousTime = this._time;
		this._time = new Date().getTime();

		if (previousTime && this._time - previousTime > 300) {
			this.resetBuffer();
		}

		this._scrollBuffer.push(Math.abs(value));
		this._scrollBuffer.shift();

		if (this._isScrolling) return;

		bufferNew = this._scrollBuffer.slice(30, 40);
		bufferOld = this._scrollBuffer.slice(0, 30);

		summ1 = bufferNew.reduce(function (previousValue, currentValue) {
			return previousValue + currentValue;
		});
		summ2 = bufferOld.reduce(function (previousValue, currentValue) {
			return previousValue + currentValue;
		});

		summ1 = summ1 / 10;
		summ2 = summ2 / 30;

		if (summ1 > summ2) {
			this.handle(direction);
		}
	}

	handle(direction) {
		let storeData = slideStore.getData().items[this._parent._options.id];

		if (storeData.isBlocked) return;

		if (direction === "up" && this._parent._scrollUp === "disabled") return;
		if (direction === "down" && this._parent._scrollDown === "disabled")
			return;

		dispatcher.dispatch({
			type: "slide-scroll:direction",
			id: this._parent._options.id,
			direction: direction,
			setFocus: this._parent._options.setFocus,
		});
	}

	enableSlidesControls() {
		document.addEventListener("mousewheel", this.onWheel);
		document.addEventListener("wheel", this.onWheel);
	}

	disableSlidesControls() {
		document.removeEventListener("mousewheel", this.onWheel);
		document.removeEventListener("wheel", this.onWheel);
	}

	setSlidesMode() {}

	setScrollMode() {}

	init() {
		this.resetBuffer();
	}

	destroy() {
		this.disableSlidesControls();
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};