import dispatcher from '../../dispatcher.js';
import slideScrollStore from '../slide-scroll.store.js';

// для того чтобы прописывать уникальный функционал по data-id

const name = 'unique'; // указать дефолтное имя декоратора

if (name === null) {
    console.error('decorator name is missing');
}

const defaultOptions = {
    name: name,
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = Object.assign({}, defaultOptions, options);

        this.handleSlideStore = this.handleSlideStore.bind(this);
    }

    handleSlideStore() {
        this._id = this._parent._options.id;
        this._index = this._parent._index;

        if (this._id === 'stage-slides-secondry') {
            let stageScrollData = slideScrollStore.getData().items[
                'stage-slides'
            ];
            if (!stageScrollData) return;

            let newIndex = stageScrollData.index;

            if (newIndex <= 1) {
                newIndex = 0;
            } else if (newIndex <= 2) {
                newIndex = 2;
            } else if (newIndex <= 4) {
                newIndex = 3;
            } else {
                newIndex = 4;
            }

            // if (newIndex >= 2 && newIndex <= 4) {
            //  newIndex = 2;
            // } else if (newIndex > 4) {
            //  newIndex -= 2;
            // }

            // let newIndex = Math.max(0, stageScrollData.index - 1);

            if (newIndex !== this._index) {
                dispatcher.dispatch({
                    type: 'slide-scroll:to',
                    id: this._id,
                    index: newIndex,
                });
            }
        }
    }

    init() {
        if (this._parent._options.id === 'stage-slides-secondry') {
            let stageScrollData = slideScrollStore.getData().items[
                'stage-slides'
            ];
            if (stageScrollData) {
                this._parent._index = stageScrollData.index;
            }
        }
        slideScrollStore.subscribe(this.handleSlideStore);
    }

    destroy() {
        slideScrollStore.unsubscribe(this.handleSlideStore);
    }
}

let attach = function (parent, options) {
    let decorator;

    if (!parent._decorators) {
        parent._decorators = {};
    }

    options = Object.assign(defaultOptions, {}, options);

    if (parent._decorators[options.name]) {
        decorator = parent._decorators[options.name];
    } else {
        decorator = new Decorator(parent, options);
        parent._decorators[options.name] = decorator;
        decorator.init();
    }

    return decorator;
};

let detach = function (parent, options) {
    options = Object.assign(defaultOptions, options);

    if (!parent._decorators[options.name]) {
        console.error('no decorator with name "' + options.name + '"');
        return;
    }

    let decorator = parent._decorators[options.name];
    decorator.destroy();
};

export default {
    attach: attach,
    detach: detach,
};
