import dispatcher from '../../dispatcher.js';
import slideScrollStore from '../slide-scroll.store.js';
import popupStore from '../../popup/popup.store.js';

const name = 'popup'; // указать дефолтное имя декоратора

if (name === null) {
	console.error('decorator name is missing');
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this.handlePopup = this.handlePopup.bind(this);
	}

	handlePopup() {
		this._parent.checkControls();
	}

	init() {
		let popup = this._parent.closest('popup-component');
		if (popup) {
			this._popupId = popup.getAttribute('data-id');
			this._parent._controlsEnabledFlagsArray.push(() => {
				return popupStore.getData().active === this._popupId;
			});
		} else {
			this._parent._controlsEnabledFlagsArray.push(() => {
				return !popupStore.getData().active;
			});
		}
		this.handlePopup();
		popupStore.subscribe(this.handlePopup);
	}

	destroy() {
		popupStore.unsubscribe(this.handlePopup);
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};
