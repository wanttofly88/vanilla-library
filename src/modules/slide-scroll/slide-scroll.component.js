import dispatcher from "../dispatcher.js";
import { datasetToOptions } from "../utils/component.utils.js";
import { FunctionArray } from "../utils/array.utils.js";

import resizeStore from "../resize/resize.store.js";
import pageLoadStore from "../page-load/page-load.store.js";

import wheelDecorator from "./controls/wheel.decorator.js";
import touchDecorator from "./controls/touch.decorator.js";
import keyboardDecorator from "./controls/keyboard.decorator.js";
import hashDecorator from "./controls/hash.decorator.js";
import scrollableDecorator from "./slides/scrollable.decorator.js";
import scrollStore from "../scroll/scroll.store.js";
import slideStore from "./slide-scroll.store.js";
import uniqueDecorator from "./decorators/unique.decorator.js";
import popupDecorator from "./decorators/popup.decorator.js";

import { transition } from "../utils/animation.utils.js";
import { getFocusable } from "../utils/dom.utils.js";

import defaultAnimationDecorator from "./animations/default.animation.js";
import stageAnimationDecorator from "./animations/stage.animation.js";

const defaultOptions = {
	__namespace: "",
	id: "",
	animation: "default",
	disabled: false,
	hash: false,
	setFocus: true,
	minHeights: "",
	shift: Infinity,
	duration: 1,
};

const decorators = [
	wheelDecorator,
	touchDecorator,
	keyboardDecorator,
	scrollableDecorator,
	uniqueDecorator,
	popupDecorator,
	hashDecorator,
];

const animationDecorators = {
	default: defaultAnimationDecorator,
	stage: stageAnimationDecorator,
};

// <slide-scroll
//		data-id="id"
//		data-min-heights="320:600;640:600;1000:1000;"
//		data-popup-id="popup-1"
//		data-shift="150">
//		<div class="s-wrapper">
// 			<div class="s-slide">...</div>
//			<div class="s-slide">...</div>
//			...
//		</div>
// </slide-scroll>

// popup-component.slide-scroll-active,
// root.slide-scroll-active,
// root.slide-scroll-active body {
//		position: fixed;
//		overflow: hidden;
// }

//	slide-scroll {
// 		display: block;
// 		position: relative;
//	}
//	slide-scroll.slide-mode {
//		height: 100vh;
//		overflow: hidden;
//	}
//	slide-scroll.slide-mode .s-wrapper {
//		height: 100vh;
//	}

let _handleResize = function () {
	let ww = resizeStore.getData().width;
	let wh = resizeStore.getData().height;

	if (this._minHeights) {
		let minHeight = this._minHeights.reduce(function (prev, cur) {
			return cur.bp < ww ? cur : prev;
		}, this._minHeights[0]);

		if (wh < minHeight.h) {
			this._heightValid = false;
		} else if (wh >= minHeight.h) {
			this._heightValid = true;
		}
	} else {
		this._heightValid = true;
	}

	this._resizeArray.forEach((f) => f());

	this.checkScrollMode();
};

let _checkScrollMode = function () {
	let self = this;
	let wh = resizeStore.getData().height;
	let rootElement = this.closest("popup-component");

	if (!rootElement) rootElement = document.documentElement;

	if (this._slidesModeFlagsArray.boolReduce() && this._mode !== "slides") {
		this._mode = "slides";
		this.classList.remove("scroll-mode");
		this.classList.add("slide-mode");
		rootElement.classList.add("slide-scroll-active");

		this._slides.forEach(function (slide, index) {
			slide.style.position = "absolute";
			slide.style.height = "100%";
			slide.style.width = "100%";
			if (isFinite(self._options.shift)) {
				slide.style.top = self._options.shift * index + "px";
			} else {
				slide.style.top = wh * index + "px";
			}
		});

		if (isFinite(this._options.shift)) {
			transition(this._wrapper, 0, {
				transform: `translateY(${
					-this._options.shift * this._index
				}px)`,
			});
		} else {
			transition(this._wrapper, 0, {
				transform: `translateY(${-wh * this._index}px)`,
			});
		}

		if (this._decorators) {
			Object.values(this._decorators).map((d) =>
				d.setSlidesMode ? d.setSlidesMode() : null
			);
		}
	} else if (
		!this._slidesModeFlagsArray.boolReduce() &&
		this._mode !== "scroll"
	) {
		this._mode = "scroll";
		this.classList.remove("slide-mode");
		this.classList.add("scroll-mode");
		rootElement.classList.remove("slide-scroll-active");

		this._slides.forEach(function (slide, index) {
			slide.style.position = "";
			slide.style.height = "";
			slide.style.width = "";
			slide.style.top = "";
		});

		transition(this._wrapper, 0, {
			transform: "translateY(0px)",
		});

		if (this._decorators) {
			Object.values(this._decorators).map((d) =>
				d.setScrollMode ? d.setScrollMode() : null
			);
		}
	}

	this.checkControls();
};

let _checkControls = function () {
	if (
		this._controlsEnabledFlagsArray.boolReduce() &&
		this._controls !== "enabled"
	) {
		this._controls = "enabled";

		if (this._decorators) {
			Object.values(this._decorators).map((d) =>
				d.enableSlidesControls ? d.enableSlidesControls() : null
			);
		}
	} else if (
		!this._controlsEnabledFlagsArray.boolReduce() &&
		this._mode !== "disabled"
	) {
		this._controls = "disabled";

		if (this._decorators) {
			Object.values(this._decorators).map((d) =>
				d.disableSlidesControls ? d.disableSlidesControls() : null
			);
		}
	}

	if (this._scrollUpEnabledFlagsArray.boolReduce() && !this._scrollUpFlag) {
		this._scrollUpFlag = true;

		this._scrollUpTO = setTimeout(() => {
			this._scrollUp = "enabled";
		}, 300);
	} else if (
		!this._scrollUpEnabledFlagsArray.boolReduce() &&
		this._scrollUpFlag
	) {
		this._scrollUpFlag = false;

		clearTimeout(this._scrollUpTO);

		this._scrollUp = "disabled";
	}

	if (
		this._scrollDownEnabledFlagsArray.boolReduce() &&
		!this._scrollDownFlag
	) {
		this._scrollDownFlag = true;

		this._scrollDownTO = setTimeout(() => {
			this._scrollDown = "enabled";
		}, 300);
	} else if (
		!this._scrollDownEnabledFlagsArray.boolReduce() &&
		this._scrollDownFlag
	) {
		this._scrollDownFlag = false;

		clearTimeout(this._scrollDownTO);

		this._scrollDown = "disabled";
	}
};

let _handleLoad = function () {
	if (this._loaded) return;

	if (pageLoadStore.getData().loaded) {
		this._loaded = true;

		this.handleResize();
		resizeStore.subscribe(this.handleResize);

		this.handleStore();
		slideStore.subscribe(this.handleStore);

		this.checkScrollMode();
		this.checkControls();

		this.classList.add("slide-scroll-ready");
	}
};

let _handleStore = function () {
	let self = this;

	let storeData = slideStore.getData().items[this._options.id];
	if (!storeData) return;

	let nativeScroll = slideStore.getData().nativeScroll;
	let focusTarget;
	let activeSlide;
	let direction;

	if (this._index < storeData.index) {
		direction = "down";
	} else {
		direction = "up";
	}

	let oldIndex = this._index;
	this._index = storeData.index;

	activeSlide = this._slides[this._index];

	let animationData = {
		oldIndex: oldIndex,
		newIndex: this._index,
		direction: direction,
		duration: storeData.animationSpeed,
	};

	if (this._mode === "slides") {
		// установка фокуса на первы интерактивный элемент слайда
		if (storeData.setFocus) {
			focusTarget = activeSlide.querySelector(".focus-target");
			if (!focusTarget) {
				focusTarget = getFocusable(activeSlide)[0];
			}
			if (
				!focusTarget &&
				activeSlide.classList.contains("s-scrollable")
			) {
				activeSlide.children[0].setAttribute("tabindex", 0);
				focusTarget = activeSlide.children[0];
			}
			if (focusTarget) {
				focusTarget.focus({ preventScroll: true });

				requestAnimationFrame(function () {
					self.scrollTo(0, 0);
					self._wrapper.scrollTo(0, 0);
				});
			}
		}

		// все возможные хэндлеры
		this._slideScrollArray.forEach((f) => f(animationData));
	} else {
		if (nativeScroll) return;

		dispatcher.dispatch({
			type: "scroll:to",
			element: activeSlide,
			speed: 0.45,
		});
	}
};

class ElementClass extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init.call(self);
	}

	init() {
		this._options = datasetToOptions(this.dataset, defaultOptions);
		this._resizeArray = new FunctionArray();
		this._enableFlagsArray = new FunctionArray();
		this._slidesModeFlagsArray = new FunctionArray();
		this._controlsEnabledFlagsArray = new FunctionArray();
		this._scrollUpEnabledFlagsArray = new FunctionArray();
		this._scrollDownEnabledFlagsArray = new FunctionArray();
		this._slideScrollArray = new FunctionArray();

		this._loaded = false;
		this._controls = undefined;
		this._mode = undefined;
		this._heightValid = true;
		this._index = 0;

		this.handleResize = _handleResize.bind(this);
		this.handleLoad = _handleLoad.bind(this);
		this.checkControls = _checkControls.bind(this);
		this.checkScrollMode = _checkScrollMode.bind(this);
		this.handleStore = _handleStore.bind(this);
	}

	connectedCallback() {
		let self = this;

		this.classList.add("scroll-mode");

		this._minHeights = this._options.minHeights;
		this._minHeights = this._minHeights
			? this._minHeights.split(";").map(function (item) {
					if (!item) return;
					let vals = item.split(":");
					return {
						bp: parseInt(vals[0]),
						h: parseInt(vals[1]),
					};
			  })
			: null;

		this._slidesModeFlagsArray.push(() => {
			return self._heightValid;
		});

		this._controlsEnabledFlagsArray.push(() => {
			return self._mode === "slides";
		});

		this._controlsEnabledFlagsArray.push(() => {
			return self._options.disabled === false;
		});

		this._scrollUpEnabledFlagsArray.push(() => {
			return true;
		});

		this._scrollDownEnabledFlagsArray.push(() => {
			return true;
		});

		this._slides = Array.prototype.slice.call(
			this.getElementsByClassName("s-slide")
		);
		this._wrapper = this.getElementsByClassName("s-wrapper")[0];
		this._total = this._slides.length;

		pageLoadStore.subscribe(this.handleLoad);
		decorators.forEach((d) => d.attach(this));
		animationDecorators[this._options.animation].attach(this);

		dispatcher.dispatch({
			type: "slide-scroll:add",
			id: this._options.id,
			index: this._index,
			total: this._total,
			animationSpeed: this._options.duration,
		});
	}

	disconnectedCallback() {
		let rootElement = this.closest("popup-component");

		if (!rootElement) rootElement = document.documentElement;
		rootElement.classList.remove("slide-scroll-active");

		this._destruct = true;
		pageLoadStore.unsubscribe(this.handleLoad);

		if (this._loaded) {
			resizeStore.unsubscribe(this.handleResize);
		}

		animationDecorators[this._options.animation].detach(
			this,
			this._options
		);
		decorators.forEach((d) => d.detach(this, this._options));

		dispatcher.dispatch({
			type: "slide-scroll:remove",
			id: this._options.id,
		});
	}
}

customElements.define("slide-scroll", ElementClass);

export default ElementClass;