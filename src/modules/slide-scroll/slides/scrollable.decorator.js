import dispatcher from "../../dispatcher.js";
import slideScrollStore from "../slide-scroll.store.js";
import resizeStore from "../../resize/resize.store.js";
import { getFocusable } from "../../utils/dom.utils.js";

const name = "scrollableSlide"; // указать дефолтное имя декоратора

if (name === null) {
	console.error("decorator name is missing");
}

const defaultOptions = {
	name: name,
};

class Decorator {
	constructor(parent, options) {
		this._parent = parent;
		this._options = Object.assign({}, defaultOptions, options);

		this.onSlideScroll = this.onSlideScroll.bind(this);
		this.onScroll = this.onScroll.bind(this);
		this.onResize = this.onResize.bind(this);
		this.setSlidesMode = this.setSlidesMode.bind(this);
		this.setScrollMode = this.setScrollMode.bind(this);
		this.checkScrollUp = this.checkScrollUp.bind(this);
		this.checkScrollDown = this.checkScrollDown.bind(this);

		this._scrollableSlides = [];
	}

	onScroll() {
		this._parent.checkControls();
	}

	onResize() {
		this._scrollableSlides.forEach((s) => {
			s._scrollHeight = s.scrollHeight;
		});

		this._parent.checkControls();
	}

	checkScrollUp() {
		if (!slideScrollStore.getData().items[this._parent._options.id]) return;

		let index = slideScrollStore.getData().items[this._parent._options.id]
			.index;
		let found = this._scrollableSlides.find((s) => {
			return s._index === index;
		});
		if (found) {
			let scrolled = found.scrollTop;

			return scrolled <= 0;
		} else {
			return true;
		}
	}

	checkScrollDown() {
		if (!slideScrollStore.getData().items[this._parent._options.id]) return;

		let index = slideScrollStore.getData().items[this._parent._options.id]
			.index;
		let found = this._scrollableSlides.find((s) => {
			return s._index === index;
		});
		if (found) {
			let wh = resizeStore.getData().height;
			let scrolled = found.scrollTop;

			return scrolled >= found._scrollHeight - wh;
		} else {
			return true;
		}
	}

	onSlideScroll(data) {
		let self = this;
		if (this._parent._mode !== "slides") return;

		if (!slideScrollStore.getData().items[this._parent._options.id]) return;

		let index = slideScrollStore.getData().items[this._parent._options.id]
			.index;
		this._scrollableSlides.forEach((s) => {
			if (s._index === index) {
				if (s._overflowTimeout) {
					clearTimeout(s._overflowTimeout);
				}

				if (
					data.oldIndex === undefined ||
					data.newIndex === undefined ||
					data.oldIndex <= data.newIndex
				) {
					s.scrollTo(0, 0);
				} else {
					let wh = resizeStore.getData().height;
					s.scrollTo(0, s._scrollHeight - wh);
				}

				s._overflowTimeout = setTimeout(() => {
					s.style.overflow = "auto";
					s.addEventListener("scroll", self.onScroll);

					self.onResize();
					setTimeout(() => {
						self.onResize();
					}, 2000); // wierd bug with delayed transform
				}, data.duration * 1000);
			} else {
				if (s._overflowTimeout) {
					clearTimeout(s._overflowTimeout);
				}

				s.style.overflow = "hidden";
				s.removeEventListener("scroll", self.onScroll);
			}
		});

		this._parent.checkControls();
	}

	setSlidesMode() {
		let self = this;

		this._scrollableSlides.forEach((s) => {
			s.style.overflow = "hidden";
			s.addEventListener("scroll", self.onScroll);
		});

		this.onSlideScroll({
			direction: "down",
			duration: 0,
			ease: window._vars.ease.bezier.ease,
		});

		this._parent._scrollUpEnabledFlagsArray.push(this.checkScrollUp);
		this._parent._scrollDownEnabledFlagsArray.push(this.checkScrollDown);
	}

	setScrollMode() {
		let self = this;

		this._scrollableSlides.forEach((s) => {
			s.removeEventListener("scroll", self.onScroll);
		});

		this._parent._scrollUpEnabledFlagsArray.remove(this.checkScrollUp);
		this._parent._scrollDownEnabledFlagsArray.remove(this.checkScrollDown);
	}

	init() {
		this._parent._slideScrollArray.push(this.onSlideScroll);
		this._parent._resizeArray.push(this.onResize);
		this._parent._slides.forEach((s, i) => {
			if (s.classList.contains("s-scrollable")) {
				s._index = i;
				this._scrollableSlides.push(s);

				let focusable = getFocusable(s);
				if (!focusable.length && s.children.length) {
					s.children[0].setAttribute("tabindex", 0);
				}
			}
		});
	}

	destroy() {
		let self = this;
		this._scrollableSlides.forEach((s) => {
			s.removeEventListener("scroll", self.onScroll);
		});
		this._parent._slideScrollArray.remove(this.onSlideScroll);
		this._parent._resizeArray.remove(this.onResize);
		this._parent._scrollUpEnabledFlagsArray.remove(this.checkScrollUp);
		this._parent._scrollDownEnabledFlagsArray.remove(this.checkScrollDown);
	}
}

let attach = function (parent, options) {
	let decorator;

	if (!parent._decorators) {
		parent._decorators = {};
	}

	options = Object.assign(defaultOptions, {}, options);

	if (parent._decorators[options.name]) {
		decorator = parent._decorators[options.name];
	} else {
		decorator = new Decorator(parent, options);
		parent._decorators[options.name] = decorator;
		decorator.init();
	}

	return decorator;
};

let detach = function (parent, options) {
	options = Object.assign(defaultOptions, options);

	if (!parent._decorators[options.name]) {
		console.error('no decorator with name "' + options.name + '"');
		return;
	}

	let decorator = parent._decorators[options.name];
	decorator.destroy();
};

export default {
	attach: attach,
	detach: detach,
};