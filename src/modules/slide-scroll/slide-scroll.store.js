import dispatcher from '../dispatcher.js';
import EventEmitter from '../utils/EventEmitter.js';

let eventEmitter = new EventEmitter();
let items = {};

let blockTime;
let animationSpeed;
let resetAfterScroll = false;

if (window._vars.time.slideScroll) {
	animationSpeed = window._vars.time.slideScroll.animationSpeed || 0.6;
	blockTime = window._vars.time.slideScroll.blockTime || 0.6;
}

// blockTime - время на которое будет заблокирована послайдовость
// animationSpeed - скорость смены слайдов

let nativeScroll = false;
let lastAdded = '';

let defaultProps = {
	blockTime: blockTime,
	animationSpeed: animationSpeed
};

let _handleEvent = function(e) {
	let setBlockTimer = false;

	if (e.type === 'slide-scroll:add') {
		if (items.hasOwnProperty(e.id)) return;

		lastAdded = e.id;
		items[e.id] = {
			id: e.id,
			index: e.index,
			total: e.total,
			isBlocked: false,
			blockTime: e.blockTime || defaultProps.blockTime,
			animationSpeed: e.animationSpeed || defaultProps.animationSpeed
		}
		items[e.id].defaultAnimationSpeed = items[e.id].animationSpeed;
		items[e.id].defaultBlockTime = items[e.id].blockTime;

		eventEmitter.dispatch();
	}

	if (e.type === 'slide-scroll:remove') {
		if (!items.hasOwnProperty(e.id)) return;
		delete items[e.id];
	}

	if (e.type === 'slide-scroll:set-props') {
		if (e.hasOwnProperty('speed')) {
			items[e.id].animationSpeed = e.speed;
		}
		if (e.hasOwnProperty('blockTime')) {
			items[e.id].blockTime = e.blockTime;
		}
	}

	if (e.type === 'slide-scroll:reset-props') {
		items[e.id].animationSpeed = items[e.id].defaultAnimationSpeed;
		items[e.id].blockTime = items[e.id].defaultBlockTime;
	}

	if (e.type === 'slide-scroll:reset-props-after') {
		items[e.id].resetAfterScroll = true;
	}

	if (e.type === 'slide-scroll:direction') {
		if (!items.hasOwnProperty(e.id)) return;
		if (items[e.id].isBlocked) return;

		if (e.direction === 'up') {
			if (items[e.id].index === 0) return;
			items[e.id].index--;
		}
		if (e.direction === 'down') {
			if (items[e.id].index === items[e.id].total - 1) return;
			items[e.id].index++;
		}

		if (e.hasOwnProperty('native')) {
			items[e.id].nativeScroll = e.native;
		}
		if (e.setFocus) {
			items[e.id].setFocus = true;
		}

		items[e.id].isBlocked = true;
		items[e.id].setBlockTimer = true;

		eventEmitter.dispatch();

		items[e.id].nativeScroll = false;
		items[e.id].setFocus = false;
	}

	if (e.type === 'slide-scroll:to') {
		if (!items[e.id]) return;
		if (items[e.id].isBlocked) return;
		if (!items.hasOwnProperty(e.id)) return;
		if (items[e.id].index === e.index) return;

		if (e.index < 0) {
			if (items[e.id].index === 0) return;
			items[e.id].index = 0;
		} else if (e.index > items[e.id].total - 1) {
			if (items[e.id].index === items[e.id].total - 1) return;
			items[e.id].index = items[e.id].total - 1;
		} else {
			items[e.id].index = e.index;
		}

		if (e.hasOwnProperty('native')) {
			items[e.id].nativeScroll = e.native;
		}
		if (e.setFocus) {
			items[e.id].setFocus = true;
		}

		items[e.id].isBlocked = true;
		items[e.id].setBlockTimer = true;

		eventEmitter.dispatch();

		items[e.id].nativeScroll = false;
		items[e.id].setFocus = false;
	}

	for (let id in items) {
		let item = items[id];
		if (item.setBlockTimer) {
			item.setBlockTimer = false;
			setTimeout(function() { // время для дополнительных ихменений настроек
				setTimeout(function() {
					item.isBlocked = false;
	
					if (item.resetAfterScroll) {
						item.animationSpeed = item.defaultAnimationSpeed;
						item.blockTime = item.defaultBlockTime;
						item.resetAfterScroll = false;
					}
				}, item.blockTime * 1000);
			}, 0);
		}
	}

}

let getData = function() {
	return {
		items: items,
		nativeScroll: nativeScroll,
		lastAdded: lastAdded,
	}
}

let _init = function() {
	dispatcher.subscribe(_handleEvent);
}

_init();

export default {
	subscribe: eventEmitter.subscribe.bind(eventEmitter),
	unsubscribe: eventEmitter.unsubscribe.bind(eventEmitter),
	getData: getData
}